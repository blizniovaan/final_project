package by.epam.library.tag;

/**
 * Created by HP on 02.03.2016.
 */
public class JspArrayBean {
    private static final String[] lettersArray = {"0","A","B","V","G","D","E","Zh","Z","I","K","L","M","N",
    "O","P","R","S","T","U","F","Kh","Ts","Ch","Sh","Shch","Y","Ia"};
    private String[] array;
    public JspArrayBean(){
        this.array = lettersArray;
    }
    public String getSize(){
        int size = array.length;
        return String.valueOf(size);
    }
    public String getElement(int i){
        return array[i];
    }
}
