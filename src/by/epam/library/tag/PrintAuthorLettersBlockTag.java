package by.epam.library.tag;

import by.epam.library.controller.SessionParameterName;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Created by HP on 02.03.2016.
 */
public class PrintAuthorLettersBlockTag extends TagSupport {
    public static final String ADMIN = "admin";
    public static final String USER = "user";
    public static final String RU = "ru";
    public static final String EN = "en";
    public static final String UNDERLINE = "_";
    public static final String LETTER = "letter.";
    public static final String SPAN_TEXT = "title.by_author";
    public static final String COLON = " :";

    private JspArrayBean array;
     public PrintAuthorLettersBlockTag(){
         this.array = new JspArrayBean();
     }

    @Override
    public int doStartTag() throws JspException {
        String path = "localization.library";
        String language = String.valueOf(pageContext.getSession().getAttribute("language"));
        switch (language){
            case RU:{
                path +=UNDERLINE + RU;
                break;
            }
            case EN:{
                path +=UNDERLINE +EN;
                break;
            }
        }
        Integer size = Integer.valueOf(array.getSize());
        ResourceBundle resourceBundle = ResourceBundle.getBundle(path);
        String spanText = resourceBundle.getString(SPAN_TEXT);
        String role = String.valueOf(pageContext.getSession().getAttribute(SessionParameterName.ROLE));
        switch (role){
            case ADMIN:{
                try{
                    JspWriter out = pageContext.getOut();
                    out.print("<ul class=\"sort\">");
                    out.print("<span>" + spanText + COLON +"</span>");

                    for(int i = 1; i < size; i++){
                        out.print("<li><a href=\"/controller?command=SELECT_ALL_BOOKS_BY_AUTHOR&letterFrom=" +
                                i + "&page=RESULT_OF_SEARCH_FOR_ADMIN_PAGE\">");
                        out.print(resourceBundle.getString("letter." + array.getElement(i)));
                        out.println("</a></li>");
                    }
                    out.write("</ul>");
                }catch(IOException e){
                    throw new JspException(e.getMessage());
                }
                break;
            }
            case USER:{
                try{
                    JspWriter out = pageContext.getOut();
                    out.print("<ul class=\"sort\">");
                    out.print("<span>" + spanText + COLON +"</span>");
                    for(int i = 1; i < size; i++){
                        out.print("<li><a href=\"/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=" +
                                i + "&page=RESULT_OF_SEARCH_FOR_USER_PAGE\">");
                        out.print(resourceBundle.getString(LETTER + array.getElement(i)));
                        out.println("</a></li>");
                    }
                    out.write("</ul>");
                }catch(IOException e){
                    throw new JspException(e.getMessage());
                }
                break;
            }
        }
        return SKIP_BODY;
    }
}
