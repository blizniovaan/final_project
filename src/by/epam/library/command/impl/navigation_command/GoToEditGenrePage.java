package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Genre;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.GenreServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoToEditGenrePage implements Command{
    private final static GenreServiceImpl genreService = GenreServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = JspPageName.EDIT_GENRE_PAGE;
        try {
            List<Genre> genres = genreService.selectAll();
            if(genres != null){
                request.setAttribute(RequestParameterName.GENRES,genres);
            }
            else{
                request.setAttribute(RequestParameterName.GENRES,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToEditGenrePage Command Exception",e);
        }
        return page;
    }
}
