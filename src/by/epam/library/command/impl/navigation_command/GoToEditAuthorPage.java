package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Author;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.AuthorServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoToEditAuthorPage implements Command {
    private final static AuthorServiceImpl authorService = AuthorServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = JspPageName.EDIT_AUTHOR_PAGE;
        try {
            List<Author> authors = authorService.selectAll();
            if(authors != null){
                request.setAttribute(RequestParameterName.AUTHORS,authors);
            }
            else{
                request.setAttribute(RequestParameterName.AUTHORS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToEditAuthorPage Command exception",e);
        }
        return page;
    }
}