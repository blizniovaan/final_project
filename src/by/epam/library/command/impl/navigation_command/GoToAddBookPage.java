package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Author;
import by.epam.library.entity.Genre;
import by.epam.library.entity.Publisher;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.AuthorServiceImpl;
import by.epam.library.service.impl.GenreServiceImpl;
import by.epam.library.service.impl.PublisherServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GoToAddBookPage implements Command {
    private final static GenreServiceImpl genreService = GenreServiceImpl.getInstance();
    private final static AuthorServiceImpl authorService = AuthorServiceImpl.getInstance();
    private final static PublisherServiceImpl publisherService = PublisherServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = JspPageName.ADD_BOOK_PAGE;
        try {
            List<Genre> genres = genreService.selectAll();
            List<Author> authors = authorService.selectAll();
            List<Publisher> publishers = publisherService.selectAll();
            if(genres != null && authors != null && publishers != null){
                request.setAttribute(RequestParameterName.PUBLISHERS,publishers);
                request.setAttribute(RequestParameterName.GENRES,genres);
                request.setAttribute(RequestParameterName.AUTHORS,authors);
            }
            else{
                request.setAttribute(RequestParameterName.PUBLISHERS,null);
                request.setAttribute(RequestParameterName.GENRES,null);
                request.setAttribute(RequestParameterName.AUTHORS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToAddBookPage Command exception",e);
        }
        return page;
    }
}