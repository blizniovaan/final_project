package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GoToDeleteBookPage implements Command{
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = JspPageName.ADMIN_BOOKS_PAGE;
        try {
            List<Book> books = service.selectAllAvailable();
            if(books != null){
                request.setAttribute(RequestParameterName.PART_BOOKS,books);
            }
            else{
                request.setAttribute(RequestParameterName.PART_BOOKS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToDeleteBookPage Command exception",e);
        }
        request.setAttribute(RequestParameterName.COMMAND,RequestParameterName.DELETE_COMMAND);
        return page;
    }
}
