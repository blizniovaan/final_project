package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.Randomizer;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.controller.SessionParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GoTo implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String pageFromUser = request.getParameter(RequestParameterName.PAGE);
        String page = JspPageName.getPage(pageFromUser);
        Double currentRandomNumber = Randomizer.getRandomNumber();
        session.setAttribute(SessionParameterName.CURRENT_RANDOM_NUMBER,currentRandomNumber);
        if(!page.isEmpty()){
            return page;
        }else{
            return JspPageName.ERROR_PAGE;
        }
    }
}
