package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoToGiveBookPage implements Command {
    private static UserServiceImpl userService = UserServiceImpl.getInstance();
    private final static BookServiceImpl bookService = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = null;
        try {
            List<Book> books = bookService.selectAllAvailable();
            List<User> users = userService.selectAll();
            if(books != null && users != null){
                request.setAttribute(RequestParameterName.BOOKS,books);
                request.setAttribute(RequestParameterName.USERS_LIST,users);
                page = JspPageName.GIVE_BOOK_PAGE;
            }
            else{
                page = JspPageName.ERROR_PAGE;
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToGiveBookPage Command Exception",e);
        }
        return page;
    }
}
