package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GoToCard implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        List<History> records = null;
        String page = null;
        HttpSession session = request.getSession(false);

        if(session != null) {
            page = JspPageName.USER_CARD;
            Integer userId = (Integer) session.getAttribute(RequestParameterName.USER_ID);
            User user = new User();
            user.setUserId(userId);
            try {
                records = service.getBooksFromCard(user);
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
            request.setAttribute(RequestParameterName.RECORDS,records);
        }
        else{
            page = JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        return page;
    }
}
