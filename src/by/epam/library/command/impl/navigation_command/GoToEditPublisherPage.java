package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Publisher;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.PublisherServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class GoToEditPublisherPage implements Command {
    private final static PublisherServiceImpl publisherService = PublisherServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = JspPageName.EDIT_PUBLISHER_PAGE;
        try {
            List<Publisher> publishers = publisherService.selectAll();
            if(publishers != null){
                request.setAttribute(RequestParameterName.PUBLISHERS,publishers);
            }
            else{
                request.setAttribute(RequestParameterName.PUBLISHERS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToEditPublisherPage Command exception",e);
        }
        return page;
    }
}