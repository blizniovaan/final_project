package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoToDeleteUserPage implements Command {
    private final static UserServiceImpl service = UserServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = JspPageName.ADMIN_USERS_PAGE;
        try {
            List<User> users = service.selectAll();
            if(users != null){
                request.setAttribute(RequestParameterName.USERS_LIST,users);
            }
            else{
                request.setAttribute(RequestParameterName.USERS_LIST,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToDeleteUserPage Command Exception",e);
        }
        request.setAttribute(RequestParameterName.COMMAND,RequestParameterName.DELETE_COMMAND);
        return page;
    }
}
