package by.epam.library.command.impl.navigation_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.GenreTranslator;
import by.epam.library.command.helper.LetterTranslator;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoToGenrePage implements Command{
    private final static BookServiceImpl service = BookServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        List<Book> books = null;
        String page = null;
        try {
            int letterFrom = Integer.parseInt(request.getParameter(RequestParameterName.LETTER_FROM));
            String preparedLetterFrom = LetterTranslator.translate(letterFrom);
            String genre = request.getParameter(RequestParameterName.GENRE);
            String preparedGenreTitle = GenreTranslator.translate(genre);

            books = service.selectAvailableBookByLetter(preparedLetterFrom, preparedGenreTitle);

            if(books != null){
                page = JspPageName.GENRE_PAGE;
                request.setAttribute(RequestParameterName.LETTER_FROM,letterFrom);
                request.setAttribute(RequestParameterName.GENRE,genre);
                request.setAttribute(RequestParameterName.PART_BOOKS, books);
            }
            else{
                 page = JspPageName.ERROR_PAGE;
            }
        } catch (ServiceException e) {
            throw new CommandException("GoToGenrePage Command Exception",e);
        }
        return page;
    }
}


