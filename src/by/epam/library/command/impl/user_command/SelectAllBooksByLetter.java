package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.LetterTranslator;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SelectAllBooksByLetter implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        List<Book> books = null;
        String pageFromRequest = request.getParameter(RequestParameterName.PAGE);
        String page = JspPageName.getPage(pageFromRequest);
        try {
            int letterFrom = Integer.parseInt(request.getParameter(RequestParameterName.LETTER_FROM));
            String preparedLetterFrom = LetterTranslator.translate(letterFrom);

            books = service.selectAllBooksByLetter(preparedLetterFrom);
            if(books != null){
                request.setAttribute(RequestParameterName.SEARCH_PARAMETER,RequestParameterName.LETTER_PARAMETER);
                request.setAttribute(RequestParameterName.LETTER_FROM,letterFrom);
                request.setAttribute(RequestParameterName.PART_BOOKS,books);
            }
            else{
                request.setAttribute(RequestParameterName.PART_BOOKS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("SelectAllBooksByLetter Command Exception",e);
        }
        return page;
    }
}
