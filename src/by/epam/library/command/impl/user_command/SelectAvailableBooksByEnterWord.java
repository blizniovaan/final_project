package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class SelectAvailableBooksByEnterWord implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        List<Book> books = null;
        String preparedEnterWord = null;
        String pageFromRequest = request.getParameter(RequestParameterName.PAGE);
        String page = JspPageName.getPage(pageFromRequest);
            try {
                String enterWord = request.getParameter(RequestParameterName.ENTER_WORD);
                preparedEnterWord = new String(enterWord.getBytes(RequestParameterName.ISO_8859_1_ENCODING), RequestParameterName.UTF_8_ENCODING );
                books = service.selectAvailableByEnterWord(preparedEnterWord);
                if(books != null){
                    request.setAttribute(RequestParameterName.SEARCH_PARAMETER,RequestParameterName.ENTER_WORD);
                    request.setAttribute(RequestParameterName.ENTER_WORD,preparedEnterWord);
                    request.setAttribute(RequestParameterName.PART_BOOKS,books);
                }
                else{
                    request.setAttribute(RequestParameterName.PART_BOOKS,null);
                }
            } catch (ServiceException e) {
                throw new CommandException("SelectAvailableBooksByEnterWord Command Exception",e);
            } catch (UnsupportedEncodingException e) {
                throw new CommandException("Encoding error",e);
            }
        return page;
    }
}