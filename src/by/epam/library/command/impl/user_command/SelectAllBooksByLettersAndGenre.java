package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.GenreTranslator;
import by.epam.library.command.helper.LetterTranslator;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SelectAllBooksByLettersAndGenre implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        List<Book> books = null;
        String page = JspPageName.GENRE_PAGE;
        try {
            int letterFrom = Integer.parseInt(request.getParameter(RequestParameterName.LETTER_FROM));
            String preparedLetterFrom = LetterTranslator.translate(letterFrom);
            String genre = request.getParameter(RequestParameterName.GENRE);
            String preparedGenreTitle = GenreTranslator.translate(genre);

            books = service.selectAllBooksByLetter(preparedLetterFrom, preparedGenreTitle);
            if(books != null){
                request.setAttribute(RequestParameterName.LETTER_FROM,letterFrom);
                request.setAttribute(RequestParameterName.GENRE,genre);
                request.setAttribute(RequestParameterName.PART_BOOKS,books);
            }
            else{
                request.setAttribute(RequestParameterName.PART_BOOKS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("SelectAllBooksByLettersAndGenre Command Exception",e);
        }
        return page;
    }
}
