package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.controller.SessionParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SelectAllBooks implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String pageFromUser = request.getParameter(RequestParameterName.PAGE);
        String page = JspPageName.getPage(pageFromUser);
        try {
            List<Book> books = service.selectAll();
            if(books != null){
                session.setAttribute(SessionParameterName.WORK_OBJECT,SessionParameterName.BOOK_OBJECT);
                request.setAttribute(RequestParameterName.PART_BOOKS,books);
            }
            else{
                request.setAttribute(RequestParameterName.PART_BOOKS,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("SelectAllBooks Command Exception",e);
        }
        return page;
    }
}
