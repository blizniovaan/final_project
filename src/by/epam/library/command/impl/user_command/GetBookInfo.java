package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetBookInfo implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Book book = new Book();
        Integer bookId = Integer.valueOf(request.getParameter(RequestParameterName.BOOK_ID));
        String genre = request.getParameter(RequestParameterName.GENRE);
        String page = request.getParameter(RequestParameterName.PAGE);
        String preparedPage = JspPageName.getPage(page);
        book.setBookId(bookId);
        Book preparedBook = null;
        try {
            preparedBook = service.selectOne(book);
        } catch (ServiceException e) {
            throw new CommandException("GetBookInfo Command exception",e);
        }
        request.setAttribute(RequestParameterName.GENRE,genre);
        request.setAttribute(RequestParameterName.BOOK_ATTRIBUTE,preparedBook);
        return preparedPage;
    }
}
