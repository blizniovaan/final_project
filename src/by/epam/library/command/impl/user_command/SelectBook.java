package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by HP on 29.02.2016.
 */
public class SelectBook implements Command {
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = null;
        Book selectedBook = null;
        Book book = new Book();
        String genre = request.getParameter(RequestParameterName.GENRE);
        Integer bookId = Integer.valueOf(request.getParameter(RequestParameterName.BOOK_ID));
        book.setBookId(bookId);
        try {
            selectedBook = service.selectOne(book);
            request.setAttribute(RequestParameterName.GENRE,genre);
            page = JspPageName.BOOK_PAGE;
            if(selectedBook != null){
                request.setAttribute(RequestParameterName.BOOK_ATTRIBUTE,selectedBook);
            }
            else{
                request.setAttribute(RequestParameterName.BOOK_ATTRIBUTE,null);
            }
        } catch (ServiceException e) {
            throw new CommandException("SelectBook Command Exception",e);
        }
        return page;
    }
}
