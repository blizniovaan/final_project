package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.History;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ReturnBook implements Command {
    public static final String COMMA = ",";
    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = null;
        List<History> records = getRecords(request);
        try {
          List<History> resultRecords = service.returnBooks(records);
            if(resultRecords != null){
                page = JspPageName.CONFIRMATION_RETURN_BOOK;
            }
            else{
                response.setStatus(404);
                page = JspPageName.CONFIRMATION_RETURN_BOOK;
            }
        } catch (ServiceException e) {
           throw new CommandException("ReturnBook Command Exception",e);
        }
        return page;
    }

    private static List<History> getRecords(HttpServletRequest request){
        List<History> records = new ArrayList<>();
        String strRecordsId = request.getParameter(RequestParameterName.RECORDS_ID);
        if(strRecordsId.isEmpty()){
            return records;
        }
        String[] recordsId = strRecordsId.split(COMMA);
        Date deliveryDate = Calendar.getInstance().getTime();
        for (int i = 0; i < recordsId.length; i++) {
            History record = new History();
            record.setRecordId(Integer.valueOf(recordsId[i]));
            record.setDeliveryDate(deliveryDate);
            records.add(record);
        }
        return records;
    }
}
