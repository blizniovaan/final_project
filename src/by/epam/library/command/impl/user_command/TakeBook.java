package by.epam.library.command.impl.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.ParameterTranslator;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.HistoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;

public class TakeBook implements Command {

    private final static HistoryServiceImpl service = HistoryServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        History resultRecord = null;
        String page = null;
        String genre = request.getParameter(RequestParameterName.GENRE);
            try {
                History record = getRecord(request);
                resultRecord = service.insert(record);
                if (resultRecord != null) {
                    request.setAttribute(RequestParameterName.GENRE,genre);
                    page = JspPageName.CONFIRMATION;
                } else {
                    response.setStatus(404);
                    page = JspPageName.ERROR_PAGE;
                }
            } catch (ServiceException e) {
                throw new CommandException("TakeBook Command Exception", e);
            }
        return page;
    }
    private static History getRecord(HttpServletRequest request){
        Book book = new Book();
        User user = new User();
        History history = new History();
        Date takingDate = Calendar.getInstance().getTime();

        String userId = request.getParameter(RequestParameterName.USER_ID);
        String bookId = request.getParameter(RequestParameterName.BOOK_ID);
        String place = request.getParameter(RequestParameterName.BOOK_PLACE);
        String preparedPlace = ParameterTranslator.translate(place);

        book.setBookId(Integer.valueOf(bookId));
        user.setUserId(Integer.valueOf(userId));
        history.setUser(user);
        history.setBook(book);
        history.setTakingDate(takingDate);
        history.setPlace(preparedPlace);

        return history;
    }
}
