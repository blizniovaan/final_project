package by.epam.library.command.impl.user_command.authorization;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.DoubleRequestAnalyser;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by HP on 03.02.2016.
 */
public class Login implements Command {
    private static final UserServiceImpl service = UserServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request,HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        boolean requestValid = DoubleRequestAnalyser.checkDoubleRequest(session);
        if(!requestValid){
            response.setStatus(400);
            return JspPageName.ERROR_PAGE;
        }
        String page = null;
        User newUser = new User();
        newUser.setLogin(request.getParameter(RequestParameterName.LOGIN));
        newUser.setPassword(request.getParameter(RequestParameterName.PASSWORD));
        User user = null;
        try {
            user = service.signIn(newUser);
        } catch (ServiceException e) {
            throw  new CommandException("Login Command Exception",e);
        }

        if(user != null){
            session.setAttribute(RequestParameterName.ROLE, user.getRole());
            session.setAttribute(RequestParameterName.USER_ID, user.getUserId());
            page = JspPageName.USER_PAGE;
        }
        else{
            response.setStatus(401);
            return JspPageName.ERROR_NOT_FOUND_USER_PAGE;
        }
        return page;
    }

}
