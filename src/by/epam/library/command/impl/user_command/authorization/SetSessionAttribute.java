package by.epam.library.command.impl.user_command.authorization;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.Randomizer;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.controller.SessionParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SetSessionAttribute implements Command {
    public static final String DASH="-";
    public static final String UNDERLINE="_";
    public static final Double FIRST_RANDOM_NUMBER = 101.0;

    public SetSessionAttribute(){}

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            session = request.getSession(true);
        }
        Double currentRandomNumber = Randomizer.getRandomNumber();
        session.setAttribute(SessionParameterName.CURRENT_RANDOM_NUMBER,currentRandomNumber);
        session.setAttribute(SessionParameterName.PREV_NUMBER,FIRST_RANDOM_NUMBER);
        String typePopup = String.valueOf(request.getParameter(RequestParameterName.TYPE_POPUP));
        TypePopup preparedTypePopup = TypePopup.valueOf(typePopup.replace(DASH, UNDERLINE).toUpperCase().trim());
        switch (preparedTypePopup){
            case SIGNIN_POPUP:{
                return JspPageName.SIGN_IN_POPUP;
            }
            case SIGNUP_POPUP:{
                return JspPageName.SIGN_UP_POPUP;
            }
            default:{
                return null;
            }
        }
    }
    enum TypePopup{
        SIGNIN_POPUP,SIGNUP_POPUP
    }
}
