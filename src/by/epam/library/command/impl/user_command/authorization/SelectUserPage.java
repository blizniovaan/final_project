package by.epam.library.command.impl.user_command.authorization;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.controller.SessionParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by HP on 26.02.2016.
 */
public class SelectUserPage implements Command {
    private static final String ADMIN = "admin";
    private static final String USER = "user";
    private final static BookServiceImpl service = BookServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = null;
        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String role = String.valueOf(session.getAttribute(RequestParameterName.ROLE));
        page = getUserPage(role,session,request);
        if(page.isEmpty()){
            page = JspPageName.ERROR_PAGE;
        }
        return page;
    }

    private static String getUserPage(String role,HttpSession session,HttpServletRequest request) throws CommandException {
        switch (role){
            case ADMIN:{
                List<Book> books = null;
                books = getAllBooks();
                request.setAttribute(RequestParameterName.PART_BOOKS, books);
                session.setAttribute(SessionParameterName.WORK_OBJECT, SessionParameterName.BOOK_OBJECT);
                session.setAttribute(SessionParameterName.ROLE, ADMIN);
                return JspPageName.ADMIN_BOOKS_PAGE;
            }
            case USER:{
                session.setAttribute(SessionParameterName.ROLE, USER);
                return JspPageName.USER_PAGE;
            }
            default:{
                return JspPageName.ERROR_PAGE;
            }
        }
    }
    private static List<Book> getAllBooks() throws CommandException {
        List<Book> books = null;
        try {
            books = service.selectAll();
        } catch (ServiceException e) {
            throw  new CommandException("SelectUserPage Command Exception",e);
        }
        return books;
    }
}
