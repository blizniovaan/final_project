package by.epam.library.command.impl.user_command.authorization;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.DoubleRequestAnalyser;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Registration implements Command {
    private static final String USER_ROLE = "user";

    private static final UserServiceImpl service = UserServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request,HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        boolean requestValid = DoubleRequestAnalyser.checkDoubleRequest(session);
        if(!requestValid){
            response.setStatus(400);
            return JspPageName.ERROR_PAGE;
        }
        User user = new User();
        user.setName(request.getParameter(RequestParameterName.NAME));
        user.setSurname(request.getParameter(RequestParameterName.SURNAME));
        user.setLogin(request.getParameter(RequestParameterName.LOGIN));
        user.setPassword(request.getParameter(RequestParameterName.PASSWORD));
        String rep_password = request.getParameter(RequestParameterName.REPEAT_PASSWORD);
        user.setRole(USER_ROLE);

        String page = null;
        User currentUser = null;
        try {
            currentUser = service.insert(user,rep_password);
        } catch (ServiceException e) {
            throw new CommandException("Registration Command Exception",e);
        }
        if(currentUser != null){
            session.setAttribute(RequestParameterName.USER_ID, user.getUserId());
            session.setAttribute(RequestParameterName.ROLE, user.getRole());
            return JspPageName.MAIN_PAGE;
        }
        else{
            response.setStatus(401);
            return JspPageName.USER_ERROR;
        }
    }
}
