package by.epam.library.command.impl.localization;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Localization implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(true);
        session.setAttribute(RequestParameterName.LANGUAGE,request.getParameter(RequestParameterName.LANGUAGE));
        String page = request.getParameter(RequestParameterName.PAGE);
        String preparedPage = JspPageName.getPage(page);
        return preparedPage;
    }
}
