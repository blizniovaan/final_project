package by.epam.library.command.impl.admin_command.author_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Author;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.AuthorServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by HP on 15.03.2016.
 */
public class AddAuthor implements Command {
    private final static AuthorServiceImpl service = AuthorServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = null;
        Author author = new Author();
        String name = request.getParameter(RequestParameterName.NAME);
        author.setName(name);
        Author inseredAuthor = null;
        try {
            inseredAuthor = service.insert(author);
        } catch (ServiceException e) {
            throw new CommandException("AddAuthor Command Exception",e);
        }
        if(inseredAuthor != null){
            page = JspPageName.EDIT_AUTHOR_PAGE;
        }
        else{
            response.setStatus(404);
            page = JspPageName.CONFIRMATION_EDIT_AUTHOR;
        }
        return page;
    }
}

