package by.epam.library.command.impl.admin_command.author_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Author;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.AuthorServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 15.03.2016.
 */
public class DeleteAuthor implements Command {
    public static final String COMMA = ",";

    private final static AuthorServiceImpl service = AuthorServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        boolean isDelete = false;
        String page = null;
        List<Author> authors = getAuthors(request);
        try {
            isDelete = service.delete(authors);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        if(isDelete){
            page = JspPageName.EDIT_AUTHOR_PAGE;
        }
        else{
            response.setStatus(404);
            page = JspPageName.CONFIRMATION_EDIT_AUTHOR;
        }
        return page;
    }
    private static List<Author> getAuthors(HttpServletRequest request){
        List<Author> authors = new ArrayList<>();
        String strAuthorsId = request.getParameter(RequestParameterName.AUTHOR_ID);
        if(strAuthorsId.isEmpty()){
            return authors;
        }
        String[] arrAuthorsId = strAuthorsId.split(COMMA);
        for (int i = 0 ; i < arrAuthorsId.length ; i++){
            Author currentAuthor = new Author();
            currentAuthor.setAuthorId(Integer.valueOf(arrAuthorsId[i]));
            authors.add(currentAuthor);
        }
        return authors;
    }
}

