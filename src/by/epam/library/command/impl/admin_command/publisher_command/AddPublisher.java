package by.epam.library.command.impl.admin_command.publisher_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Publisher;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.PublisherServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddPublisher implements Command {
    private final static PublisherServiceImpl service = PublisherServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = null;
        Publisher publisher = new Publisher();
        String title = request.getParameter(RequestParameterName.TITLE);
        publisher.setTitle(title);
        Publisher inseredPublisher = null;
        try {
            inseredPublisher = service.insert(publisher);
        } catch (ServiceException e) {
            throw new CommandException("AddPublisher Command Exception",e);
        }
        if(inseredPublisher != null){
            page = JspPageName.EDIT_PUBLISHER_PAGE;
        }
        else{
            response.setStatus(404);
            page = JspPageName.EDIT_PUBLISHER_PAGE;
        }
        return page;
    }
}
