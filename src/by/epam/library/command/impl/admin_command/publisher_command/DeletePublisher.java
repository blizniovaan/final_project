package by.epam.library.command.impl.admin_command.publisher_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Publisher;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.PublisherServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


public class DeletePublisher implements Command {
    public static final String COMMA = ",";

    private final static PublisherServiceImpl service = PublisherServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        boolean isDelete = false;
        String page = null;
        List<Publisher> publishers = getPublishers(request);
        try {
            isDelete = service.delete(publishers);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        if(isDelete){
            page = JspPageName.EDIT_PUBLISHER_PAGE;
        }
        else{
            response.setStatus(404);
            page = JspPageName.CONFIRMATION_EDIT_PUBLISHER;
        }
        return page;
    }
    private static List<Publisher> getPublishers(HttpServletRequest request){
        List<Publisher> publishers = new ArrayList<>();
        String strPublisherId = request.getParameter(RequestParameterName.PUBLISHER_ID);
        if(strPublisherId.isEmpty()){
            return publishers;
        }
        String[] arrPublisherId = strPublisherId.split(COMMA);
        for (int i = 0 ; i < arrPublisherId.length ; i++){
            Publisher currentPublisher = new Publisher();
            currentPublisher.setPublisherId(Integer.valueOf(arrPublisherId[i]));
            publishers.add(currentPublisher);
        }
        return publishers;
    }
}
