package by.epam.library.command.impl.admin_command.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetInfoAboutUser implements Command {
    private final static BookServiceImpl bookService = BookServiceImpl.getInstance();
    private static UserServiceImpl userService = UserServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
       User userInfo = null;
       String page = JspPageName.USER_INFO_PAGE;
       User user = new User();
       Integer userId = Integer.valueOf(request.getParameter(RequestParameterName.USER_ID));
       user.setUserId(userId);
       List<History> resultRecords = null;
       History history = new History();
       history.setUser(user);
        try {
            userInfo = userService.selectOne(user);
            resultRecords = bookService.getBooksFromCard(user);
        } catch (ServiceException e) {
            throw new CommandException("GetInfoAboutUser Command Exception",e);
        }
        if(userInfo != null){
            request.setAttribute(RequestParameterName.USER,userInfo);
        }
        else{
            request.setAttribute(RequestParameterName.USER,null);
        }

        if(resultRecords != null){
            request.setAttribute(RequestParameterName.RECORDS,resultRecords);
        }
        else{
            request.setAttribute(RequestParameterName.RECORDS,null);
        }
       return page;
    }
}
