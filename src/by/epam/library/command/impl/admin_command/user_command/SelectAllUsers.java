package by.epam.library.command.impl.admin_command.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.controller.SessionParameterName;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SelectAllUsers implements Command {
    private static UserServiceImpl service = UserServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request,HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return  JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = JspPageName.ADMIN_USERS_PAGE;
        List<User> users = null;
        try {
            users = service.selectAll();
        } catch (ServiceException e) {
            throw new CommandException("SelectAllUsers Command Exception",e);
        }
        if(users != null){
            session.setAttribute(SessionParameterName.WORK_OBJECT,SessionParameterName.USER);
            request.setAttribute(RequestParameterName.USERS_LIST,users);
        }
        else {
            request.setAttribute(RequestParameterName.USERS_LIST,null);
        }
        return page;
    }
}
