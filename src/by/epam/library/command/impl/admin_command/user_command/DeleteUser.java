package by.epam.library.command.impl.admin_command.user_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class DeleteUser implements Command {
    public static final String COMMA = ",";
    private static UserServiceImpl service = UserServiceImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = null;
        List<User> users = getUsers(request);
        try {
            boolean isDelete = service.delete(users);
            if (isDelete) {
                page = JspPageName.ADMIN_USERS_PAGE;
            } else {
                response.setStatus(404);
                page = JspPageName.CONFIRMATION_REMOVE_USER;
            }
        } catch (ServiceException e) {
            throw new CommandException("DeleteUser Command exception", e);
        }
        return page;
    }
    private static List<User> getUsers(HttpServletRequest request) {
        List<User> users = new ArrayList<>();
        String strUsersId = request.getParameter(RequestParameterName.USERS_ID);
        if (strUsersId.isEmpty()) {
            return users;
        }
        String[] arrUsersId = strUsersId.split(COMMA);
        for (int i = 0; i < arrUsersId.length; i++) {
            User user = new User();
            user.setUserId(Integer.valueOf(arrUsersId[i]));
            users.add(user);
        }
        return users;
    }
}
