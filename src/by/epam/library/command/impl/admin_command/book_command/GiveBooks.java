package by.epam.library.command.impl.admin_command.book_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.command.helper.ParameterTranslator;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.HistoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GiveBooks implements Command {
    public static final String COMMA = ",";
    private final static HistoryServiceImpl service = HistoryServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        List<History> resultRecords = null;
        String page = null;
        String genre = request.getParameter(RequestParameterName.GENRE);

            try {
                History record = getRecord(request);
                List<Book> books = getBooks(request);
                resultRecords = service.insertPart(record,books);
                if (resultRecords != null) {
                    request.setAttribute(RequestParameterName.GENRE,genre);
                    page = JspPageName.CONFIRMATION;
                } else {
                    response.setStatus(404);
                    page = JspPageName.CONFIRMATION;
                }
            } catch (ServiceException e) {
                throw new CommandException("GiveBooks Command Exception", e);
            }

        return page;
    }
    private static History getRecord(HttpServletRequest request){
        User user = new User();
        History history = new History();
        Date takingDate = Calendar.getInstance().getTime();

        String userId = request.getParameter(RequestParameterName.USER_ID);
        String place = request.getParameter(RequestParameterName.BOOK_PLACE);
        String preparedPlace = ParameterTranslator.translate(place);

        if(userId.isEmpty() || place.isEmpty()){
            user = null;
            history.setUser(user);
            return history;
        }
        user.setUserId(Integer.valueOf(userId));
        history.setUser(user);
        history.setTakingDate(takingDate);
        history.setPlace(preparedPlace);

        return history;
    }

    private static List<Book> getBooks(HttpServletRequest request){
        List<Book> books = new ArrayList<>();
        String strBooksId = request.getParameter(RequestParameterName.BOOKS_ID);
        if(strBooksId.isEmpty()){
            return books;
        }
        String[] arrBooksId = strBooksId.split(COMMA);
        for (int i = 0; i < arrBooksId.length; i++) {
            Book book = new Book();
            book.setBookId(Integer.valueOf(arrBooksId[i]));
            books.add(book);
        }
        return books;
    }
}
