package by.epam.library.command.impl.admin_command.book_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.*;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Set;

public class AddBook implements Command {
    public static final String COMMA = ",";
    public static final Integer AVAILABLE_STATUS = 1;

    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        String page = null;
        Book inseredBook = null;
        Book book = getBook(request);
        try {
            inseredBook = service.insert(book);
        } catch (ServiceException e) {
            throw  new CommandException("AddBook Command Exception",e);
        }
        if (inseredBook != null){
            page = JspPageName.CONFIRMATION_ADD_BOOK;
        }
        else {
            response.setStatus(404);
            page = JspPageName.ERROR_PAGE;
        }
     return page;
    }
    private static Book getBook(HttpServletRequest request){
        Book book = new Book();
        Genre genre = new Genre();
        Set<Author> authors = new HashSet<>();
        Publisher publisher = new Publisher();
        String strAuthorsId = request.getParameter(RequestParameterName.AUTHOR_ID);
        String[] arrAuthorId = strAuthorsId.split(COMMA);
        for (int i = 0; i < arrAuthorId.length; i++) {
            Author author = new Author();
            author.setAuthorId(Integer.valueOf(arrAuthorId[i]));
            authors.add(author);
        }
        book.setTitle(request.getParameter(RequestParameterName.BOOK_TITLE));
        book.setEditionYear(Integer.valueOf(request.getParameter(RequestParameterName.EDITION_YEAR)));
        genre.setGenreId(Integer.valueOf(request.getParameter(RequestParameterName.GENRE_ID)));
        book.setGenre(genre);
        publisher.setPublisherId(Integer.valueOf(request.getParameter(RequestParameterName.PUBLISHER_ID)));
        book.setPublisher(publisher);
        BookStatus status = new BookStatus();
        status.setStatusId(AVAILABLE_STATUS);
        book.setStatus(status);
        book.setDescription(request.getParameter(RequestParameterName.DESCRIPTION));
        book.setAuthors(authors);

        return book;
    }
}
