package by.epam.library.command.impl.admin_command.book_command;

import by.epam.library.command.Command;
import by.epam.library.command.CommandException;
import by.epam.library.controller.JspPageName;
import by.epam.library.controller.RequestParameterName;
import by.epam.library.entity.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.impl.BookServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class DeleteBook implements Command {
    public static final String COMMA = ",";

    private final static BookServiceImpl service = BookServiceImpl.getInstance();
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return JspPageName.SIGN_IN_AGAIN_PAGE;
        }
        boolean isDelete = false;
        String page = null;
        List<Book> books = getBooks(request);
        try {
            isDelete = service.delete(books);
        } catch (ServiceException e) {
            throw new CommandException("DeleteBook Command Exception",e);
        }
        if(isDelete){
            page = JspPageName.ADMIN_BOOKS_PAGE;
        }
        else{
            response.setStatus(404);
            return JspPageName.ERROR_PAGE;
        }
        return page;
    }
    private static List<Book> getBooks(HttpServletRequest request) {
        List<Book> books = new ArrayList<>();
        String strBookId = request.getParameter(RequestParameterName.BOOK_ID);
        if (!strBookId.isEmpty()) {
        String[] arrBookId = strBookId.split(COMMA);
        for (int i = 0; i < arrBookId.length; i++) {
            Book currentBook = new Book();
            currentBook.setBookId(Integer.valueOf(arrBookId[i]));
            books.add(currentBook);
        }
    }
        return books;
    }
}
