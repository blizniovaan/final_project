package by.epam.library.command;

import by.epam.library.command.impl.user_command.authorization.SetSessionAttribute;
import by.epam.library.command.impl.admin_command.author_command.AddAuthor;
import by.epam.library.command.impl.admin_command.author_command.DeleteAuthor;
import by.epam.library.command.impl.admin_command.author_command.EditAuthor;
import by.epam.library.command.impl.admin_command.book_command.AddBook;
import by.epam.library.command.impl.admin_command.book_command.DeleteBook;
import by.epam.library.command.impl.admin_command.book_command.GiveBooks;
import by.epam.library.command.impl.admin_command.publisher_command.AddPublisher;
import by.epam.library.command.impl.admin_command.publisher_command.DeletePublisher;
import by.epam.library.command.impl.admin_command.publisher_command.EditPublisher;
import by.epam.library.command.impl.admin_command.user_command.DeleteUser;
import by.epam.library.command.impl.admin_command.user_command.GetInfoAboutUser;
import by.epam.library.command.impl.admin_command.user_command.SelectAllUsers;
import by.epam.library.command.impl.localization.Localization;
import by.epam.library.command.impl.navigation_command.*;
import by.epam.library.command.impl.user_command.*;
import by.epam.library.command.impl.user_command.authorization.Login;
import by.epam.library.command.impl.user_command.authorization.Registration;
import by.epam.library.command.impl.user_command.authorization.SelectUserPage;

import java.util.HashMap;
import java.util.Map;

public class CommandHelper {
    private static final String DASH = "-";
    private static final String UNDERLINE = "_";
    private static final CommandHelper instance = new CommandHelper();
    private Map<CommandName,Command> commands = new HashMap<>();

    public CommandHelper(){
        commands.put(CommandName.LOGIN,new Login());
        commands.put(CommandName.REGISTRATION,new Registration());
        commands.put(CommandName.SELECT_ALL_USERS,new SelectAllUsers());
        commands.put(CommandName.GO_TO,new GoTo());
        commands.put(CommandName.LOCALIZATION,new Localization());
        commands.put(CommandName.SELECT_ALL_BOOKS_BY_LETTERS_AND_GENRE,new SelectAllBooksByLettersAndGenre());
        commands.put(CommandName.GET_BOOK_INFO,new GetBookInfo() );
        commands.put(CommandName.TAKE_BOOK,new TakeBook());
        commands.put(CommandName.GO_TO_GENRE_PAGE,new GoToGenrePage());
        commands.put(CommandName.SELECT_ALL_BOOKS_BY_AUTHOR,new SelectAllBooksByAuthor());
        commands.put(CommandName.SELECT_AVAILABLE_BOOKS_BY_AUTHOR,new SelectAvailableBooksByAuthor());
        commands.put(CommandName.SELECT_AVAILABLE_BOOKS_BY_ENTER_WORD,new SelectAvailableBooksByEnterWord());
        commands.put(CommandName.SELECT_ALL_BOOKS_BY_ENTER_WORD,new SelectAllBooksByEnterWord());
        commands.put(CommandName.GO_TO_CARD,new GoToCard());
        commands.put(CommandName.SELECT_AVAILABLE_BOOKS_BY_LETTER,new SelectAvailableBooksByLetter());
        commands.put(CommandName.SELECT_ALL_BOOKS_BY_LETTER,new SelectAllBooksByLetter());
        commands.put(CommandName.RETURN_BOOK,new ReturnBook());
        commands.put(CommandName.SELECT_USER_PAGE,new SelectUserPage());
        commands.put(CommandName.SELECT_ALL_AVAILABLE_BOOKS,new SelectAllAvailableBooks());
        commands.put(CommandName.SELECT_ALL_BOOKS,new SelectAllBooks());
        commands.put(CommandName.DELETE_BOOK,new DeleteBook());
        commands.put(CommandName.GO_TO_DELETE_BOOK_PAGE,new GoToDeleteBookPage());
        commands.put(CommandName.GO_TO_ADD_BOOK_PAGE,new GoToAddBookPage());
        commands.put(CommandName.ADD_BOOK,new AddBook());
        commands.put(CommandName.GO_TO_EDIT_GENRE_PAGE,new GoToEditGenrePage());
        commands.put(CommandName.GO_TO_EDIT_AUTHOR_PAGE,new GoToEditAuthorPage());
        commands.put(CommandName.GO_TO_EDIT_PUBLISHER_PAGE,new GoToEditPublisherPage());
        commands.put(CommandName.GO_TO_ADMIN_BOOKS_PAGE,new GoToAdminBooksPage());
        commands.put(CommandName.DELETE_USER,new DeleteUser());
        commands.put(CommandName.SELECT_BOOK,new SelectBook());
        commands.put(CommandName.SELECT_BOOK,new SelectBook());
        commands.put(CommandName.GO_TO_GIVE_BOOK_PAGE,new GoToGiveBookPage());
        commands.put(CommandName.GIVE_BOOKS,new GiveBooks());
        commands.put(CommandName.GET_INFO_ABOUT_USER,new GetInfoAboutUser());
        commands.put(CommandName.GO_TO_DELETE_USER_PAGE,new GoToDeleteUserPage());
        commands.put(CommandName.ADD_PUBLISHER,new AddPublisher());
        commands.put(CommandName.DELETE_PUBLISHER,new DeletePublisher());
        commands.put(CommandName.EDIT_PUBLISHER,new EditPublisher());
        commands.put(CommandName.SET_SESSION_ATTRIBUTE,new SetSessionAttribute());
        commands.put(CommandName.ADD_AUTHOR,new AddAuthor());
        commands.put(CommandName.DELETE_AUTHOR,new DeleteAuthor());
        commands.put(CommandName.EDIT_AUTHOR,new EditAuthor());
    }
    public static CommandHelper getInstance(){return  instance;}
    public Command getCommand(String commandName){
        CommandName name = CommandName.valueOf(commandName.toUpperCase().trim().replace(DASH,UNDERLINE));
        Command command;
        if ( null != name){
            command = commands.get(name);
        } else{
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }
        return command;
    }
}
