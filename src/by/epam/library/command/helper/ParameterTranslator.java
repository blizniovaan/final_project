package by.epam.library.command.helper;

public final class ParameterTranslator {
    public static final String READING_ROOM = "Читательский зал";
    public static final String LIBRARY_CARD = "Читательский билет";

    public ParameterTranslator(){}
    public static String translate(String parameter) {
        Parameter preparedParameter = Parameter.valueOf(parameter);
        switch (preparedParameter){
            case READING_ROOM:
                return ParameterTranslator.READING_ROOM;
            case LIBRARY_CARD:
                return ParameterTranslator.LIBRARY_CARD;
            default:
                return null;
        }
    }

    enum Parameter{
        READING_ROOM,LIBRARY_CARD
    }
}
