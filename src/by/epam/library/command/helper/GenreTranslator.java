package by.epam.library.command.helper;

public final class GenreTranslator {
    public static final String MILITARY_SCIENCE = "Военное дело";
    public static final String BUSINESS_LITERATURE = "Деловая литература";
    public static final String DETECTIVE = "Детективы и триллеры";
    public static final String CHILDREN = "Детское";
    public static final String DOCUMENTARY = "Документальная литература";
    public static final String HOUSEKEEPING = "Домоводство";
    public static final String DRAMATIC_ART = "Драматургия";
    public static final String COMPUTERS = "Компьютеры и интернет";
    public static final String ROMANCE_NOVELS = "Любовные романы";
    public static final String EDUCATION = "Наука,образование";
    public static final String POETRY = "Поэзия";
    public static final String ADVENTURES = "Приключения";
    public static final String PROSE = "Проза";
    public static final String RELIGION = "Религия";
    public static final String FANTASY = "Фантастика";
    public static final String FOLKLORE = "Фольклор";
    public static final String HUMOUR = "Юмор";

    public GenreTranslator(){}
    public static String translate(String genre){
        GenreTitle genreTitle = GenreTitle.valueOf(genre.trim().toUpperCase());
        switch (genreTitle){
            case MILITARY_SCIENCE:{
                return MILITARY_SCIENCE;
            }
            case BUSINESS_LITERATURE:{
                return BUSINESS_LITERATURE;
            }
            case DETECTIVE:{
                return DETECTIVE;
            }
            case CHILDREN:{
                return CHILDREN;
            }
            case DOCUMENTARY:{
                return DOCUMENTARY;
            }
            case HOUSEKEEPING:{
                return HOUSEKEEPING;
            }
            case DRAMATIC_ART:{
                return DRAMATIC_ART;
            }
            case COMPUTERS:{
                return COMPUTERS;
            }
            case ROMANCE_NOVELS:{
                return ROMANCE_NOVELS;
            }
            case EDUCATION:{
                return EDUCATION;
            }
            case POETRY:{
                return POETRY;
            }
            case PROSE:{
                return PROSE;
            }
            case ADVENTURES:{
                return ADVENTURES;
            }
            case RELIGION:{
                return RELIGION;
            }
            case FANTASY:{
                return FANTASY;
            }
            case FOLKLORE:{
                return FOLKLORE;
            }
            case HUMOUR:{
                return HUMOUR;
            }
            default:{
                return null;
            }
        }
    }
}
