package by.epam.library.command.helper;

import by.epam.library.controller.SessionParameterName;

import javax.servlet.http.HttpSession;

public final class DoubleRequestAnalyser {
    public DoubleRequestAnalyser(){}
    public static boolean checkDoubleRequest(HttpSession session){
        boolean isValid = true;
        Double prevNumber = (Double)session.getAttribute(SessionParameterName.PREV_NUMBER);
        Double curNumber = (Double)session.getAttribute(SessionParameterName.CURRENT_RANDOM_NUMBER);
        if(prevNumber != null){
            if(prevNumber.equals(curNumber)){
                isValid = false;
            } else {
                session.setAttribute(SessionParameterName.PREV_NUMBER,curNumber);
            }
        }
        else {
            Double randomNumber = Randomizer.getRandomNumber();
            session.setAttribute(SessionParameterName.CURRENT_RANDOM_NUMBER,randomNumber);
        }
        return isValid;
    }
}
