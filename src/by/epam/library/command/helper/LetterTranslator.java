package by.epam.library.command.helper;

public final class LetterTranslator {
    public static final String A = "А";
    public static final String B = "Б";
    public static final String V = "В";
    public static final String G = "Г";
    public static final String D = "Д";
    public static final String E = "Е";
    public static final String Zh = "Ж";
    public static final String Z = "З";
    public static final String I = "И";
    public static final String K = "К";
    public static final String L = "Л";
    public static final String M = "М";
    public static final String N = "Н";
    public static final String O = "О";
    public static final String P = "П";
    public static final String R = "Р";
    public static final String S = "С";
    public static final String T = "Т";
    public static final String U = "У";
    public static final String F = "Ф";
    public static final String Kh = "Х";
    public static final String Ts = "Ц";
    public static final String Ch = "Ч";
    public static final String Sh = "Ш";
    public static final String Shch = "Щ";
    public static final String Y = "Ы";
    public static final String Ia = "Я";


    public static String translate(int numberOfLetter) {
        switch (numberOfLetter) {
            case 1: {
                return A;
            }
            case 2: {
                return B;
            }
            case 3: {
                return V;
            }
            case 4: {
                return G;
            }
            case 5: {
                return D;
            }
            case 6: {
                return E;
            }
            case 7: {
                return Zh;
            }
            case 8: {
                return Z;
            }
            case 9: {
                return I;
            }
            case 10: {
                return K;
            }
            case 11: {
                return L;
            }
            case 12: {
                return M;
            }
            case 13: {
                return N;
            }
            case 14: {
                return O;
            }
            case 15: {
                return P;
            }
            case 16: {
                return R;
            }
            case 17: {
                return S;
            }
            case 18: {
                return T;
            }
            case 19: {
                return U;
            }
            case 20: {
                return F;
            }
            case 21: {
                return Kh;
            }
            case 22: {
                return Ts;
            }
            case 23: {
                return Ch;
            }
            case 24: {
                return Sh;
            }
            case 25: {
                return Shch;
            }
            case 26: {
                return Y;
            }
            case 27: {
                return Ia;
            }
            default: {
                return A;
            }
        }
    }
}
