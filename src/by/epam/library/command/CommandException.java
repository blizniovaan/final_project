package by.epam.library.command;


public class CommandException extends Exception {
    private static final long serialVersionUID = 1L;

    public CommandException(String msg){
        super(msg);
    }
    public CommandException(Exception e){
        super(e);
    }
    public CommandException(String msg, Exception e){
        super(msg, e);
    }
}
