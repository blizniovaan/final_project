package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {
    /**
     * This method is used to execute the command.
     * @return String    the page,which will be open
     * @throws CommandException
     */
    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
