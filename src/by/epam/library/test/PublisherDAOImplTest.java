import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.dao.impl.BookDAOImpl;
import by.epam.library.dao.impl.PublisherDAOImpl;
import by.epam.library.entity.Book;
import by.epam.library.entity.Publisher;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by HP on 20.03.2016.
 */
public class PublisherDAOImplTest {
    private static ConnectionPool pool = null;
    private static PublisherDAOImpl publisherDAO;
    private static BookDAOImpl bookDao = null;
    private static Publisher publisher;
    private static List<Publisher> publishers;
    private static Publisher insertedPublisher;
    private static List<Book> books;
    private static Book book;
    private  static Book insertedBook;
    @BeforeClass
    public static void init() throws DAOException {
        ConnectionPool.getInstance().initialize();
        publisherDAO = PublisherDAOImpl.getInstance();
        bookDao = BookDAOImpl.getInstance();
        pool = ConnectionPool.getInstance();
        createPublisher();
        createBook();
    }
    @Test
    public void testSelectAll() throws DAOException {
        Assert.assertNotNull(publisherDAO.selectAll());
    }
    @Test
    public void testSelectBookPublisher() throws DAOException {
        Assert.assertNotNull(publisherDAO.select(insertedBook));
    }
    @Test
    public void testDelete() throws DAOException {
        assertEquals(true, publisherDAO.delete(publishers));
        createPublisher();
    }
    @AfterClass
    public static void dispose() throws DAOException, ConnectionException {
        bookDao.delete(books);
        publisherDAO.delete(publisher);
        pool.releasePool();
    }
    private static void createPublisher() throws DAOException {
        publishers = new ArrayList<>();
        publisher = new Publisher();
        insertedPublisher = publisherDAO.insert(publisher);
        publishers.add(insertedPublisher);
    }
    private static void createBook() throws DAOException {
        books = new ArrayList<>();
        book = new Book();
        insertedBook = bookDao.insert(book);
        books.add(insertedBook);
    }
}
