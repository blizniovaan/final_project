import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.dao.impl.*;
import by.epam.library.entity.Book;
import by.epam.library.entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 20.03.2016.
 */
public class GeneralDAOTest {
    public static final String NAME = "Name";
    public static final String SURNAME = "Surname";
    public static final String LOGIN = "Login2016";
    public static final String PASSWORD = "Password2016";
    public static final String ROLE = "user";
    private static ConnectionPool pool = null;
    private static GeneralDao generalDao;
    private static BookDAOImpl bookDao = null;
    private static UserDAOImpl userDAO = null;
    private static Book book;
    private static List<Book> books;
    private static Book insertedBook;
    private static User user;
    private static User insertedUser;
    private static List<User> users;
    private final static int STATUS = 2;

    @BeforeClass
    public static void init() throws DAOException {
        ConnectionPool.getInstance().initialize();
        generalDao = GeneralDao.getInstance();
        bookDao = BookDAOImpl.getInstance();
        userDAO = UserDAOImpl.getInstance();
        pool = ConnectionPool.getInstance();
        createBook();
        createUser();
    }
    @Test
    public void testUpdateBookStatus() throws DAOException {
        Assert.assertNotNull(generalDao.updateBookStatus(books, STATUS));
    }
    @Test
       public void testGetCard() throws DAOException {
        Assert.assertNotNull(generalDao.getCard(insertedUser));
    }
    @Test
    public void testGetAuthors() throws DAOException {
        Assert.assertNotNull(generalDao.getAuthors(insertedBook));
    }
    @AfterClass
    public static void dispose() throws DAOException, ConnectionException {
        bookDao.delete(books);
        userDAO.delete(users);
        pool.releasePool();
    }
    private static void createBook() throws DAOException {
        books = new ArrayList<>();
        book = new Book();
        insertedBook = bookDao.insert(book);
        books.add(insertedBook);
    }
    private static void createUser() throws DAOException {
        users = new ArrayList<>();
        user = new User();
        user.setRole(ROLE);
        user.setName(NAME);
        user.setSurname(SURNAME);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        insertedUser = userDAO.insert(user);
        users.add(insertedUser);
    }
}
