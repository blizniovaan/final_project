import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.dao.impl.UserDAOImpl;
import by.epam.library.entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by HP on 20.03.2016.
 */
public class UserDAOImplTest {
    public static final String NAME = "Name";
    public static final String SURNAME = "Surname";
    public static final String LOGIN = "Login2016";
    public static final String PASSWORD = "Password2016";
    public static final String ROLE = "user";
    private static ConnectionPool pool = null;
    private static UserDAOImpl userDao = null;
    private static User user ;
    private static List<User> users ;
    private static User insertedUser ;

    @BeforeClass
    public static void init() throws DAOException {
        ConnectionPool.getInstance().initialize();
        userDao = UserDAOImpl.getInstance();
        pool = ConnectionPool.getInstance();
        createUser();
    }
    @Test
    public void testSignIn() throws DAOException {
        Assert.assertNotNull(userDao.signIn(insertedUser));
    }
    @Test
    public void testSelectAll() throws DAOException {
        Assert.assertNotNull(userDao.selectAll());
    }
    @Test
    public void testSelectOne() throws DAOException {
        Assert.assertNotNull(userDao.selectOne(insertedUser));
    }
    @Test
    public void testDelete() throws DAOException {
        assertEquals(true, userDao.delete(users));
        createUser();
    }
    @AfterClass
    public static void dispose() throws DAOException, ConnectionException {
        userDao.delete(users);
        pool.releasePool();
    }
    private static void createUser() throws DAOException {
        user = new User();
        user.setRole(ROLE);
        user.setName(NAME);
        user.setSurname(SURNAME);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        users = new ArrayList<>();
        users.add(user);
        insertedUser = userDao.insert(user);
    }
}
