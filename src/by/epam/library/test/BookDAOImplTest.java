import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.dao.impl.BookDAOImpl;
import by.epam.library.dao.impl.HistoryDaoImpl;
import by.epam.library.dao.impl.UserDAOImpl;
import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by HP on 20.03.2016.
 */
public class BookDAOImplTest {
    public static final String NAME = "Name";
    public static final String SURNAME = "Surname";
    public static final String LOGIN = "Login2016";
    public static final String PASSWORD = "Password2016";
    public static final String ROLE = "user";
    public static final String PLACE = "Читательский билет";
    private static ConnectionPool pool = null;
    private static BookDAOImpl bookDao = null;
    private static UserDAOImpl userDao = null;
    private static HistoryDaoImpl historyDao = null;
    private static Book book;
    private static List<Book> books;
    private static Book insertedBook;

    private static History record;

    private static History insertedRecord;

    private static User user;
    private static User insertedUser;
    private static List<User> users;

    private static List<History> records;

    @BeforeClass
    public static void init() throws DAOException {
        ConnectionPool.getInstance().initialize();
        bookDao = BookDAOImpl.getInstance();
        userDao = UserDAOImpl.getInstance();
        historyDao  = HistoryDaoImpl.getInstance();
        pool = ConnectionPool.getInstance();
        createBook();
        createUser();
        createRecord();
    }
    @Test
    public void testSelectAll() throws DAOException {
        Assert.assertNotNull(bookDao.selectAll());
    }
    @Test
    public void testSelectOne() throws DAOException {
        Assert.assertNotNull(bookDao.selectOne(insertedBook));
    }
    @Test
    public void testDelete() throws DAOException {
        assertEquals(true, bookDao.delete(books));
        createBook();
    }
    @Test
    public void testReturnBook() throws DAOException {
        Assert.assertNotNull(bookDao.returnBooks(records));
    }
    @Test
    public void testGetBooksFromCard() throws DAOException {
        Assert.assertNotNull(bookDao.getBooksFromCard(user));
    }

    @AfterClass
    public static void dispose() throws DAOException, ConnectionException {
        historyDao.delete(record);
        bookDao.delete(books);
        userDao.delete(users);
        pool.releasePool();
    }

    private static void createBook() throws DAOException {
        books = new ArrayList<>();
        book = new Book();
        insertedBook = bookDao.insert(book);
        books.add(insertedBook);
    }
    private static void createUser() throws DAOException {
        users = new ArrayList<>();
        user = new User();
        user.setRole(ROLE);
        user.setName(NAME);
        user.setSurname(SURNAME);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        insertedUser = userDao.insert(user);
        users.add(insertedUser);
    }
    private static void createRecord() throws DAOException {
        records = new ArrayList<>();
        record = new History();
        Date takingDate = Calendar.getInstance().getTime();
        record.setPlace(PLACE);
        record.setTakingDate(takingDate);
        record.setDeliveryDate(takingDate);
        record.setUser(insertedUser);
        record.setBook(insertedBook);
        insertedRecord = historyDao.insert(record);
        record.setRecordId(insertedRecord.getRecordId());
        records.add(record);
    }

}
