package by.epam.library.entity;

import java.io.Serializable;
import java.util.Date;

public class History implements Serializable{
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final String PLACE = "place";

    private Integer recordId;
    private User user;
    private Card card;
    private Book book;
    private Date takingDate;
    private Date deliveryDate;
    private String place;

    public History(){
        this.recordId = ID;
        this.user = new User();
        this.card = new Card();
        this.book = new Book();
        this.place = PLACE;
        this.takingDate = null;
        this.deliveryDate = null;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getTakingDate() {
        return takingDate;
    }

    public void setTakingDate(Date takingDate) {
        this.takingDate = takingDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        History temp = (History) obj;
        if(!this.recordId.equals(temp.recordId)){
            return false;
        }
        if(!this.user.equals(temp.user)){
            return false;
        }
        if(!this.card.equals(temp.card)){
            return false;
        }
        if(!this.book.equals(temp.book)){
            return false;
        }
        if(!this.takingDate.equals(temp.takingDate)){
            return false;
        }
        if(!this.deliveryDate.equals(temp.deliveryDate)){
            return false;
        }
        if(!this.deliveryDate.equals(temp.deliveryDate)){
            return false;
        }
        if(!this.place.equals(temp.place)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.recordId.hashCode() + 5 * this.user.hashCode() + 7 * this.card.hashCode() +
                   9 * this.book.hashCode() + 3 * this.takingDate.hashCode() + 5 * this.deliveryDate.hashCode() +
                      7 * this.place.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(recordId);
        sb.append(user);
        sb.append(card);
        sb.append(book);
        sb.append(takingDate);
        sb.append(deliveryDate);
        sb.append(place);
        return sb.toString();
    }
}
