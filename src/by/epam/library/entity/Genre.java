package by.epam.library.entity;

import java.io.Serializable;

public class Genre implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final String TITLE = "title";

    private Integer genreId;
    private String title;

    public Genre(){
        this.genreId = ID;
        this.title = TITLE;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Genre temp = (Genre) obj;
        if(!this.genreId.equals(temp.genreId)){
            return false;
        }
        if(!this.title.equals(temp.title)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.genreId.hashCode() + 5 * this.title.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(genreId);
        sb.append(title);
        return sb.toString();
    }
}
