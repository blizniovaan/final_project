package by.epam.library.entity;

import java.io.Serializable;

public class Author implements Serializable{
    public static final String NAME = "name";
    public static final Integer ID = 1;

    private static final long serialVersionUID = 1L;
    private Integer authorId;
    private String name;

    public Author(){
        this.authorId = ID;
        this.name = NAME;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(authorId);
        str.append(name);
        return str.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Author temp = (Author) obj;
        if(!this.authorId.equals(temp.authorId)){
            return false;
        }
        if(!this.name.equals(temp.name)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.authorId.hashCode() + 5 * this.name.hashCode();
    }

}
