package by.epam.library.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Book implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final Integer EDITION_YEAR = 2000;
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";

    private Integer bookId;
    private Integer editionYear;
    private String title;

    private Set<Author> authors;
    private Genre genre;
    private Publisher publisher;
    private String description;

    private BookStatus status;

    public Book(){
        this.bookId = ID;
        this.editionYear = EDITION_YEAR;
        this.title = TITLE;
        this.authors = new HashSet<>();
        this.genre = new Genre();
        this.publisher = new Publisher();
        this.description = DESCRIPTION;
        this.status = new BookStatus();
    }


    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public BookStatus getStatus() {
        return status;
    }

    public void setStatus(BookStatus status) {
        this.status = status;
    }

    public Integer getEditionYear() {
        return editionYear;
    }

    public void setEditionYear(Integer editionYear) {
        this.editionYear = editionYear;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(bookId);
        str.append(publisher);
        str.append(editionYear);
        str.append(title);
        str.append(genre);
        str.append(authors);
        str.append(description);
        return str.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Book temp = (Book) obj;
        if(!this.bookId.equals(temp.bookId)){
            return false;
        }
        if(!this.editionYear.equals(temp.editionYear)){
            return false;
        }
        if(!this.title.equals(temp.title)){
            return false;
        }
        if(!this.publisher.equals(temp.publisher)){
            return false;
        }
        if(!this.authors.equals(temp.authors)){
            return false;
        }
        if(!this.genre.equals(temp.genre)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.bookId.hashCode() + 5 * this.editionYear.hashCode()  +
                     9 * this.title.hashCode() + 3 * this.authors.hashCode() +
                      5 * this.genre.hashCode() + 9*this.publisher.hashCode();
    }
}
