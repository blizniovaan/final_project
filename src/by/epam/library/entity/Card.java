package by.epam.library.entity;

import java.io.Serializable;
import java.util.List;

public class Card implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;

    private Integer cardId;
    private List<Book> books;
    private User user;

    public Card(){
        this.cardId = ID;
        this.user = new User();
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Card temp = (Card) obj;
        if(!this.cardId.equals(temp.cardId)){
            return false;
        }
        if(!this.user.equals(temp.user)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.cardId.hashCode() + 5 * this.user.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(cardId);
        sb.append(user);
        return sb.toString();
    }
}
