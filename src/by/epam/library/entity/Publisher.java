package by.epam.library.entity;

import java.io.Serializable;

public class Publisher implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final String TITLE = "title";

    private Integer publisherId;
    private String title;

    public Publisher(){
        this.publisherId = ID;
        this.title = TITLE;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publiserId) {
        this.publisherId = publiserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Publisher temp = (Publisher) obj;
        if(!this.publisherId.equals(temp.publisherId)){
            return false;
        }
        if(!this.title.equals(temp.title)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.publisherId.hashCode() + 5 * this.title.hashCode();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(publisherId);
        sb.append(title);
        return sb.toString();
    }
}
