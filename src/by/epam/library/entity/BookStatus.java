package by.epam.library.entity;

import java.io.Serializable;

public class BookStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final String STATUS = "status";

    private Integer statusId;
    private String status;

    public  BookStatus(){
        this.statusId = ID;
        this.status = STATUS;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        BookStatus temp = (BookStatus) obj;
        if(!this.statusId.equals(temp.statusId)){
            return false;
        }
        if(!this.status.equals(temp.status)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.statusId.hashCode() + 5 * this.status.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(statusId);
        sb.append(status);
        return sb.toString();
    }

}
