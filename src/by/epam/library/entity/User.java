package by.epam.library.entity;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final Integer ID = 1;
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String ROLE = "role";

    private Integer userId;
    private String login;
    private String password;
    private String name;
    private String surname;
    private String role;

    public User(){
        this.userId =ID;
        this.login = LOGIN;
        this.password = PASSWORD;
        this.name = NAME;
        this.surname = SURNAME;
        this.role = ROLE;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        User temp = (User) obj;
        if(!this.userId.equals(temp.name)){
            return false;
        }
        if(!this.login.equals(temp.login)){
            return false;
        }
        if(!this.password.equals(temp.password)){
            return false;
        }
        if(!this.name.equals(temp.surname)){
            return false;
        }
        if(!this.surname.equals(temp.surname)){
            return false;
        }
        if(!this.role.equals(temp.role)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 3 * this.userId.hashCode() + 5 * this.login.hashCode() + 7 * this.password.hashCode() +
                9 * this.name.hashCode() + 3 * this.surname.hashCode() + 5 * this.role.hashCode();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append(login);
        sb.append(password);
        sb.append(name);
        sb.append(surname);
        sb.append(role);
        return sb.toString();
    }
}
