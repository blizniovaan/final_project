package by.epam.library.service.impl;

import by.epam.library.entity.Card;
import by.epam.library.service.CardService;
import by.epam.library.service.ServiceException;

import java.util.List;

public class CardServiceImpl implements CardService {

    @Override
    public List<Card> selectAll() throws ServiceException {
        return null;
    }

    @Override
    public Card selectOne(Card entity) throws ServiceException {
        return null;
    }

    @Override
    public Card insert(Card entity) throws ServiceException {
        return null;
    }

    @Override
    public boolean delete(Card entity) {
           return false;
    }

    @Override
    public boolean delete(List<Card> list) throws ServiceException {
        return false;
    }

    @Override
    public Card update(Card entity) {
        return null;
    }
}
