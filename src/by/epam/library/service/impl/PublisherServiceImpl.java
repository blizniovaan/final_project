package by.epam.library.service.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.PublisherDao;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.entity.Publisher;
import by.epam.library.service.PublisherService;
import by.epam.library.service.ServiceException;
import by.epam.library.service.validator.Validator;

import java.util.List;

public class PublisherServiceImpl implements PublisherService {

    private static final PublisherServiceImpl instance = new PublisherServiceImpl();
    private PublisherServiceImpl(){}
    public static PublisherServiceImpl getInstance() {  return instance;  }

    private PublisherDao dao = SQLDAOFactory.getInstance().getPublisherDAO();
    Validator validator = new Validator();

    @Override
    public List<Publisher> selectAll() throws ServiceException {
        List<Publisher> publishers = null;
        try {
            publishers = dao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("PublisherServiceImpl Exception",e);
        }
        return publishers;
    }

    @Override
    public Publisher insert(Publisher publisher) throws ServiceException {
       Publisher inseredPublisher = null;
        if(validator.validateAddPublisherForm(publisher)) {
            try {
                inseredPublisher = dao.insert(publisher);
            } catch (DAOException e) {
                throw new ServiceException("PublisherServiceImpl Exception", e);
            }
        }
        return inseredPublisher;
    }

    @Override
    public boolean delete(List<Publisher> publishers) throws ServiceException {
        boolean isDelete = false;
        if(validator.validateDeletePublisherForm(publishers)) {
            try {
                isDelete = dao.delete(publishers);
            } catch (DAOException e) {
                throw new ServiceException("PublisherServiceImpl Exception", e);
            }
        }
        return isDelete;
    }

    @Override
    public Publisher update(Publisher publisher) throws ServiceException {
        Publisher updatedPublisher = null;
        if(validator.validateEditPublisherForm(publisher)) {
            try {
                updatedPublisher = dao.update(publisher);
            } catch (DAOException e) {
                throw new ServiceException("PublisherServiceImpl Exception", e);
            }
        }
        return updatedPublisher;
    }

    @Override
    public Publisher selectOne(Publisher entity) throws ServiceException {
        return null;
    }

    @Override
    public boolean delete(Publisher entity) throws ServiceException {
        return false;
    }

}
