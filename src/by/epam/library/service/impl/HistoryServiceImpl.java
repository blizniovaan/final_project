package by.epam.library.service.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.HistoryDao;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.service.HistoryService;
import by.epam.library.service.ServiceException;
import by.epam.library.service.validator.Validator;

import java.util.List;

public class HistoryServiceImpl implements HistoryService {

    private static final HistoryServiceImpl instance = new HistoryServiceImpl();
    private HistoryServiceImpl(){}
    public static HistoryServiceImpl getInstance() {  return instance;  }

    private HistoryDao dao = SQLDAOFactory.getInstance().getHistoryDAO();
    Validator validator = new Validator();

    @Override
    public History insert(History record) throws ServiceException {
        History inseredRecord = null;
        try {
            inseredRecord = dao.insert(record);
        } catch (DAOException e) {
            throw new ServiceException("HistoryServiceImpl Exception",e);
        }
        return inseredRecord;
    }

    @Override
    public List<History> insertPart(History history, List<Book> books) throws ServiceException {
        List<History> addedRecords = null;
        if(validator.validateGiveBookForm(books,history.getUser(),history.getPlace())) {
            try {
                addedRecords = dao.insertPart(history, books);
            } catch (DAOException e) {
                throw new ServiceException("HistoryServiceImpl Exception", e);
            }
        }
        return addedRecords;
    }

    @Override
    public boolean delete(History entity) throws ServiceException {
        return false;
    }

    @Override
    public boolean delete(List<History> list) throws ServiceException {
        return false;
    }

    @Override
    public History update(History entity) throws ServiceException {
        return null;
    }


    @Override
    public List<History> selectAll() throws ServiceException {
        return null;
    }

    @Override
    public History selectOne(History entity) throws ServiceException {
        return null;
    }


}
