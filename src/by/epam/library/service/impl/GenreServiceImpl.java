package by.epam.library.service.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.GenreDao;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.entity.Genre;
import by.epam.library.service.GenreService;
import by.epam.library.service.ServiceException;

import java.util.List;

public class GenreServiceImpl implements GenreService{

    private static final GenreServiceImpl instance = new GenreServiceImpl();
    private GenreServiceImpl(){}
    public static GenreServiceImpl getInstance() {  return instance;  }

    private GenreDao dao = SQLDAOFactory.getInstance().getGenreDAO();

    @Override
    public List<Genre> selectAll() throws ServiceException {
        List<Genre> genres = null;
        try {
            genres = dao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("GenreServiceImpl Exception",e);
        }
        return genres;
    }

    @Override
    public Genre insert(Genre genre) throws ServiceException {
        Genre inseredGenre = null;
        try {
            inseredGenre = dao.insert(genre);
        } catch (DAOException e) {
            throw new ServiceException("GenreServiceImpl Exception",e);
        }
        return inseredGenre;
    }

    @Override
    public boolean delete(List<Genre> list) throws ServiceException {
       boolean isDelete = false;
        try {
            isDelete = dao.delete(list);
        } catch (DAOException e) {
            throw new ServiceException("GenreServiceImpl Exception",e);
        }
        return isDelete;
    }

    @Override
    public Genre update(Genre genre) throws ServiceException {
        Genre editedGenre = null;
        try {
            editedGenre = dao.update(genre);
        } catch (DAOException e) {
            throw new ServiceException("GenreServiceImpl Exception",e);
        }
        return editedGenre;
    }

    @Override
    public Genre selectOne(Genre entity) throws ServiceException {
        return null;
    }

    @Override
    public boolean delete(Genre entity) throws ServiceException {
        return false;
    }

}
