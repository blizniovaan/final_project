package by.epam.library.service.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.dao.UserDao;
import by.epam.library.entity.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.UserService;
import by.epam.library.service.validator.Validator;

import java.util.List;

public class UserServiceImpl implements UserService{
    private static final UserServiceImpl instance = new UserServiceImpl();
    private UserServiceImpl(){}
    public static UserServiceImpl getInstance() {
        return instance;
    }
    private Validator validator = new Validator();

    private UserDao dao = SQLDAOFactory.getInstance().getUserDAO();

    @Override
    public List<User> selectAll() throws ServiceException {
        List<User> users = null;
        try {
            users = dao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("UserServiceImpl Exception",e);
        }
        return users;
    }

    @Override
    public User selectOne(User user) throws ServiceException {
        User selectedUser = null;
        try {
            selectedUser = dao.selectOne(user);
        } catch (DAOException e) {
            throw new ServiceException("UserServiceImpl Exception",e);
        }
        return selectedUser;
    }

    @Override
    public User insert(User user,String rep_password) throws ServiceException {
        User currentUser  = null;
        if(validator.validateSignUpForm(user,rep_password)) {
            try {
                currentUser = dao.insert(user);
            } catch (DAOException e) {
                throw new ServiceException("UserServiceImpl Exception", e);
            }
        }
        return currentUser;
    }

    @Override
    public boolean delete(List<User> list) throws ServiceException {
        boolean isDelete = false;
        if(validator.validateDeleteUserForm(list)) {
            try {
                isDelete = dao.delete(list);
            } catch (DAOException e) {
                throw new ServiceException("UserServiceImpl Exception", e);
            }
        }
        return isDelete;
    }

    public User signIn(User user) throws ServiceException {
        User currentUser = null;
        if(validator.validateSignInForm(user)) {
            try {
                currentUser = dao.signIn(user);
            } catch (DAOException e) {
                throw new ServiceException("UserServiceImpl Exception", e);
            }
        }
        return currentUser;
    }

    @Override
    public User update(User entity) {
        return null;
    }

    @Override
    public boolean delete(User entity) {
        return false;
    }

    @Override
    public User insert(User entity) throws ServiceException {
        return null;
    }

}
