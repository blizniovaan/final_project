package by.epam.library.service.impl;

import by.epam.library.dao.AuthorDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.entity.Author;
import by.epam.library.service.AuthorService;
import by.epam.library.service.ServiceException;
import by.epam.library.service.validator.Validator;

import java.util.List;

public class AuthorServiceImpl implements AuthorService {

    private static final AuthorServiceImpl instance = new AuthorServiceImpl();
    private AuthorServiceImpl(){}
    public static AuthorServiceImpl getInstance() {  return instance;  }

    private AuthorDao dao = SQLDAOFactory.getInstance().getAuthorDAO();
    Validator validator = new Validator();
    @Override
    public List<Author> selectAll() throws ServiceException {
        List<Author> authors = null;
        try {
            authors = dao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("AuthorServiceImpl Exception",e);
        }
        return authors;
    }

    @Override
    public Author insert(Author author) throws ServiceException {
        Author insertedAuthor = null;
        if(validator.validateAddAuthorForm(author)){
            try {
                insertedAuthor = dao.insert(author);
            } catch (DAOException e) {
                throw new ServiceException("PublisherServiceImpl Exception",e);
            }
        }
        return insertedAuthor;
    }

    @Override
    public boolean delete(List<Author> authors) throws ServiceException {
        boolean isDelete = false;
        if(validator.validateDeleteAuthorForm(authors)) {
            try {
                isDelete = dao.delete(authors);
            } catch (DAOException e) {
                throw new ServiceException("AuthorServiceImpl Exception", e);
            }
        }
        return isDelete;
    }

    @Override
    public Author update(Author author) throws ServiceException {
        Author updatedAuthor = null;
        if(validator.validateEditAuthorForm(author)) {
            try {
                updatedAuthor = dao.update(author);
            } catch (DAOException e) {
                throw new ServiceException("AuthorServiceImpl Exception", e);
            }
        }
        return updatedAuthor;
    }

    @Override
    public boolean delete(Author entity) throws ServiceException {
        return false;
    }

    @Override
    public Author selectOne(Author entity) throws ServiceException {
        return null;
    }

}
