package by.epam.library.service.impl;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.SQLDAOFactory;
import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import by.epam.library.service.BookService;
import by.epam.library.service.ServiceException;
import by.epam.library.service.validator.Validator;

import java.util.List;

public class BookServiceImpl implements BookService {

    private static final BookServiceImpl instance = new BookServiceImpl();
    private BookServiceImpl(){}
    public static BookServiceImpl getInstance() {  return instance;  }

    private BookDao dao = SQLDAOFactory.getInstance().getBookDAO();
    Validator validator = new Validator();

    @Override
    public List<History> returnBooks(List<History> records) throws ServiceException {
        List<History> list = null;
        if(validator.validateReturnBookForm(records)) {
            try {
                list = dao.returnBooks(records);
            } catch (DAOException e) {
                throw new ServiceException("BookServiceImpl Exception", e);
            }
        }
        return list;
    }

    @Override
    public List<Book> selectAll() throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAll();
        } catch (DAOException e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAllAvailable() throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAllAvailable();
        } catch (DAOException e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public Book selectOne(Book book) throws ServiceException {
        try {
            book = dao.selectOne(book);
        } catch (DAOException e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return book;
    }

    @Override
    public Book insert(Book book) throws ServiceException {
        Book inseredBook = null;
        if(validator.validateAddForm(book)) {
            try {
                inseredBook = dao.insert(book);
            } catch (DAOException e) {
                throw new ServiceException("BookServiceImpl Exception", e);
            }
        }
        return inseredBook;
    }

    @Override
    public boolean delete(List<Book> removableBook) throws ServiceException {
        boolean isDelete = false;
        if(validator.validateDeleteBookForm(removableBook)) {
            try {
                isDelete = dao.delete(removableBook);
            } catch (DAOException e) {
                throw new ServiceException("BookServiceImpl Exception", e);
            }
        }
        return isDelete;
    }

    @Override
    public Book update(Book entity) {
    return null;
    }

    @Override
    public List<Book> selectAvailableBookByLetter(String letterFrom,  String genre) throws  ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAvailableBookByLetter(letterFrom,genre);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAllBooksByLetter(String letterFrom, String genre) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAllBooksByLetter(letterFrom, genre);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }


    @Override
    public List<Book> selectAllBooksByAuthor(String letterFrom) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAllBooksByAuthor(letterFrom);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAvailableBooksByAuthor(String letterFrom) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAvailableBooksByAuthor(letterFrom);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAvailableByEnterWord(String enterWord) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAvailableByEnterWord(enterWord);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAllByEnterWord(String enterWord) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAllByEnterWord(enterWord);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<Book> selectAvailableBooksByLetter(String letterFrom) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAvailableBooksByLetter(letterFrom);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public List<History> getBooksFromCard(User user) throws ServiceException {
        List<History> records = null;
        try {
            records = dao.getBooksFromCard(user);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return records;
    }

    @Override
    public List<Book> selectAllBooksByLetter(String letterFrom) throws ServiceException {
        List<Book> books = null;
        try {
            books = dao.selectAllBooksByLetter(letterFrom);
        } catch (DAOException  e) {
            throw new ServiceException("BookServiceImpl Exception",e);
        }
        return books;
    }

    @Override
    public boolean delete(Book entity) throws ServiceException {
        return false;
    }

}
