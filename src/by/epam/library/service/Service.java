package by.epam.library.service;

import java.util.List;

public interface Service<T> {
    List<T> selectAll() throws ServiceException;

    T selectOne(T entity) throws ServiceException;

    T insert(T entity) throws ServiceException;

    boolean delete(T entity) throws ServiceException;

    boolean delete(List<T> list) throws ServiceException;

    T update(T entity) throws ServiceException;
}
