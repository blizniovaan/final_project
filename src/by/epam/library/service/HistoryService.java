package by.epam.library.service;

import by.epam.library.entity.Book;
import by.epam.library.entity.History;

import java.util.List;

public interface HistoryService extends Service<History> {
    @Override
    List<History> selectAll() throws ServiceException;

    @Override
    History selectOne(History entity) throws ServiceException;

    @Override
     History insert(History entity) throws ServiceException;

    @Override
     boolean delete(History entity) throws ServiceException ;


    @Override
     boolean delete(List<History> list) throws ServiceException;

    @Override
    History update(History entity) throws ServiceException;

    List<History> insertPart(History entity, List<Book> books) throws ServiceException;
}
