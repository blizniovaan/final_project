package by.epam.library.service.validator;

import by.epam.library.entity.User;

import java.util.List;

public interface UserValidatable extends Validatable{
    boolean validateSignInForm(User user);
    boolean validateSignUpForm(User user, String rep_password);
    boolean validateDeleteUserForm(List<User> users);
}
