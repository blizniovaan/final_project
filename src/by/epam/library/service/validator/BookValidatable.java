package by.epam.library.service.validator;

import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;

import java.util.List;

public interface BookValidatable  extends Validatable{
    boolean validateAddForm(Book object);
    boolean validateDeleteBookForm(List<Book> books);
    boolean validateGiveBookForm(List<Book> books, User user, String place);
    boolean validateReturnBookForm(List<History> records);
}