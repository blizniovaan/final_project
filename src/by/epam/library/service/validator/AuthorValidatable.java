package by.epam.library.service.validator;

import by.epam.library.entity.Author;

import java.util.List;

public interface AuthorValidatable extends Validatable{
    boolean validateDeleteAuthorForm(List<Author> list);
    boolean validateAddAuthorForm(Author object);
    boolean validateEditAuthorForm(Author object);
}
