package by.epam.library.service.validator;

import by.epam.library.entity.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator implements UserValidatable,BookValidatable,PublisherValidatable,AuthorValidatable{
    public static final String NAME_PATTERN = "^[A-ZА-Я][A-ZА-Яа-яa-z-]{1,18}";
    public static final String SURNAME_PATTERN = "^[A-ZА-Я][A-ZА-Яа-яa-z-]{1,18}";
    public static final String LOGIN_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,18}$";
    public static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,18}$";

    public static final String EDITION_YEAR_PATTERN = "[0-9]{3,4}";
    public static final String AUTHOR_NAME_PATTERN = "^[A-ZА-Я][A-ZА-Яа-яa-z-]{1,18}";
    public static final String BOOK_TITLE_PATTERN = "[A-Za-zА-Яа-я-0-9\\s]{1,100}";
    public static final String DESCRIPTION_PATTERN = "[A-Za-zА-Яа-я\\d\\s()\\-\\w]{0,850}";
    public static final String PUBLISHER_TITLE_PATTERN = "([A-Za-zА-Яа-я]+[A-Za-zА-Яа-я\\d\\s()\\-\\w]+){2,50}";
    public static final String ID_PATTERN = "[\\d]{0,11}";
    public static final String PLACE = "[A-Za-zА-Яа-я-\\s]{1,30}";
    public static final int MIN_YEAR = 500;
    public static final int MAX_YEAR = 2016;

    @Override
    public boolean validateSignInForm(User user) {
        String login = user.getLogin();
        String password = user.getPassword();
        boolean isValid = true;
        if (!validate(LOGIN_PATTERN, login)){
            isValid = false;
            return isValid;
        }
        if (!validate(PASSWORD_PATTERN, password)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

    @Override
    public boolean validateSignUpForm(User user,String rep_password) {
        boolean isInsert = true;
        if(!validate(NAME_PATTERN, user.getName())){
            isInsert = false;
            return isInsert;
        }
        if (!validate(SURNAME_PATTERN, user.getSurname())){
            isInsert = false;
            return isInsert;
        }
        if (!validate(LOGIN_PATTERN, user.getLogin())){
            isInsert = false;
            return isInsert;
        }
        String password = user.getPassword();
        if(rep_password == null){
            rep_password = password;
        }
        if (!validate(PASSWORD_PATTERN, password) || !validate(PASSWORD_PATTERN,rep_password)){
            isInsert = false;
            return isInsert;
        }
        if(!password.equals(rep_password)){
            isInsert = false;
            return isInsert;
        }
        return isInsert;
    }


    @Override
    public boolean validateDeleteUserForm(List<User> users) {
        boolean isValid = true;
        if(users.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(User user : users){
            String id = String.valueOf(user.getUserId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        return isValid;
    }

    @Override
    public boolean validateAddForm(Book book) {
        boolean isInsert = true;
        if(!validate(BOOK_TITLE_PATTERN, book.getTitle())){
            isInsert = false;
            return isInsert;
        }
        if (!validate(EDITION_YEAR_PATTERN,
                String.valueOf(book.getEditionYear())) || book.getEditionYear() < MIN_YEAR || book.getEditionYear() > MAX_YEAR){
            isInsert = false;
            return isInsert;
        }
        if (!validate(DESCRIPTION_PATTERN, book.getDescription())){
            isInsert = false;
            return isInsert;
        }
        for(Author author : book.getAuthors()){
            if(!validate(ID_PATTERN, String.valueOf(author.getAuthorId()))){
                isInsert = false;
                return isInsert;
            }
        }
        if (!validate(ID_PATTERN, String.valueOf(book.getGenre().getGenreId()))){
            isInsert = false;
            return isInsert;
        }
        if (!validate(ID_PATTERN, String.valueOf(book.getPublisher().getPublisherId()))){
            isInsert = false;
            return isInsert;
        }
        return isInsert;
    }

    @Override
    public boolean validateDeleteBookForm(List<Book> books) {
        boolean isValid = true;
        if(books.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(Book book : books){
            String id = String.valueOf(book.getBookId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        return isValid;
    }

    @Override
    public boolean validateGiveBookForm(List<Book> books, User user, String place) {
        boolean isValid = true;
        if(books.isEmpty() || user == null || place.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(Book book:books){
            String id = String.valueOf(book.getBookId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        if(!validate(ID_PATTERN, String.valueOf(user.getUserId()))){
            isValid = false;
            return isValid;
        }
        if(!validate(PLACE,place)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

    @Override
    public boolean validateReturnBookForm(List<History> records) {
        boolean isValid = true;
        if(records.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(History record : records){
            String id = String.valueOf(record.getRecordId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        return isValid;
    }

    @Override
    public boolean validate(String patternForExps, String expression) {
        Pattern pattern = Pattern.compile(patternForExps);
        Matcher matcher = pattern.matcher(expression);
        if(matcher.find()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean validateDeleteAuthorForm(List<Author> authors) {
        boolean isValid = true;
        if(authors.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(Author author : authors){
            String id = String.valueOf(author.getAuthorId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        return isValid;
    }

    @Override
    public boolean validateAddAuthorForm(Author author) {
        boolean isValid = true;
        String name = author.getName();
        if (!validate(AUTHOR_NAME_PATTERN, name)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

    @Override
    public boolean validateEditAuthorForm(Author author) {
        boolean isValid = true;
        String name = author.getName();
        String id = String.valueOf(author.getAuthorId());
        if (!validate(AUTHOR_NAME_PATTERN, name) || !validate(ID_PATTERN,id)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

    @Override
    public boolean validateDeletePublisherForm(List<Publisher> publishers) {
        boolean isValid = true;
        if(publishers.isEmpty()){
            isValid = false;
            return isValid;
        }
        for(Publisher publisher : publishers){
            String id = String.valueOf(publisher.getPublisherId());
            if(!validate(ID_PATTERN,id)){
                isValid = false;
                return isValid;
            }
        }
        return isValid;
    }

    @Override
    public boolean validateAddPublisherForm(Publisher publisher) {
        boolean isValid = true;
        String title = publisher.getTitle();
        if (!validate(PUBLISHER_TITLE_PATTERN, title)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

    @Override
    public boolean validateEditPublisherForm(Publisher publisher) {
        boolean isValid = true;
        String title = publisher.getTitle();
        String id = String.valueOf(publisher.getPublisherId());
        if (!validate(PUBLISHER_TITLE_PATTERN, title) || !validate(ID_PATTERN,id)){
            isValid = false;
            return isValid;
        }
        return isValid;
    }

}
