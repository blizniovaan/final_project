package by.epam.library.service.validator;

import by.epam.library.entity.Publisher;

import java.util.List;

public interface PublisherValidatable extends Validatable{
    boolean validateDeletePublisherForm(List<Publisher> publishers);
    boolean validateAddPublisherForm(Publisher object);
    boolean validateEditPublisherForm(Publisher object);
}

