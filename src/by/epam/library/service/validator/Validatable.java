package by.epam.library.service.validator;

public interface Validatable {
    /**
     * This method is used for verification of data which are sent by the client
     * @param pattern
     * @param expression   data,which have to be checked
     * @return boolean     if data correspond to a pattern,it will return true.Otherwise - false.
     */
    boolean validate(String pattern, String expression);
}
