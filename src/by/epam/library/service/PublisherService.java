package by.epam.library.service;

import by.epam.library.entity.Publisher;

import java.util.List;

public interface PublisherService extends Service<Publisher> {
    @Override
    Publisher selectOne(Publisher entity) throws ServiceException;

    @Override
    Publisher insert(Publisher entity) throws ServiceException;

    @Override
    List<Publisher> selectAll() throws ServiceException;

    @Override
    boolean delete(Publisher entity) throws ServiceException;

    @Override
    boolean delete(List<Publisher> list) throws ServiceException;

    @Override
    Publisher update(Publisher entity) throws ServiceException;
}
