package by.epam.library.service;

import by.epam.library.entity.Card;

import java.util.List;

public interface CardService extends Service<Card> {
    @Override
    List<Card> selectAll() throws ServiceException;

    @Override
    Card selectOne(Card entity) throws ServiceException;

    @Override
    Card insert(Card entity) throws ServiceException;

    @Override
    boolean delete(Card entity);

    @Override
    Card update(Card entity);
}
