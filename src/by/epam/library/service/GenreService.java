package by.epam.library.service;

import by.epam.library.entity.Genre;

import java.util.List;

public interface GenreService extends Service<Genre> {
    @Override
    List<Genre> selectAll() throws ServiceException;

    @Override
    boolean delete(List<Genre> list) throws ServiceException;

    @Override
    Genre update(Genre entity) throws ServiceException;

    @Override
    Genre insert(Genre entity) throws ServiceException;
}
