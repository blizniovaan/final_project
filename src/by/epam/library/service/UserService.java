package by.epam.library.service;

import by.epam.library.entity.User;

import java.util.List;

/**
 * Created by HP on 10.02.2016.
 */
public interface UserService extends Service<User>{
    @Override
    List<User> selectAll() throws ServiceException;

    @Override
    User selectOne(User entity) throws ServiceException;

    @Override
    User insert(User entity) throws ServiceException;

    User insert(User user, String rep_password) throws ServiceException;

    @Override
    boolean delete(User entity) throws ServiceException;

    @Override
    User update(User entity) throws ServiceException;

    boolean delete(List<User> users) throws ServiceException;
}
