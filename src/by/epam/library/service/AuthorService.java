package by.epam.library.service;

import by.epam.library.entity.Author;

import java.util.List;

public interface AuthorService extends Service<Author> {
    @Override
    List<Author> selectAll() throws ServiceException;

    @Override
    Author update(Author entity) throws ServiceException;

    @Override
    boolean delete(List<Author> list) throws ServiceException;

    @Override
    Author insert(Author entity) throws ServiceException;
}
