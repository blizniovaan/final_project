package by.epam.library.service;

import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;

import java.util.List;

public interface BookService extends Service<Book> {
    @Override
    List<Book> selectAll() throws ServiceException;


    List<Book> selectAllAvailable() throws ServiceException;

    @Override
    Book selectOne(Book entity) throws ServiceException;

    @Override
    Book insert(Book entity) throws ServiceException;

    @Override
    boolean delete(Book entity) throws ServiceException;

    @Override
    boolean delete(List<Book> list) throws ServiceException;

    @Override
    Book update(Book entity);

    List<Book> selectAvailableBookByLetter(String letterFrom, String genre) throws  ServiceException;

    List<Book> selectAllBooksByLetter(String letterFrom, String genre) throws ServiceException;

    List<Book> selectAllBooksByAuthor(String letterFrom) throws ServiceException;

    List<Book> selectAvailableBooksByAuthor(String letterFrom) throws ServiceException;

    List<Book> selectAvailableByEnterWord(String enterWord) throws ServiceException;

    List<Book> selectAllByEnterWord(String enterWord) throws ServiceException;

    List<Book> selectAvailableBooksByLetter(String letterFrom) throws ServiceException;

    List<Book> selectAllBooksByLetter(String letterFrom) throws ServiceException;

    List<History> getBooksFromCard(User user) throws ServiceException;

    List<History> returnBooks(List<History> records) throws ServiceException;

}
