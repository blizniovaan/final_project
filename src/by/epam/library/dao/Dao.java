package by.epam.library.dao;

import java.util.List;

public interface Dao<T> {
    List<T> selectAll() throws DAOException;

    T selectOne(T entity) throws DAOException;

    T insert(T entity) throws DAOException;

    boolean delete(T entity) throws DAOException;

    boolean delete(List<T> entity) throws DAOException;

    T update(T entity) throws DAOException;
}
