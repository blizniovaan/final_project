package by.epam.library.dao;

import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;

import java.util.List;

public interface GenreDao extends Dao<Genre> {
    @Override
    List<Genre> selectAll() throws DAOException;

    @Override
    Genre selectOne(Genre entity);

    @Override
    Genre insert(Genre entity) throws DAOException;

    @Override
    boolean delete(Genre entity);

    @Override
    Genre update(Genre entity) throws DAOException;

    Genre select(Book book) throws DAOException;
}
