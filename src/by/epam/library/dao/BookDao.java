package by.epam.library.dao;

import by.epam.library.entity.Book;
import by.epam.library.entity.History;
import by.epam.library.entity.User;

import java.util.List;

public interface BookDao extends Dao<Book> {
    /**
     * This method is used to select all books from the database.
     * @return  List<Book>  all books
     * @throws DAOException   if connection or sql exception occured
     */
    @Override
    List<Book> selectAll() throws DAOException;
    /**
     * This method is used to select one book from the database.
     * @return Book           selected book
     * @throws DAOException   if connection or sql exception occured
     */
    @Override
    Book selectOne(Book entity) throws DAOException;
    /**
     * This method is used to insert a book in the database.
     * @return Book          inserted book
     * @throws DAOException  if connection or sql exception occured
     */
    @Override
    Book insert(Book entity) throws DAOException;
    /**
     * This method is used to delete a book from the database.
     * @return boolean       if the book is successfully deleted,it will return true.Otherwise it will return false.
     * @throws DAOException  if connection or sql exception occured
     */
    @Override
    boolean delete(Book entity) throws DAOException;
    /**
     * This method is used to delete books from the database.
     * @return boolean       if books is successfully deleted,it will return true.Otherwise it will return false.
     * @throws DAOException  if connection or sql exception occured
     */
    boolean delete(List<Book> list) throws DAOException;
    /**
     * This method is used to update an information about book.
     * @return Book            updated book
     * @throws DAOException    if connection or sql exception occured
     */
    @Override
    Book update(Book entity);

    /**
     * This method is used to select available book defined the genre, which begin with a certain letter.
     * If there is no such books in the database, it will return null.
     * @return List<Book>      list of books
     * @throws DAOException    if connection or sql exception occured
     */
    List<Book> selectAvailableBookByLetter(String letterFrom, String genre) throws DAOException;

    /**
     * This method is used to select all books, which begin with a certain letter.
     * If there is no such books in the database, it will return empty list.
     * @return List<Book>      list of books
     * @throws DAOException    if connection or sql exception occured
     */
    List<Book> selectAllBooksByLetter(String letterFrom, String genre) throws DAOException;

    /**
     * This method is used to select all list of books which the authors' names begin with a certain letter.
     * It selects all authors using the certain letter.Then it selects all books by these authors.
     * If there is no such books or there is no such authors in the database, it will return empty list.
     * @return List<Book>      list of books
     * @throws DAOException    if connection or sql exception occured
     */
    List<Book> selectAllBooksByAuthor(String letterFrom) throws DAOException;

    /**
     * This method is used to select available list of books which the authors' names begin with a certain letter.
     * It selects all authors using the certain letter.Then it selects available books by these authors.
     * If there is no such books or there is no such authors in the database, it will return empty list.
     * @return List<Book>     available books by the first letter of a surname of the author
     * @throws DAOException   if connection or sql exception occured
     */
    List<Book> selectAvailableBooksByAuthor(String letterFrom) throws DAOException;

    /**
     * This method is used to return the list of available books which titles contain the string entered by the user.
     * If there is no such books in the database, it will return empty list.
     * Search is in a subline.
     * @return List<Book>     available books by enter word
     * @throws DAOException   if connection or sql exception occured
     */
    List<Book> selectAvailableByEnterWord(String enterWord) throws DAOException;

    /**
     * This method is used to return the list of all books which titles contain the string entered by the user.
     * If there is no such books in the database, it will return empty list.
     * @return List<Book>     all books by enter word
     * @throws DAOException   if connection or sql exception occured
     */
    List<Book> selectAllByEnterWord(String enterWord) throws DAOException;

    /**
     * This method is used to select available book, which begins with a certain letter.
     * If there is no such books in the database, it will return empty list.
     * @return  List<Book>     available books with the title,which begins with a certain letter
     * @throws DAOException    if connection or sql exception occured
     */
    List<Book> selectAvailableBooksByLetter(String letterFrom) throws DAOException;

    /**
     * This method is used to select all books, which begin with a certain letter.
     * If there is no such books in the database, it will return empty list.
     * @return List<Book>     all books with the title,which begins with a certain letter
     * @throws DAOException   if connection or sql exception occured.
     */
    List<Book> selectAllBooksByLetter(String letterFrom) throws DAOException;

    /**
     * This method is used to select books of the user from the library card.
     * It selects card of the user.Then it selects all records which belong to
     * the card of the user from table 'history' and returns them.
     * @return  List<History>   records of the certain user.
     * @throws DAOException     if connection or sql exception occured.
     */
    List<History> getBooksFromCard(User user) throws DAOException;

    /**
     * This method used to return books in library.In table 'history' there is change delivery date from null to
     * current date,then there is update status of book from 'unavailable' to 'available'.
     * @return List<History>  records with the changed delivery dates.
     * @throws DAOException   if connection or sql exception occured.
     */
    List<History> returnBooks(List<History> records) throws DAOException;

    /**
     * The method is used to return a list of available books.If there are no books available,
     * the method will return an empty list.
     * @return  List<Book>   all available books from the database.
     * @throws DAOException  if connection or sql exception occured.
     */
    List<Book> selectAllAvailable() throws DAOException;

}
