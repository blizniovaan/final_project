package by.epam.library.dao;

import by.epam.library.entity.Card;
import by.epam.library.entity.User;

import java.util.List;

public interface CardDao extends Dao<Card> {
    @Override
    List<Card> selectAll();

    @Override
    Card selectOne(Card entity);

    @Override
    Card insert(Card entity);

    @Override
    boolean delete(Card entity);

    @Override
    Card update(Card entity);

    Card select(User user) throws DAOException;

}
