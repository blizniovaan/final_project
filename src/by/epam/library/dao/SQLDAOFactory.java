package by.epam.library.dao;

import by.epam.library.dao.impl.*;

public class SQLDAOFactory extends AbstractDAOFactory {
    private static final SQLDAOFactory instance = new SQLDAOFactory();
    private SQLDAOFactory(){}
    public static SQLDAOFactory getInstance(){
        return instance;
    }

    @Override
    public UserDao getUserDAO() {
        return UserDAOImpl.getInstance();
    }
    @Override
    public BookDao getBookDAO() {
        return BookDAOImpl.getInstance();
    }

    @Override
    public GenreDao getGenreDAO() {
        return GenreDAOImpl.getInstance();
    }

    @Override
    public AuthorDao getAuthorDAO() {
        return AuthorDAOImpl.getInstance();
    }

    @Override
    public PublisherDao getPublisherDAO() {
        return PublisherDAOImpl.getInstance();
    }

    @Override
    public HistoryDao getHistoryDAO() {
        return HistoryDaoImpl.getInstance();
    }

    @Override
    public GeneralDao getGeneralDAO() {
        return GeneralDao.getInstance();
    }
}
