package by.epam.library.dao;


import by.epam.library.dao.impl.GeneralDao;

public abstract class AbstractDAOFactory {
    public abstract UserDao getUserDAO();
    public abstract BookDao getBookDAO();
    public abstract GenreDao getGenreDAO();
    public abstract AuthorDao getAuthorDAO();
    public abstract PublisherDao getPublisherDAO();
    public abstract HistoryDao getHistoryDAO();
    public abstract GeneralDao getGeneralDAO();
}
