package by.epam.library.dao;

import by.epam.library.entity.Book;
import by.epam.library.entity.BookStatus;

import java.util.List;

public interface BookStatusDao extends Dao<BookStatus>{
    /**
     * This method is used to select the status of certain book.
     */
    BookStatus select(Book book) throws DAOException;

    /**
     * This method is used to insert new status of a book.
     */
    @Override
    BookStatus insert(BookStatus status) throws DAOException;

    /**
     * This method is used to delete a status of a book.
     */
    @Override
    boolean delete(BookStatus status) throws DAOException;
    /**
     * This method is used to delete statuses of a book.
     */
    @Override
    boolean delete(List<BookStatus> statuses) throws DAOException;
    /**
     * This method is used to update a status of a book.
     */
    @Override
    BookStatus update(BookStatus status) throws DAOException;

    /**
     * This method is used to select all statuses from the database
     */
    @Override
    List<BookStatus> selectAll() throws DAOException;
}
