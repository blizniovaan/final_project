package by.epam.library.dao.config;

import org.apache.log4j.Logger;

import java.util.ResourceBundle;

public class DAOConfigManager {
    public static final Logger LOGGER = Logger.getLogger(DAOConfigManager.class);
    public static final String URL = "url";
    public static final String USER = "user";
    public static final String PASS = "pass";
    public static final String POOL_SIZE = "pool.size";

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("jdbc.jdbc");

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public static void setResourceBundle(ResourceBundle resourceBundle) {
        DAOConfigManager.resourceBundle = resourceBundle;
    }

    public static String getProperty(String property) {
        if (resourceBundle == null) {
            resourceBundle = ResourceBundle.getBundle("jdbc.jdbc");
            LOGGER.info("Resource bundle has been created");
        }
        return (String) resourceBundle.getObject(property);
    }
}
