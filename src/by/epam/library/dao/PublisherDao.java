package by.epam.library.dao;

import by.epam.library.entity.Book;
import by.epam.library.entity.Publisher;

import java.util.List;

public interface PublisherDao extends Dao<Publisher> {
    @Override
    List<Publisher> selectAll() throws DAOException;

    @Override
    Publisher selectOne(Publisher entity);

    @Override
    Publisher insert(Publisher entity) throws DAOException;

    @Override
    boolean delete(Publisher entity);

    @Override
    Publisher update(Publisher entity) throws DAOException;

    Publisher select(Book book)throws DAOException;
}
