package by.epam.library.dao;

import by.epam.library.entity.Book;
import by.epam.library.entity.Card;
import by.epam.library.entity.History;

import java.util.List;

public interface HistoryDao extends Dao<History> {
    @Override
    List<History> selectAll() throws DAOException;

    @Override
    History selectOne(History entity) throws DAOException;

    @Override
    History insert(History entity) throws DAOException;

    List<History> insert(History record, List<Book> books, Card card) throws DAOException;

    @Override
    boolean delete(History entity) throws DAOException;

    @Override
    boolean delete(List<History> entity) throws DAOException;

    @Override
    History update(History entity) throws DAOException;

    boolean update(List<History> records) throws DAOException;

    List<History> insertPart(History entity, List<Book> books) throws DAOException;

}
