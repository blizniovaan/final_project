package by.epam.library.dao;

import by.epam.library.entity.User;

import java.util.List;

public interface UserDao extends Dao<User> {
    @Override
    List<User> selectAll() throws DAOException;

    @Override
    User selectOne(User entity) throws DAOException;

    @Override
    User insert(User entity) throws DAOException;

    @Override
    boolean delete(User entity);

    @Override
    User update(User entity);

    User signIn(User user) throws DAOException;

    boolean canInsert(String login)  throws DAOException;

    boolean delete(List<User> users)  throws DAOException;

    boolean canDelete(List<User> users)  throws DAOException;
}
