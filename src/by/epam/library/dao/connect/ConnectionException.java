package by.epam.library.dao.connect;

/**
 * Created by HP on 10.02.2016.
 */
public class ConnectionException extends Exception{
    private static final long serialVersionUID = 1L;

    public ConnectionException(){}
    public ConnectionException(String message){super(message);}
    public ConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
    public ConnectionException(Throwable cause) {
        super(cause);
    }
}
