package by.epam.library.dao.impl;

import by.epam.library.dao.AuthorDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.*;
import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by HP on 04.02.2016.
 */
public class AuthorDAOImpl implements AuthorDao {
    public static final String NAME = "name";
    public static final String AUTHOR_ID = "authorId";
    public static final String SELECT_AUTHOR  = "SELECT DISTINCT author.name FROM author "+
            "JOIN author_has_book ON author_has_book.authorId = author.authorId WHERE author_has_book.bookId=?";
    public static final String SELECT_ALL = "SELECT * FROM author";
    public static final String INSERT = "INSERT INTO author (name) VALUES (?)";
    public static final String SELECT_ID_AUTHOR_FROM_AUTHOR_HAS_BOOK_TABLE = "SELECT authorId FROM author_has_book " +
            "WHERE author_has_book.authorId=?";
    public static final String DELETE = "DELETE FROM author WHERE authorId=?";
    public static final String UPDATE = "UPDATE author SET name=? WHERE authorId=?";

    private static final AuthorDAOImpl instance = new AuthorDAOImpl();
    private AuthorDAOImpl(){}
    public static AuthorDAOImpl getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();

    public static final Logger LOGGER = Logger.getLogger(AuthorDAOImpl.class);
    /**
     * This method is used to select author of the book.
     * @param book the book,which author needs to be recognized
     * @return Set<Author>  set of authors of the book
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public Set<Author> select(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Set<Author> authors = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT_AUTHOR)){
                authors = new HashSet<>();
                statement.setInt(1, book.getBookId());
                resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Author author = new Author();
                    author.setName(resultSet.getString(1));
                    authors.add(author);
                }
            } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return authors;
    }
    /**
     * This method is used to select all authors from the database.
     * @return List<Author>  set of authors from the database
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Author> selectAll() throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        List<Author> authors = new ArrayList<>();
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Author author = new Author();
                author.setAuthorId(resultSet.getInt(AUTHOR_ID));
                author.setName(resultSet.getString(NAME));
                authors.add(author);
            }
        }catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return authors;
    }

    /**
     * This method is used to insert new author in the database.
     * @param author author,which will be insert
     * @return Author inserted author
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public Author insert(Author author) throws DAOException {
        Connection connection = null;
        Author insertedAuthor = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, author.getName());
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()){
                insertedAuthor = new Author();
                insertedAuthor.setAuthorId(resultSet.getInt(1));
            }
        }catch ( SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return insertedAuthor;
    }

    /**
     * This method delete list of authors.
     * @param authors authors,which will be delete
     * @return boolean if removal was successful,method would return true.Otherwise it will return false
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public boolean delete(List<Author> authors) throws DAOException {
        boolean isDelete = false;
        if(!canDelete(authors)){
            return isDelete;
        }
        Connection connection = null;
        int countRemovedStr = 0;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            for (Author author: authors){
                statement.setInt(1, author.getAuthorId());
                countRemovedStr += statement.executeUpdate();
            }
            if(countRemovedStr > 0){
                isDelete = true;
            }
            else{
                isDelete = false;
            }
        } catch ( SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return isDelete;
    }

    /**
     * The method checks an opportunity to remove.The author can be removed if he hasn't written any book.
     * @param authors authors,which will be checked
     * @return boolean If removal is possible,it will return true.Otherwise it will return false.
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    private boolean canDelete(List<Author> authors) throws DAOException {
        Connection connection = null;
        boolean canDelete = true;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_ID_AUTHOR_FROM_AUTHOR_HAS_BOOK_TABLE)) {
            for (Author author : authors){
                statement.setInt(1,author.getAuthorId());
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    canDelete = false;
                    return canDelete;
                }
            }
        }catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return canDelete;
    }

    /**
     * This method is used to update an information(name) about author.
     * @param author
     * @return Author the updated author
     * @throws DAOException If connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public Author update(Author author) throws DAOException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setString(1, author.getName());
            statement.setInt(2, author.getAuthorId());
            int countEditedStr = statement.executeUpdate();
            if(countEditedStr == 0){
                author = null;
            }
        }catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("AuthorDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("AuthorDAOImpl Exception",e);
            }
        }
        return author;
    }
    @Override
    public boolean delete(Author entity) {
        return false;
    }

    @Override
    public Author selectOne(Author entity) throws DAOException {
        return null;
    }

}
