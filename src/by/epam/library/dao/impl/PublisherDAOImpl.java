package by.epam.library.dao.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.PublisherDao;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.Book;
import by.epam.library.entity.Publisher;
import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 06.02.2016.
 */
public class PublisherDAOImpl implements PublisherDao {
    public static final Logger LOGGER = Logger.getLogger(PublisherDAOImpl.class);

    public static final String SELECT_ALL_PUBLISHER = "SELECT * FROM publisher " +
            " GROUP BY publisher.title" ;
    public static final String SELECT = "SELECT * FROM publisher JOIN book ON book.publisherId=publisher.publisherId " +
            "WHERE book.bookId=?";
    public static final String INSERT = "INSERT INTO publisher (title) VALUES (?)";
    public static final String PUBLISHER_ID = "publisherId";
    public static final String TITLE = "title";
    public static final String UPDATE = "UPDATE publisher SET title=? WHERE publisherId=?";
    public static final String DELETE = "DELETE FROM publisher WHERE publisherId=?";
    public static final String SELECT_ID_PUBLISHER_FROM_BOOK_TABLE = "SELECT bookId FROM book WHERE book.publisherId=?";

    private static final PublisherDAOImpl instance = new PublisherDAOImpl();
    private PublisherDAOImpl(){}
    public static PublisherDAOImpl getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public List<Publisher> selectAll() throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        List<Publisher> publishers = new ArrayList<>();
        try{
            connection = pool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(SELECT_ALL_PUBLISHER)){
                resultSet = statement.executeQuery();

                while (resultSet.next()){
                    Publisher publisher = new Publisher();
                    String title = resultSet.getString(TITLE);
                    Integer publisherId = resultSet.getInt(PUBLISHER_ID);
                    publisher.setPublisherId(publisherId);
                    publisher.setTitle(title);
                    publishers.add(publisher);
                }
            }
        }catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return publishers;
    }

    @Override
    public Publisher select(Book book) throws DAOException {
        Connection connection = null;
        Publisher publisher = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("PublisherDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT)){
                statement.setInt(1, book.getBookId());
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()){
                    publisher = new Publisher();
                    publisher.setPublisherId(resultSet.getInt(PUBLISHER_ID));
                    publisher.setTitle(resultSet.getString(TITLE));
                }
            }catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return publisher;
    }

    @Override
    public Publisher insert(Publisher publisher) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Publisher inseredPublisher = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("PublisherDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)){
                statement.setString(1,publisher.getTitle());
                statement.executeUpdate();

                resultSet = statement.getGeneratedKeys();

                if (resultSet.next()){
                    inseredPublisher = new Publisher();
                    inseredPublisher.setPublisherId(resultSet.getInt(1));
                }
            }catch ( SQLException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return inseredPublisher;
    }

    @Override
    public boolean delete(List<Publisher> publishers) throws DAOException {
        boolean isDelete = false;
        if(!canDelete(publishers)){
            return isDelete;
        }
        Connection connection = null;
        int countRemovedStr = 0;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("PublisherDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(DELETE)){
                for (Publisher publisher: publishers){
                    statement.setInt(1, publisher.getPublisherId());
                    countRemovedStr += statement.executeUpdate();
                }
                if(countRemovedStr > 0){
                    isDelete = true;
                }
                else{
                    isDelete = false;
                }
            } catch ( SQLException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return isDelete;
    }

    boolean canDelete(List<Publisher> publishers) throws DAOException {
        Connection connection = null;
        boolean canDelete = true;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("PublisherDAOImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_ID_PUBLISHER_FROM_BOOK_TABLE)) {
            for (Publisher publisher : publishers){
                statement.setInt(1,publisher.getPublisherId());
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    canDelete = false;
                    return canDelete;
                }
            }
        }catch ( SQLException e) {
            LOGGER.error(e);
            throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return canDelete;
    }

    @Override
    public Publisher update(Publisher publisher) throws DAOException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("PublisherDAOImpl Exception",e);
        }
            try(PreparedStatement psUpdate = connection.prepareStatement(UPDATE)){
                psUpdate.setString(1, publisher.getTitle());
                psUpdate.setInt(2, publisher.getPublisherId());
                int countEditedStr = psUpdate.executeUpdate();
                if(countEditedStr == 0){
                    publisher = null;
                }
            }catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("PublisherDAOImpl Exception",e);
            }
        }
        return publisher;
    }

    @Override
    public Publisher selectOne(Publisher publisher) {
        return null;
    }

    @Override
    public boolean delete(Publisher entity) {
        return false;
    }

}
