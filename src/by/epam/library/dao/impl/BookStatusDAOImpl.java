package by.epam.library.dao.impl;

import by.epam.library.dao.BookStatusDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.Book;
import by.epam.library.entity.BookStatus;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class BookStatusDAOImpl implements BookStatusDao {
    public static final Logger LOGGER = Logger.getLogger(BookStatusDAOImpl.class);
    public static final String STATUS = "status";
    public static final String STATUS_ID = "statusId";
    public static final String SELECT = "SELECT * FROM book_status JOIN book ON book.statusId=book_status.statusId " +
            "WHERE book.bookId=?";

    private static final BookStatusDAOImpl instance = new BookStatusDAOImpl();
    private BookStatusDAOImpl(){}
    public static BookStatusDAOImpl getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();
    @Override
    public BookStatus select(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        BookStatus status = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookStatusDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT)){
                statement.setInt(1, book.getBookId());
                resultSet = statement.executeQuery();
                while (resultSet.next()){
                    status = new BookStatus();
                    status.setStatus(resultSet.getString(STATUS));
                    status.setStatusId(resultSet.getInt(STATUS_ID));
                }
            } catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("BookStatusDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookStatusDAOImpl Exception",e);
            }
        }
        return status;
    }

    @Override
    public List<BookStatus> selectAll() throws DAOException {
        return null;
    }

    @Override
    public BookStatus selectOne(BookStatus entity) throws DAOException {
        return null;
    }

    @Override
    public BookStatus insert(BookStatus entity) throws DAOException {
        return null;
    }

    @Override
    public boolean delete(BookStatus entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(List<BookStatus> entity) throws DAOException {
        return false;
    }

    @Override
    public BookStatus update(BookStatus entity) throws DAOException {
        return null;
    }
}
