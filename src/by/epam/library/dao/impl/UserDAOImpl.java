package by.epam.library.dao.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.UserDao;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.Card;
import by.epam.library.entity.History;
import by.epam.library.entity.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDao {
    public static final Logger LOGGER = Logger.getLogger(UserDAOImpl.class);
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String USER_LOGIN = "login";
    public static final String USER_ID = "userId";
    public static final String ROLE = "role";
    public static final String PASSWORD = "password";
    public static final String CARD_ID = "cardId";
    public static final String DELIVERY_DATE = "deliveryDate";
    public static final int NULL = 0;

    public static final String SELECT_ALL_USERS = "SELECT * FROM `user` WHERE role='user'";
    public static final String SELECT_USER_BY_ID = "SELECT * FROM `user` WHERE UserId=?";
    public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM `user` WHERE login=?";
    public static final String LOGIN = "SELECT * FROM `user` WHERE login=? AND password=?";
    public static final String INSERT = "INSERT INTO `user`(login, password, name,surname, role) VALUES(?,?,?,?,?)";
    public static final String INSERT_CARD_ID = "INSERT INTO card(cardId,userId) VALUES(?,?)";
    public static final String DELETE_USER = "DELETE FROM user WHERE userId=?";
    public static final String DELETE_RECORDS = "DELETE FROM history WHERE cardId=?";
    public static final String SELECT_ID_CARD= "SELECT * FROM card WHERE card.userId = ?";
    public static final String DELETE_CARD= "DELETE FROM card WHERE cardId=?";
    public static final String SELECT_RECORD_BY_ID_CARD= "SELECT * FROM history " +
            "JOIN book ON book.bookId = history.bookId " +
            "WHERE history.cardId = ?";

    private ConnectionPool pool = ConnectionPool.getInstance();

    private static final UserDAOImpl instance = new UserDAOImpl();
    private UserDAOImpl(){}
    public static UserDAOImpl getInstance() {
        return instance;
    }

    @Override
    public List<User> selectAll() throws DAOException{
        Connection connection = null;
        List<User> users = new ArrayList<>();
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("UserDAOImpl Exception",e);
        }
            try( PreparedStatement statement = connection.prepareStatement(SELECT_ALL_USERS)){
                ResultSet resultSet = statement.executeQuery();
                while(resultSet.next()){
                    User user = new User();
                    user.setUserId(resultSet.getInt(USER_ID));
                    user.setName(resultSet.getString(NAME));
                    user.setSurname(resultSet.getString(SURNAME));
                    user.setLogin(resultSet.getString(USER_LOGIN));
                    user.setRole(resultSet.getString(ROLE));
                    users.add(user);
                }
            }catch ( SQLException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null) {
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException(e);
            }
        }
        return users;
    }

    @Override
    public User selectOne(User user) throws DAOException {
        Connection connection = null;
        User selectedUser = null;
        try {
            connection = pool.getConnection();
            try (PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_ID)){
                statement.setInt(1, user.getUserId());
                ResultSet resultSet = statement.executeQuery();

                if(resultSet.next()){
                    selectedUser = new User();
                    selectedUser.setUserId(resultSet.getInt(USER_ID));
                    selectedUser.setLogin(resultSet.getString(USER_LOGIN));
                    selectedUser.setPassword(resultSet.getString(PASSWORD));
                    selectedUser.setName(resultSet.getString(NAME));
                    selectedUser.setSurname(resultSet.getString(SURNAME));
                    selectedUser.setRole(resultSet.getString(ROLE));
                }
            }
        } catch (ConnectionException |SQLException e) {
            LOGGER.error(e);
            throw new DAOException("UserDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
            }
        }
        return selectedUser;
    }

    @Override
    public User insert(User user) throws DAOException {
        if(!canInsert(user.getLogin())){
            return null;
        }
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("UserDAOImpl Exception", e);
        }
            try( PreparedStatement psUser = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
                 PreparedStatement psCarId = connection.prepareStatement(INSERT_CARD_ID) ){
                connection.setAutoCommit(false);
                psUser.setString(1,user.getLogin());
                psUser.setString(2,String.valueOf(user.getPassword().hashCode()));
                psUser.setString(3,user.getName());
                psUser.setString(4,user.getSurname());
                psUser.setString(5, user.getRole());
                psUser.executeUpdate();

                resultSet = psUser.getGeneratedKeys();

                if(resultSet.next()){
                    user.setUserId(resultSet.getInt(1));
                }
                psCarId.setInt(1,user.getUserId());
                psCarId.setInt(2,user.getUserId());
                psCarId.executeUpdate();
                connection.commit();
            }catch (SQLException e) {
                LOGGER.error(e);
                try {
                    user = null;
                    connection.rollback();
                } catch (SQLException e1) {
                    throw new DAOException("UserDAOImpl Exception", e1);
                }
        }finally {
            try {
                if(connection != null) {
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
            }
            }
        return user;
  }

    @Override
    public boolean delete(List<User> users) throws DAOException {
        boolean isDelete = false;
        if(!canDelete(users)){
            return isDelete;
        }
        Connection connection = null;
        int countDeletedUser = 0;
        List<Card> cards = new ArrayList<>();
        try {
            connection = pool.getConnection();
            try(PreparedStatement psDeleteUser = connection.prepareStatement(DELETE_USER);
                PreparedStatement psCardId = connection.prepareStatement(SELECT_ID_CARD);
                PreparedStatement psDeleteRecords = connection.prepareStatement(DELETE_RECORDS);
                PreparedStatement psDeleteCard = connection.prepareStatement(DELETE_CARD)){
                connection.setAutoCommit(false);
                for (User user : users){
                    psCardId.setInt(1,user.getUserId());
                    ResultSet rsCardId = psCardId.executeQuery();
                    while (rsCardId.next()){
                        Card card = new Card();
                        card.setCardId(rsCardId.getInt(CARD_ID));
                        cards.add(card);
                    }
                }
                for (Card card : cards){
                    psDeleteCard.setInt(1,card.getCardId());
                    psDeleteCard.executeUpdate();

                    psDeleteRecords.setInt(1,card.getCardId());
                    psDeleteRecords.executeUpdate();
                }
                for (User user : users){
                    psDeleteUser.setInt(1,user.getUserId());
                    countDeletedUser += psDeleteUser.executeUpdate();
                }
                if(countDeletedUser > NULL){
                    isDelete = true;
                }
                else{
                    isDelete = false;
                }
                connection.commit();
            }
        } catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DAOException("UserDAOImpl Exception", e1);
            }
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
            }
        }
        return isDelete;
    }

    /**
     * This method is used to check users who can be removed.It selects the library card of user,then
     *  it chooses records from the table according to this card.If at least one book hasn't been returned {@see #isRemoved()},
     *  it is impossible to remove the user.
     * @param users
     * @return boolean
     * @throws DAOException    if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public boolean canDelete(List<User> users) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        List<Card> cards = new ArrayList<>();

        boolean canDelete = false;
        try {
            connection = pool.getConnection();
            try(PreparedStatement psCardId = connection.prepareStatement(SELECT_ID_CARD);
                PreparedStatement psRecords = connection.prepareStatement(SELECT_RECORD_BY_ID_CARD) ) {
                for(User user : users){
                    psCardId.setInt(1, user.getUserId());
                    resultSet = psCardId.executeQuery();
                    if(resultSet.next()){
                        Card card = new Card();
                        card.setCardId(resultSet.getInt(CARD_ID));
                        cards.add(card);
                    }
                }
                for (Card card : cards){
                    List<History> records = new ArrayList<>();
                    psRecords.setInt(1,card.getCardId());
                    resultSet = psRecords.executeQuery();
                    while (resultSet.next()){
                        History record = new History();
                        record.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                        records.add(record);
                    }
                    if(isRemoved(records)){
                        canDelete = true;
                    }
                    else{
                        canDelete = false;
                        return canDelete;
                    }
                }
            }

        } catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("UserDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
            }
        }
        return canDelete;
    }

    private static boolean isRemoved(List<History> records){
        boolean isRemoved = true;
        for(History record : records){
            if(record.getDeliveryDate() == null){
                isRemoved = false;
                break;
            }
        }
        return isRemoved;
    }

    public User signIn(User user) throws DAOException{
        String login = user.getLogin();
        String password = user.getPassword();
        Connection connection = null;
        User curUser = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            try (PreparedStatement statement = connection.prepareStatement(LOGIN)){
                statement.setString(1,login);
                statement.setString(2,String.valueOf(password.hashCode()));
                resultSet = statement.executeQuery();

                if(resultSet.next()){
                    curUser = new User();
                    curUser.setUserId(resultSet.getInt(USER_ID));
                    curUser.setLogin(resultSet.getString(USER_LOGIN));
                    curUser.setPassword(resultSet.getString(PASSWORD));
                    curUser.setName(resultSet.getString(NAME));
                    curUser.setSurname(resultSet.getString(SURNAME));
                    curUser.setRole(resultSet.getString(ROLE));
                    return curUser;
                }
            }
        } catch (ConnectionException |SQLException e) {
            LOGGER.error(e);
            throw new DAOException("UserDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("UserDAOImpl Exception",e);
            }
        }
        return curUser;
    }


    @Override
    public boolean canInsert(String login) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        boolean isUser = true;
        try {
            connection = pool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_LOGIN)) {
                statement.setString(1, login);
                resultSet = statement.executeQuery();
                isUser = !resultSet.next();
            }

        } catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("Connection Excepton",e);
        }finally {
                try {
                    if(connection != null){
                        pool.returnConnection(connection);
                    }
                } catch (ConnectionException e) {
                    LOGGER.error(e);
                    throw new DAOException("UserDAOImpl Exception",e);
                }
            }
        return isUser;
    }

    @Override
    public boolean delete(User entity) {
        return false;
    }

    @Override
    public User update(User entity) {
        return null;
    }

}
