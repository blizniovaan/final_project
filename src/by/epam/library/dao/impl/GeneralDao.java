package by.epam.library.dao.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GeneralDao {
    public static final String UPDATE_BOOK_STATUS = "UPDATE book SET book.statusId=? WHERE book.bookId=?";
    public static final Logger LOGGER = Logger.getLogger(GeneralDao.class);
    public static final String CARD_ID = "cardId";
    public static final String SIGN_PERCENT = "%";
    public static final String NAME = "name";
    public static final String PUBLISHER_ID = "publisherId";
    public static final String TITLE = "title";
    public static final String AUTHOR_ID = "authorId";
    public static final String GENRE_ID = "genreId";
    public static final String STATUS = "status";
    public static final String STATUS_ID = "statusId";
    public static final String BOOK_TITLE = "book.title";
    public static final String BOOK_EDITION_YEAR = "book.editionYear";
    public static final String BOOK_ID = "book.bookId";
    public static final String PLACE = "place";
    public static final String TAKING_DATE = "takingDate";
    public static final String DELIVERY_DATE = "deliveryDate";
    public static final String RECORD_ID = "recordId";
    public static final String SELECT_ID_CARD= "SELECT * FROM card WHERE card.userId = ?";
    public static final String SELECT_AUTHOR  = "SELECT DISTINCT author.name FROM author "+
            "JOIN author_has_book ON author_has_book.authorId = author.authorId WHERE author_has_book.bookId=?";
    public static final String SELECT_ID_AUTHOR_BY_LETTER ="SELECT * FROM author " +
            " WHERE (author.name LIKE ?)" +
            " GROUP BY author.name";
    public static final String SELECT_PUBLISHER_OF_BOOK = "SELECT * FROM publisher " +
            "JOIN book ON book.publisherId=publisher.publisherId " +
            "WHERE book.bookId=?";
    public static final String SELECT_GENRE_OF_BOOK = "SELECT * FROM genre " +
            "JOIN book ON book.genreId=genre.genreId WHERE book.bookId=?";
    public static final String SELECT_STATUS_OF_BOOK = "SELECT * FROM book_status " +
            "JOIN book ON book.statusId=book_status.statusId " +
            "WHERE book.bookId=?";
    public static final String SELECT_RECORD_BY_ID_CARD= "SELECT * FROM history " +
            "JOIN book ON book.bookId=history.bookId WHERE history.cardId=?";
    public static final String UPDATE_RECORD_BY_RETURN = "UPDATE history SET history.deliveryDate=? " +
            "WHERE history.recordId=?";

    private static final GeneralDao instance = new GeneralDao();
    private GeneralDao(){}
    public static GeneralDao getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();

    /**
     * This method is used to update status one of more books.
     * @param books          books, whose statuses will be changed
     * @param status
     * @return boolean       if the status is successfully updated,it will return true.Otherwise it will return false.
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    public boolean updateBookStatus(List<Book> books,int status) throws DAOException {
        Connection connection = null;
        boolean isUpdated = true;
        int count = 0;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException(e);
        }
        try( PreparedStatement psBookUpdate = connection.prepareStatement(UPDATE_BOOK_STATUS)  ){
            for(int i = 0; i < books.size(); i++){
                psBookUpdate.setInt(1, status);
                psBookUpdate.setInt(2,books.get(i).getBookId());
                count+=psBookUpdate.executeUpdate();
            }
            if(count == 0){
                isUpdated = false;
                return isUpdated;
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return isUpdated;
    }

    /**
     * This method is used to return the card of the user.
     * @param user           the user, whose card has to be returned.
     * @return Card          the card of the user
     * @throws DAOException  if connection or sql exception occured.
     * The method needs refactoring.
     */
    public Card getCard(User user) throws DAOException {
        Connection connection = null;
        Card card = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }
        try(PreparedStatement psCard = connection.prepareStatement(SELECT_ID_CARD)){
            psCard.setInt(1, user.getUserId());
            ResultSet rsCard = psCard.executeQuery();
            if(rsCard.next()){
                card = new Card();
                card.setCardId(rsCard.getInt(CARD_ID));
            }
        }catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return card;
    }

    /**
     * This method is used to return authors of the book.
     * @param book           authors of this book have to be selected
     * @return Set<Author>   selected authors of the book
     * @throws DAOException  if connection or sql exception occured.
     * The method needs refactoring.
     */
    public Set<Author> getAuthors(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Set<Author> authors = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_AUTHOR)){
            authors = new HashSet<>();
            statement.setInt(1, book.getBookId());
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Author author = new Author();
                author.setName(resultSet.getString(1));
                authors.add(author);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return authors;
    }

    /**
     * This method is used to return authors, whose surnames begin with a letterFrom.
     * If there is no such authors in the database, it will return empty list.
     * @param letterFrom       initial letter of a surname of the author
     * @return List<Author>    authors, whose surnames begin with a letterFrom.
     * @throws DAOException    if connection or sql exception occured.
     * The method needs refactoring.
     */
    public List<Author> getAuthorsByLetterFilter(String letterFrom) throws DAOException {
        Connection connection = null;
        String preparedLetter = letterFrom + SIGN_PERCENT;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
        ResultSet resultSet = null;
        List<Author> authors = new ArrayList<>();

        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ID_AUTHOR_BY_LETTER)){
            preparedStatement.setString(1, preparedLetter);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Author author = new Author();
                author.setAuthorId(resultSet.getInt(AUTHOR_ID));
                authors.add(author);
            }
        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return authors;
    }

    /**
     * This method is used to return the publisher of the book.
     * @param book           the publisher of this book needs to be returned
     * @return Publisher     publisher of the book
     * @throws DAOException  if connection or sql exception occured.
     * The method needs refactoring.
     */
    public Publisher selectPublisherOfBook(Book book) throws DAOException {
        Connection connection = null;
        Publisher publisher = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_PUBLISHER_OF_BOOK)){
            statement.setInt(1, book.getBookId());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()){
                publisher = new Publisher();
                publisher.setPublisherId(resultSet.getInt(PUBLISHER_ID));
                publisher.setTitle(resultSet.getString(TITLE));
            }
        }catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return publisher;
    }
    /**
     * This method is used to return the genre of the book.
     * @param book           the genre of this book needs to be returned
     * @return Genre         genre of the book
     * @throws DAOException  if connection or sql exception occured.
     * The method needs refactoring.
     */
    public Genre selectGenreOfBook(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Genre genre = null;
        try{
            connection = pool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(SELECT_GENRE_OF_BOOK)){
                statement.setInt(1, book.getBookId());
                resultSet = statement.executeQuery();

                if (resultSet.next()){
                    genre = new Genre();
                    genre.setGenreId(resultSet.getInt(GENRE_ID));
                    genre.setTitle(resultSet.getString(TITLE));
                }
            }
        }catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return genre;
    }
    /**
     * This method is used to return the status of the book.
     * @param book           the status of this book needs to be returned
     * @return BookStatus    status of the book
     * @throws DAOException  if connection or sql exception occured.
     * The method needs refactoring.
     */
    public BookStatus selectStatusOfBook(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        BookStatus status = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_STATUS_OF_BOOK)){
            statement.setInt(1, book.getBookId());
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                status = new BookStatus();
                status.setStatus(resultSet.getString(STATUS));
                status.setStatusId(resultSet.getInt(STATUS_ID));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return status;
    }

    /**
     * This method is used to return records from the library card.
     * @param card             record from this card needs to be returned
     * @return List<History>   records from the library card
     * @throws DAOException    if connection or sql exception occured.
     * The method needs refactoring.
     */
    public List<History> getRecordsFromCard(Card card) throws DAOException {
        Connection connection = null;
        List<History> records = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
        try(PreparedStatement statement = connection.prepareStatement(SELECT_RECORD_BY_ID_CARD)){
            statement.setInt(1,card.getCardId());
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                History record = new History();
                Book book = new Book();
                book.setBookId(resultSet.getInt(BOOK_ID));
                book.setTitle(resultSet.getString(BOOK_TITLE));
                book.setEditionYear(resultSet.getInt(BOOK_EDITION_YEAR));

                Set<Author> authorsCurrentBook  = getAuthors(book);
                book.setAuthors(authorsCurrentBook);
                record.setRecordId(resultSet.getInt(RECORD_ID));
                record.setPlace(resultSet.getString(PLACE));
                record.setTakingDate(resultSet.getDate(TAKING_DATE));
                record.setDeliveryDate(resultSet.getDate(DELIVERY_DATE));
                record.setBook(book);
                records.add(record);
            }
        }catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return records;
    }

    /**
     * This method is used in a method of return of the book{@see #returnBooks(List<History> records) in BookDAOImpl}.
     * When the user returns the book,
     * in the current record for this book it is necessary to add date of return,
     * and to change the status of the book for 'available'.
     * @param records        updated records
     * @return boolean       if records is successfully updated,it will return true.Otherwise it will return false.
     * @throws DAOException  if connection or sql exception occured.
     */
    public boolean updateRecords(List<History> records) throws DAOException {
        Connection connection = null;
        boolean isUpdate = false;
        int updatedCount = 0;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception",e);
        }
        try(PreparedStatement psRecordUpdate = connection.prepareStatement(UPDATE_RECORD_BY_RETURN)){
            for(int i = 0; i < records.size(); i++){
                psRecordUpdate.setDate(1, new java.sql.Date(records.get(i).getDeliveryDate().getTime()));
                psRecordUpdate.setInt(2, records.get(i).getRecordId());
                updatedCount = psRecordUpdate.executeUpdate();
            }
            if(updatedCount > 0){
                isUpdate = true;
            }
            else{
                isUpdate = false;
            }
        }  catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GeneralDao Exception", e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GeneralDao Exception",e);
            }
        }
        return isUpdate;
    }

}
