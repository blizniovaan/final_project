package by.epam.library.dao.impl;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.*;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BookDAOImpl implements BookDao {
    public static final String SIGN_PERCENT = "%";
    public static final int AVAILABLE_STATUS = 1;
    public static final int NULL = 0;
    public static final String BOOK_TITLE = "title";
    public static final String EDITION_YEAR = "editionYear";
    public static final String DESCRIPTION = "description";
    public static final String BOOK_ID = "bookId";
    public static final String INSERT_IN_BOOK = "INSERT INTO book (title,publisherId,genreId,editionYear,description,statusId) " +
            "VALUES(?,?,?,?,?,?)";
    public static final String INSERT_IN_AUTHOR_HAS_BOOK = "INSERT INTO author_has_book (bookId,authorId) " +
            "VALUES (?,?)";
    public static final String SELECT_ALL_AVAILABLE_BOOKS = "SELECT * FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            " WHERE  book_status.status = 'доступна'" +
            " GROUP BY book.title" ;
    public static final String SELECT_ALL_BOOKS = "SELECT * FROM book  GROUP BY book.title" ;
    public static final String SELECT_AVAILABLE_BOOK_BY_ENTER_WORD ="SELECT * FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            "WHERE (book.title LIKE ? AND book_status.status='доступна')" ;
    public static final String SELECT_ALL_BOOK_BY_ENTER_WORD ="SELECT * FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            "WHERE book.title LIKE ?" ;
    public static final String SELECT_BOOK = "SELECT *" +
            "FROM book " +
            "JOIN book_status ON book_status.statusId = book.statusId " +
            "WHERE book.bookId=?";
    public static final String SELECT_AVAILABLE_BOOK = "SELECT * " +
            "FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            "JOIN genre ON book.genreId = genre.genreId " +
            " WHERE ((book_status.status LIKE 'доступна') AND (book.title LIKE ?) AND (genre.title=?) )" +
            " GROUP BY book.title";
    public static final String SELECT_AVAILABLE_BOOK_BY_LETTER = "SELECT * FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            " WHERE (book_status.status LIKE 'доступна' AND (book.title LIKE ?))" +
            " GROUP BY book.title";
    public static final String SELECT_ALL_BOOK_BY_LETTER = "SELECT * FROM book " +
            " WHERE book.title LIKE?" +
            " GROUP BY book.title";
    public static final String SELECT_AVAILABLE_BOOK_BY_AUTHOR = "SELECT * " +
            "FROM book " +
            "JOIN book_status ON book.statusId = book_status.statusId " +
            "JOIN author_has_book ON book.bookId = author_has_book.bookId " +
            " WHERE ((book_status.status LIKE 'доступна') AND (author_has_book.authorId=?)) " +
            "GROUP BY book.title";
   public static final String SELECT_ALL_BOOKS_BY_LETTER = "SELECT * " +
           "FROM book " +
           "JOIN genre ON book.genreId = genre.genreId " +
           "WHERE ((book.title LIKE ?) AND (genre.title=?) )" +
           " GROUP BY book.title";
    public static final String SELECT_ALL_BOOKS_BY_AUTHOR = "SELECT * FROM book " +
            "JOIN author_has_book ON author_has_book.bookId = book.bookId " +
            "WHERE author_has_book.authorId=?" +
            " GROUP BY book.title";
    public static final String DELETE_BOOK = "DELETE FROM book WHERE bookId=?";
    public static final String SELECT_BOOKS_BY_ID_RECORDS = "SELECT bookId FROM history WHERE history.recordId=?";
    public static final String SELECT_DELIVERY_DATE = "SELECT * FROM history " +
            "WHERE (history.bookId=? AND history.deliveryDate=NULL)";

    private static final BookDAOImpl instance = new BookDAOImpl();
    private BookDAOImpl(){}
    public static BookDAOImpl getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();
    private GeneralDao generalDAO = GeneralDao.getInstance();

    public static final Logger LOGGER = Logger.getLogger(BookDAOImpl.class);

    /**
     * The method is used to return a list of all books.If there are no books in the database,
     * the method will return an empty list.
     * @return List<Book>     all books in the database
     * @throws DAOException   if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAll() throws DAOException {
        Connection connection = null;
        List<Book> books = new ArrayList<>();
        try{
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT_ALL_BOOKS)){
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()){
                    Book book = new Book();
                    book.setBookId(resultSet.getInt(BOOK_ID));

                    String title = resultSet.getString(BOOK_TITLE);
                    book.setTitle(title);

                    book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                    Genre genreBook = generalDAO.selectGenreOfBook(book);
                    book.setGenre(genreBook);

                    Publisher publisher = generalDAO.selectPublisherOfBook(book);
                    book.setPublisher(publisher);

                    book.setDescription(resultSet.getString(DESCRIPTION));

                    BookStatus status = generalDAO.selectStatusOfBook(book);
                    book.setStatus(status);

                    Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                    book.setAuthors(authorsCurrentBook);

                    books.add(book);
                }
            }catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * The method is used to return a list of available books.If there are no books available,
     * the method will return an empty list.
     * @return List<Book>    all available books in the database
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAllAvailable() throws DAOException {
        Connection connection = null;
        List<Book> books = new ArrayList<>();
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("BookDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT_ALL_AVAILABLE_BOOKS)){
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Book book = new Book();
                    book.setBookId(resultSet.getInt(BOOK_ID));

                    String title = resultSet.getString(BOOK_TITLE);
                    book.setTitle(title);

                    book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                    Genre genreBook = generalDAO.selectGenreOfBook(book);
                    book.setGenre(genreBook);

                    Publisher publisher = generalDAO.selectPublisherOfBook(book);
                    book.setPublisher(publisher);

                    book.setDescription(resultSet.getString(DESCRIPTION));

                    BookStatus status = generalDAO.selectStatusOfBook(book);
                    book.setStatus(status);

                    Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                    book.setAuthors(authorsCurrentBook);

                    books.add(book);
                }
            }catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * The method is used to return one required book.If the book is absent in database,
     * the method will return null.
     * @param book           the book to be select
     * @return Book          selected book
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public Book selectOne(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Book preparedBook = null;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT_BOOK)){
                statement.setInt(1,book.getBookId());
                resultSet = statement.executeQuery();

                while (resultSet.next()){
                    preparedBook = new Book();

                    preparedBook.setBookId(resultSet.getInt(BOOK_ID));

                    String title = resultSet.getString(BOOK_TITLE);
                    preparedBook.setTitle(title);

                    preparedBook.setEditionYear(resultSet.getInt(EDITION_YEAR));

                    Genre genreBook = generalDAO.selectGenreOfBook(book);
                    preparedBook.setGenre(genreBook);

                    Publisher publisher = generalDAO.selectPublisherOfBook(book);
                    preparedBook.setPublisher(publisher);

                    preparedBook.setDescription(resultSet.getString(DESCRIPTION));

                    BookStatus status = generalDAO.selectStatusOfBook(book);
                    preparedBook.setStatus(status);

                    Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                    preparedBook.setAuthors(authorsCurrentBook);
                }
            }catch (SQLException e) {
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return preparedBook;
    }

    /**
     * The method is used to insert the book in the table 'book'.Then it inserts authors ID and book ID in
     * the additional table 'author_has_book'.
     * The method returns one inserted book or null.
     * @param book           the book to be insert
     * @return Book          the book,which has been added to the database
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public Book insert(Book book) throws DAOException {
        Book inseredBook = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException(e);
        }
        try(PreparedStatement psInsertInBook = connection.prepareStatement(INSERT_IN_BOOK, Statement.RETURN_GENERATED_KEYS);
            PreparedStatement psInsertInAuthorHasBook = connection.prepareStatement(INSERT_IN_AUTHOR_HAS_BOOK) ){
            connection.setAutoCommit(false);
            psInsertInBook.setString(1, book.getTitle());
            psInsertInBook.setInt(2, book.getPublisher().getPublisherId());
            psInsertInBook.setInt(3,book.getGenre().getGenreId());
            psInsertInBook.setInt(4, book.getEditionYear());
            psInsertInBook.setString(5, book.getDescription());
            psInsertInBook.setInt(6, book.getStatus().getStatusId());

            psInsertInBook.executeUpdate();
            ResultSet rsInsertInBook = psInsertInBook.getGeneratedKeys();
            if(rsInsertInBook.next()){
                inseredBook = new Book();
                inseredBook.setBookId(rsInsertInBook.getInt(1));
            }

            Set<Author> authors = book.getAuthors();
            for(Author author : authors){
                psInsertInAuthorHasBook.setInt(1,inseredBook.getBookId());
                psInsertInAuthorHasBook.setInt(2,author.getAuthorId());
                psInsertInAuthorHasBook.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
            try {
                inseredBook = null;
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error(e1);
                throw new DAOException("BookDAOImpl Exception", e1);
            }
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception", e);
            }
        }
        return inseredBook;
    }

    /**
     * This method is used to delete books.If books haven't been removed, the method would had returned false.
     * Otherwise it would hsd returned true.
     * @param books           books to be remove
     * @return boolean        if books is successfully deleted,it will return true.Otherwise it will return false.
     * @throws DAOException   if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public boolean delete(List<Book> books) throws DAOException {
        Connection connection = null;
        boolean isDelete = false;
        if(!canDelete(books)){
            return isDelete;
        }
        int countDeletedStr = 0;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(DELETE_BOOK)){
               for (Book book : books){
                   Integer bookId = book.getBookId();
                   statement.setInt(1, bookId);
                   countDeletedStr = statement.executeUpdate();
               }
                if(countDeletedStr > NULL){
                    isDelete = true;
                }
                else{
                    isDelete = false;
                }
            } catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return isDelete;
    }
 private boolean canDelete(List<Book> books) throws DAOException {
     Connection connection = null;
     boolean canDelete = true;
     try {
         connection = pool.getConnection();
     }catch (ConnectionException e) {
         LOGGER.error(e);
         throw new DAOException("BookDAOImpl Exception",e);
     }
     try(PreparedStatement statement = connection.prepareStatement(SELECT_DELIVERY_DATE)){
         for (Book book : books){
             statement.setInt(1,book.getBookId());
             ResultSet resultSet = statement.executeQuery();
             if(resultSet.next()){
                 canDelete = false;
                 return canDelete;
             }
         }
     } catch (SQLException e) {
         LOGGER.error(e);
         throw new DAOException("BookDAOImpl Exception",e);
     }finally {
         try {
             if(connection != null){
                 pool.returnConnection(connection);
             }
         } catch (ConnectionException e) {
             LOGGER.error(e);
             throw new DAOException("BookDAOImpl Exception",e);
         }
     }
     return canDelete;
 }
    /**
     * This method is used to select a book defined the genre, which begin with a certain letter.
     * If there is no such books in the database, it will return null.
     * @param letterFrom search option
     * @param genre search option
     * @return List<Book>
     * @throws DAOException if connection or sql exception occured
     * The method needs refactoring.
     */
    public List<Book> selectAllBooksByLetter(String letterFrom,String genre) throws DAOException{
        Connection connection = null;
        StringBuffer letter = new StringBuffer();
        letter.append(letterFrom);
        letter.append(SIGN_PERCENT);
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();

        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BOOKS_BY_LETTER)){
            preparedStatement.setString(1,letter.toString());
            preparedStatement.setString(2,genre);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();

                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook = generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                BookStatus status = generalDAO.selectStatusOfBook(book);
                book.setStatus(status);

                Set<Author> authorsCurrentBook  =  generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);
                books.add(book);
            }

        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method is used to select available book defined the genre, which begin with a certain letter.
     * If there is no such books in the database, it will return null.
     * @param letterFrom search option
     * @param genre search option
     * @return List<Book>
     * @throws DAOException if connection or sql exception occured
     * The method needs refactoring.
     */
    public List<Book> selectAvailableBookByLetter(String letterFrom,String genre) throws DAOException {
        Connection connection = null;
        StringBuffer letter = new StringBuffer();
        letter.append(letterFrom);
        letter.append(SIGN_PERCENT);
        try {
             connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();

        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AVAILABLE_BOOK)){
            preparedStatement.setString(1,letter.toString());
            preparedStatement.setString(2,genre);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();

                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook = generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                Set<Author> authorsCurrentBook  =  generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);
                books.add(book);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method is used to select all list of books which the authors' names begin with a certain letter.
     * It selects all authors using the certain letter.Then it selects all books by these authors.
     * If there is no such books or there is no such authors in the database, it will return empty list.
     * @param letterFrom        first letter of the name of the author
     * @return List<Book>       all the books which the authors' names begin with the certain letter
     * @throws DAOException     if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAllBooksByAuthor(String letterFrom) throws DAOException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();
        List<Author> authors = generalDAO.getAuthorsByLetterFilter(letterFrom);
        try(PreparedStatement preparedStatementBooks = connection.prepareStatement(SELECT_ALL_BOOKS_BY_AUTHOR)){
                for(Author author : authors){
                    Integer authorId = author.getAuthorId();
                    preparedStatementBooks.setInt(1,authorId);

                    resultSet = preparedStatementBooks.executeQuery();

                    while (resultSet.next()){
                        Book book = new Book();

                        book.setBookId(resultSet.getInt(BOOK_ID));

                        book.setTitle(resultSet.getString(BOOK_TITLE));

                        book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                        Genre genreBook = generalDAO.selectGenreOfBook(book);
                        book.setGenre(genreBook);

                        Publisher publisher = generalDAO.selectPublisherOfBook(book);
                        book.setPublisher(publisher);

                        BookStatus status = generalDAO.selectStatusOfBook(book);
                        book.setStatus(status);

                        Set<Author> authorsCurrentBook  =  generalDAO.getAuthors(book);
                        book.setAuthors(authorsCurrentBook);
                        books.add(book);
                    }
                }
            } catch (SQLException  e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }
    /**
     * This method is used to select available list of books which the authors' names begin with a certain letter.
     * It selects all authors using the certain letter.Then it selects available books by these authors.
     * If there is no such books or there is no such authors in the database, it will return empty list.
     * @param letterFrom         first letter of the name of the author
     * @return List<Book>        all the books which the authors' names begin with the certain letter
     * @throws DAOException      if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAvailableBooksByAuthor(String letterFrom) throws DAOException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException("BookDAOImpl Exception",e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();

        List<Author> authors = generalDAO.getAuthorsByLetterFilter(letterFrom);
            try(PreparedStatement preparedStatementBooks = connection.prepareStatement(SELECT_AVAILABLE_BOOK_BY_AUTHOR)){
                for(Author author : authors){
                    preparedStatementBooks.setInt(1,author.getAuthorId());

                    resultSet = preparedStatementBooks.executeQuery();

                    while (resultSet.next()){
                        Book book = new Book();

                        book.setBookId(resultSet.getInt(BOOK_ID));

                        book.setTitle(resultSet.getString(BOOK_TITLE));

                        book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                        Genre genreBook = generalDAO.selectGenreOfBook(book);
                        book.setGenre(genreBook);

                        Publisher publisher = generalDAO.selectPublisherOfBook(book);
                        book.setPublisher(publisher);

                        BookStatus status = generalDAO.selectStatusOfBook(book);
                        book.setStatus(status);

                        Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                        book.setAuthors(authorsCurrentBook);
                        books.add(book);
                    }
                }
            } catch (SQLException  e) {
                LOGGER.error(e);
                throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method is used to return the list of available books which titles contain the string entered by the user.
     * If there is no such books in the database, it will return empty list.
     * @param enterWord       the string or substring which will be found in the title of the book.
     * @return List<Book>     all books which names contain the entered word.
     * @throws DAOException   if connection or sql exception occured.
     * The method needs refactoring.
     */
    public List<Book> selectAvailableByEnterWord(String enterWord) throws DAOException{
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException(e);
        }
        List<Book> books = new ArrayList<>();
        StringBuffer word = new StringBuffer();
        word.append(SIGN_PERCENT );
        word.append(enterWord.trim());
        word.append(SIGN_PERCENT);
        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AVAILABLE_BOOK_BY_ENTER_WORD)){
            preparedStatement.setString(1,word.toString());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();

                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook =generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                BookStatus status = generalDAO.selectStatusOfBook(book);
                book.setStatus(status);

                Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);
                books.add(book);
            }

        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }
    /**
     * This method is used to return the list of all books which titles contain the string entered by the user.
     * If there is no such books in the database, it will return empty list.
     * @param enterWord        the string or substring which will be found in the title of the book.
     * @return List<Book>      all books which names contain the entered word.
     * @throws DAOException    if connection or sql exception occured.
     * The method needs refactoring.
     */
    public List<Book> selectAllByEnterWord(String enterWord) throws DAOException{
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();
        StringBuffer word = new StringBuffer();
        word.append(SIGN_PERCENT );
        word.append(enterWord.trim());
        word.append(SIGN_PERCENT);
        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BOOK_BY_ENTER_WORD)){
            preparedStatement.setString(1,word.toString());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();

                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook = generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                BookStatus status = generalDAO.selectStatusOfBook(book);
                book.setStatus(status);

                Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);
                books.add(book);
            }

        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method is used to select available book, which begins with a certain letter.
     * If there is no such books in the database, it will return empty list.
     * @param letterFrom     search option
     * @return List<Book>
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAvailableBooksByLetter(String letterFrom) throws DAOException {
        Connection connection = null;
        StringBuffer letter = new StringBuffer();
        letter.append(letterFrom);
        letter.append(SIGN_PERCENT);
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException(e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AVAILABLE_BOOK_BY_LETTER)){
            preparedStatement.setString(1, letter.toString());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();

                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook = generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                BookStatus status = generalDAO.selectStatusOfBook(book);
                book.setStatus(status);

                Set<Author> authorsCurrentBook  = generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);

                books.add(book);
            }

        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method is used to select books of the user from the library card.It selects card of the user.
     * Then it selects all records which belong to the card of the user from table 'history' and returns them.
     * @param user            the user whose books will be select
     * @return List<History>  list of records with
     * @throws DAOException   if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<History> getBooksFromCard(User user) throws DAOException {
        Card card = generalDAO.getCard(user);
        List<History> records = generalDAO.getRecordsFromCard(card);
        return records;
    }
    /**
     * This method is used to select all books, which begin with a certain letter.
     * If there is no such books in the database, it will return empty list.
     * @param letterFrom      search option
     * @return List<Book>
     * @throws DAOException   if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<Book> selectAllBooksByLetter(String letterFrom) throws DAOException {
        Connection connection = null;
        StringBuffer letter = new StringBuffer();
        letter.append(letterFrom);
        letter.append(SIGN_PERCENT);
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
        ResultSet resultSet = null;
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BOOK_BY_LETTER)){
            preparedStatement.setString(1, letter.toString());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Book book = new Book();
                book.setBookId(resultSet.getInt(BOOK_ID));

                book.setTitle(resultSet.getString(BOOK_TITLE));

                book.setEditionYear(resultSet.getInt(EDITION_YEAR));

                Genre genreBook = generalDAO.selectGenreOfBook(book);
                book.setGenre(genreBook);

                Publisher publisher = generalDAO.selectPublisherOfBook(book);
                book.setPublisher(publisher);

                BookStatus status = generalDAO.selectStatusOfBook(book);
                book.setStatus(status);

                Set<Author> authorsCurrentBook  =  generalDAO.getAuthors(book);
                book.setAuthors(authorsCurrentBook);

                books.add(book);
            }
        } catch (SQLException  e) {
            throw new DAOException(e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return books;
    }

    /**
     * This method used to return books in library.In table 'history' there is change delivery date from null to
     * current date,then there is update status of book from 'unavailable' to 'available'.
     * @param records         records,which will are update
     * @return List<History>
     * @throws DAOException   if connection or sql exception occured
     * The method needs refactoring.
     */
    @Override
    public List<History> returnBooks(List<History> records) throws DAOException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }
        try{
            connection.setAutoCommit(false);
            generalDAO.updateRecords(records);
            List<Book> books = getBooksByRecord(records);
            generalDAO.updateBookStatus(books, AVAILABLE_STATUS);
            connection.commit();
            }  catch (SQLException e) {
            try {
                records = null;
                connection.rollback();
            } catch (SQLException e1) {
                throw new DAOException("BookDAOImpl Exception", e1);
            }
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return records;
    }

    /**
     * This method returns books,which correspond to records.
     * @param records
     * @return List<Book>    list of books
     * @throws DAOException  if connection or sql exception occured
     * The method needs refactoring.
     */
    private List<Book> getBooksByRecord(List<History> records) throws DAOException {
        Connection connection = null;
        List<Book> resultBooks = new ArrayList<>();
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            throw new DAOException(e);
        }
        try( PreparedStatement statement = connection.prepareStatement(SELECT_BOOKS_BY_ID_RECORDS) ){
            for (History record : records){
                statement.setInt(1,record.getRecordId());
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    Book book = new Book();
                    book.setBookId(resultSet.getInt(BOOK_ID));
                    resultBooks.add(book);
                }
            }
        } catch (SQLException  e) {
            LOGGER.error(e);
            throw new DAOException("BookDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("BookDAOImpl Exception",e);
            }
        }
        return resultBooks;
    }

    @Override
    public boolean delete(Book entity) throws DAOException {
       return false;
    }
    @Override
    public Book update(Book entity) {
        return null;
    }
}
