package by.epam.library.dao.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.GenreDao;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.*;
import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreDAOImpl implements GenreDao {
    public static final Logger LOGGER = Logger.getLogger(GenreDAOImpl.class);

    public static final String GENRE_ID = "genreId";
    public static final String TITLE = "title";

    public static final String SELECT_ALL_GENRE = "SELECT * FROM genre GROUP BY genre.title";
    public static final String INSERT = "INSERT INTO genre (title) VALUES (?)";
    public static final String UPDATE = "UPDATE genre SET title=? WHERE genreId=?";
    public static final String DELETE = "DELETE FROM genre WHERE genreId=?";
    public static final String SELECT = "SELECT * FROM genre JOIN book ON book.genreId=genre.genreId WHERE book.bookId=?";


    private static final GenreDAOImpl instance = new GenreDAOImpl();
    private GenreDAOImpl(){}
    public static GenreDAOImpl getInstance(){return instance;}

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public List<Genre> selectAll() throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        List<Genre> genres = new ArrayList<>();
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("GenreDAOImpl Exception",e);
        }
            try(PreparedStatement statement = connection.prepareStatement(SELECT_ALL_GENRE)){
                resultSet = statement.executeQuery();

                while (resultSet.next()){
                    Genre genre = new Genre();
                    String title = resultSet.getString(TITLE);
                    Integer genreId = resultSet.getInt(GENRE_ID);
                    genre.setGenreId(genreId);
                    genre.setTitle(title);
                    genres.add(genre);
                }
            }catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
            }
        }
        return genres;
    }


    @Override
    public Genre insert(Genre genre) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Genre inseredGenre = null;
        try{
            connection = pool.getConnection();
            try(PreparedStatement psAddGenre = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)){
                psAddGenre.setString(1,genre.getTitle());
                psAddGenre.executeUpdate();

                resultSet = psAddGenre.getGeneratedKeys();

                if (resultSet.next()){
                    inseredGenre = new Genre();
                    inseredGenre.setGenreId(resultSet.getInt(1));
                }
            }
        }catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GenreDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
            }
        }
        return inseredGenre;
    }

    @Override
    public boolean delete(List<Genre> genres) throws DAOException {
        Connection connection = null;
        boolean isDelete = false;
        int countRemovedStr = 0;
        try {
            connection = pool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(DELETE)){
                for (Genre genre: genres){
                    statement.setInt(1, genre.getGenreId());
                    countRemovedStr += statement.executeUpdate();
                }
                if(countRemovedStr > 0){
                    isDelete = true;
                }
                else{
                    isDelete = false;
                }
            }
        } catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GenreDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
            }
        }
        return isDelete;
    }

    @Override
    public Genre update(Genre genre) throws DAOException {
        Connection connection = null;
        Genre editedGenre = null;
        try{
            connection = pool.getConnection();
            try(PreparedStatement psUpdate = connection.prepareStatement(UPDATE,Statement.RETURN_GENERATED_KEYS)){
                psUpdate.setString(1, genre.getTitle());
                psUpdate.setInt(2, genre.getGenreId());
                int countEditedStr = psUpdate.executeUpdate();
                if(countEditedStr > 0){
                    editedGenre = new Genre();
                    editedGenre.setGenreId(genre.getGenreId());
                }
            }
        }catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GenreDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
            }
        }
        return editedGenre;
    }

    @Override
    public Genre select(Book book) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        Genre genre = null;
        try{
            connection = pool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(SELECT)){
                statement.setInt(1, book.getBookId());
                resultSet = statement.executeQuery();

                if (resultSet.next()){
                    genre = new Genre();
                    genre.setGenreId(resultSet.getInt(GENRE_ID));
                    genre.setTitle(resultSet.getString(TITLE));
                }
            }
        }catch (ConnectionException | SQLException e) {
            LOGGER.error(e);
            throw new DAOException("GenreDAOImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("GenreDAOImpl Exception",e);
            }
        }
        return genre;
    }

    @Override
    public Genre selectOne(Genre entity) {
        return null;
    }

    @Override
    public boolean delete(Genre entity) {
        return false;
    }

}
