package by.epam.library.dao.impl;

import by.epam.library.dao.DAOException;
import by.epam.library.dao.HistoryDao;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.*;
import com.mysql.jdbc.Statement;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class HistoryDaoImpl implements HistoryDao {

    public static final Logger LOGGER = Logger.getLogger(HistoryDaoImpl.class);

    public static final String INSERT_INTO_HISTORY = "INSERT INTO history (bookId,cardId,takingDate,place) " +
            "VALUES (?,?,?,?)";
    public static final String SELECT_RECORD_BY_ID_CARD= "SELECT * FROM history " +
            "JOIN book ON book.bookId=history.bookId WHERE history.cardId=?";
    public static final String UPDATE_RECORD_BY_RETURN = "UPDATE history SET history.deliveryDate=? " +
            "WHERE history.recordId=?";
    public static final String DELETE = "DELETE FROM history WHERE recordId=?";
    public static final String CARD_ID = "cardId";
    public static final String BOOK_TITLE = "book.title";
    public static final String BOOK_EDITION_YEAR = "book.editionYear";
    public static final String BOOK_ID = "book.bookId";
    public static final String PLACE = "place";
    public static final String TAKING_DATE = "takingDate";
    public static final String DELIVERY_DATE = "deliveryDate";
    public static final String RECORD_ID = "recordId";
    public static final int UNAVAILABLE_STATUS = 2;
    public static final int NULL = 0;
    public static final int FIRST_ELEMENT = 0;

    private static final HistoryDaoImpl instance = new HistoryDaoImpl();
    private HistoryDaoImpl() {
    }
    public static HistoryDaoImpl getInstance() {
        return instance;
    }

    private GeneralDao generalDAO = GeneralDao.getInstance();

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public List<History> insertPart(History history,List<Book> books) throws DAOException {
        List<History> records = new ArrayList<>();
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
    try {
            connection.setAutoCommit(false);
            Card card = generalDAO.getCard( history.getUser());
            records = insert(history, books, card);
            generalDAO.updateBookStatus(books, UNAVAILABLE_STATUS);
            connection.commit();
        } catch (SQLException e) {
          LOGGER.error(e);
            try {
                records = null;
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error(e1);
                throw new DAOException("HistoryDaoImpl Exception", e1);
            }
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("HistoryDaoImpl Exception", e);
            }
        }
      return records;
    }


    @Override
    public boolean update(List<History> records) throws DAOException {
        Connection connection = null;
        boolean isUpdate = false;
        int updatedCount = 0;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException("HistoryDaoImpl Exception",e);
        }
        try(PreparedStatement psRecordUpdate = connection.prepareStatement(UPDATE_RECORD_BY_RETURN)){
            for(int i = 0; i < records.size(); i++){
                psRecordUpdate.setDate(1, new java.sql.Date(records.get(i).getDeliveryDate().getTime()));
                psRecordUpdate.setInt(2, records.get(i).getRecordId());
                updatedCount = psRecordUpdate.executeUpdate();
            }
            if(updatedCount > NULL){
                isUpdate = true;
            }
            else{
                isUpdate = false;
            }
        }  catch (SQLException e) {
                LOGGER.error(e);
                throw new DAOException("HistoryDaoImpl Exception", e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("HistoryDaoImpl Exception",e);
            }
        }
        return isUpdate;
    }

    @Override
    public History insert(History history) throws DAOException {
        List<Book> books = new ArrayList<>();
        Book book = history.getBook();
        books.add(book);

        List<History> records = null;
        History inseredRecord = new History();
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
        try {
            connection.setAutoCommit(false);
            Card card = generalDAO.getCard( history.getUser());
            records = insert(history, books, card);
            generalDAO.updateBookStatus(books, UNAVAILABLE_STATUS);
            if(records.get(FIRST_ELEMENT) != null){
                inseredRecord = records.get(FIRST_ELEMENT);
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
            try {
                inseredRecord = null;
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error(e1);
                throw new DAOException("HistoryDaoImpl Exception", e1);
            }
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("HistoryDaoImpl Exception", e);
            }
        }
        return inseredRecord;
    }

    @Override
    public List<History> insert(History history,List<Book> books, Card card) throws DAOException {
        History record = new History();
        List<History> records = new ArrayList<>();
        Connection connection = null;
        try {
            connection = pool.getConnection();
        } catch (ConnectionException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        }
        try(PreparedStatement psInsertIntoHistory = connection.prepareStatement(INSERT_INTO_HISTORY,Statement.RETURN_GENERATED_KEYS)){
            for(Book book : books){
                psInsertIntoHistory.setInt(1, book.getBookId());
                psInsertIntoHistory.setInt(2, card.getCardId());
                Date takingDate = history.getTakingDate();
                psInsertIntoHistory.setDate(3,new java.sql.Date(takingDate.getTime()));
                psInsertIntoHistory.setString(4,  history.getPlace());
                psInsertIntoHistory.executeUpdate();

                ResultSet rsInsertIntoHistory = psInsertIntoHistory.getGeneratedKeys();
                while (rsInsertIntoHistory.next()){
                    record.setRecordId(rsInsertIntoHistory.getInt(1));
                    records.add(record);
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException("HistoryDaoImpl Exception", e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e1) {
                LOGGER.error(e1);
                throw new DAOException("HistoryDaoImpl Exception", e1);
            }
        }
        return records;
    }


    @Override
    public boolean delete(History record) throws DAOException {
        boolean isDelete = false;
        Connection connection = null;
        int countRemovedStr = 0;
        try {
            connection = pool.getConnection();
        }catch (ConnectionException e) {
            throw new DAOException("HistoryDaoImpl Exception",e);
        }
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            statement.setInt(1, record.getRecordId());
            countRemovedStr += statement.executeUpdate();

            if(countRemovedStr > 0){
                isDelete = true;
            }
            else{
                isDelete = false;
            }
        } catch ( SQLException e) {
            LOGGER.error(e);
            throw new DAOException("HistoryDaoImpl Exception",e);
        }finally {
            try {
                if(connection != null){
                    pool.returnConnection(connection);
                }
            } catch (ConnectionException e) {
                LOGGER.error(e);
                throw new DAOException("HistoryDaoImpl Exception",e);
            }
        }
        return isDelete;
    }

    @Override
    public boolean delete(List<History> entity) throws DAOException {
        return false;
    }

    @Override
    public History update(History entity) throws DAOException {
        return null;
    }

    @Override
    public List<History> selectAll() throws DAOException {
        return null;
    }

    @Override
    public History selectOne(History entity) throws DAOException {
        return null;
    }


}
