package by.epam.library.dao.impl;

import by.epam.library.dao.CardDao;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import by.epam.library.entity.Card;
import by.epam.library.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by HP on 06.02.2016.
 */
public class CardDAOImpl implements CardDao {
    public static final Logger LOGGER = Logger.getLogger(CardDAOImpl.class);
    public static final String CARD_ID = "cardId";
    public static final String SELECT_ID_CARD= "SELECT * FROM card WHERE card.userId = ?";

    private static final CardDAOImpl instance = new CardDAOImpl();
    private CardDAOImpl(){}
    public static CardDAOImpl getInstance(){return instance;}

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public Card select(User user) throws DAOException {
        return null;
    }
    @Override
    public List<Card> selectAll() {
        return null;
    }

    @Override
    public Card selectOne(Card entity) {
        return null;
    }

    @Override
    public Card insert(Card entity) {
        return null;
    }

    @Override
    public boolean delete(Card entity) {
        return false;
    }

    @Override
    public boolean delete(List<Card> entity) throws DAOException {
        return false;
    }

    @Override
    public Card update(Card entity) {
        return null;
    }

}
