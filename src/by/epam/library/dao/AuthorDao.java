package by.epam.library.dao;

import by.epam.library.entity.Author;
import by.epam.library.entity.Book;

import java.util.List;
import java.util.Set;

public interface AuthorDao extends Dao<Author> {
    /**
     * This method is used to select all authors from the database.
     * @return List<Author>   all authors
     * @throws DAOException   if connection or sql exception occured
     */
    @Override
    List<Author> selectAll() throws DAOException;
    /**
     * This method is used to select all authors of one book.
     * @return Set<Author>    all authors of the book
     * @throws DAOException   if connection or sql exception occured
     */
    Set<Author> select(Book book) throws DAOException;

    /**
     * This method is used to insert an author in the database.
     * @return Author        inserted author
     * @throws DAOException  if connection or sql exception occured
     */
    @Override
    Author insert(Author author) throws DAOException;
    /**
     * This method is used to insert an author in the database.
     * @return boolean       if author is successfully deleted,it will return true.Otherwise it will return false.
     * @throws DAOException  if connection or sql exception occured
     */
    @Override
    boolean delete(Author entity);
    /**
     * This method is used to update an information(name) about author.
     * @return Author          updated author
     * @throws DAOException    if connection or sql exception occured
     */
    @Override
    Author update(Author author) throws DAOException;
}
