package by.epam.library.controller;

/**
 * Created by HP on 02.02.2016.
 */
public final class RequestParameterName {
    private RequestParameterName() {}
    public static final String PAGE = "page";
    public static final String PART_BOOKS = "partBooks";
    public static final String NAME = "name";
    public static final String NEW_NAME = "newName";
    public static final String SURNAME = "surname";
    public static final String LOGIN = "login";
    public static final String USER_ID = "userId";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";

    public static final String USERS_LIST = "users";
    public static final String USERS_ID = "usersId";
    public static final String USER = "user";

    public static final String LETTER_FROM = "letterFrom";
    public static final String UTF_8_ENCODING ="UTF-8";
    public static final String ISO_8859_1_ENCODING ="ISO-8859-1";


    public static final String BOOK_ID = "bookId";
    public static final String BOOK_TITLE = "bookTitle";
    public static final String EDITION_YEAR = "editionYear";
    public static final String AUTHOR_ID = "authorId";
    public static final String GENRE_ID = "genreId";
    public static final String DESCRIPTION = "description";
    public static final String PUBLISHER_ID = "publisherId";
    public static final String BOOKS = "books";
    public static final String AUTHOR ="author";
    public static final String AUTHORS ="authors";
    public static final String GENRES ="genres";
    public static final String GENRE ="genre";
    public static final String BOOK_PLACE ="place";
    public static final String BOOKS_ID ="booksId";
    public static final String PUBLISHERS ="publishers";

    public static final String BOOK_ATTRIBUTE ="book";

    public static final String LANGUAGE = "language";

    public static final String ENTER_WORD = "enterWord";
    public static final String RECORDS = "records";
    public static final String RECORDS_ID = "recordsId";

    public static final String SEARCH_PARAMETER = "searchParameter";
    public static final String LETTER_PARAMETER = "letter";

    public static final String COMMAND = "command";
    public static final String DELETE_COMMAND = "delete";
    public static final String TITLE = "title";
    public static final String TYPE_POPUP = "typePopup";
    public static final String REPEAT_PASSWORD="repeatPassword";

}
