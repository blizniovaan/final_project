package by.epam.library.controller;

/**
 * Created by HP on 29.02.2016.
 */
public class SessionParameterName {
    private SessionParameterName(){}

    public static final String BOOK_OBJECT = "book";
    public static final String WORK_OBJECT = "workObject";
    public static final String ROLE = "role";
    public static final String PREV_NUMBER = "prevNumber";
    public static final String CURRENT_RANDOM_NUMBER = "curRandomNumber";
    public static final String USER = "user";
}
