package by.epam.library.controller;

public final class JspPageName {
    private JspPageName() {}
    public static final String USER_PAGE = "templates/userPages/userPage.jsp";
    public static final String ADMIN_BOOKS_PAGE = "templates/adminPages/adminBooksPage.jsp";
    public static final String ADMIN_BOOK_PAGE = "templates/adminPages/adminBookPage.jsp";
    public static final String ADMIN_USERS_PAGE = "templates/adminPages/adminUsersPage.jsp";
    public static final String ERROR_PAGE = "templates/error/error.jsp";
    public static final String ERROR_NOT_FOUND_USER_PAGE = "templates/error/notFoundPage.jsp";
    public static final String GENRE_PAGE = "templates/userPages/genrePage.jsp";
    public static final String MAIN_PAGE = "index.jsp";
    public static final String SIGN_IN_AGAIN_PAGE = "templates/error/signInAgainPage.jsp";
    public static final String BOOK_PAGE = "templates/userPages/bookPage.jsp";
    public static final String USER_ERROR = "templates/error/registrationError.jsp";
    public static final String RESULT_OF_SEARCH_FOR_USER_PAGE = "templates/userPages/searchResult.jsp";
    public static final String RESULT_OF_SEARCH_FOR_ADMIN_PAGE = "templates/adminPages/adminSearchResult.jsp";
    public static final String USER_CARD = "templates/userPages/card.jsp";
    public static final String SETTINGS = "templates/settings/settings.jsp";
    public static final String CONFIRMATION = "templates/modals/confirmation/confirmation.jsp";
    public static final String CONFIRMATION_RETURN_BOOK = "templates/modals/confirmation/confirmReturnBook.jsp";
    public static final String CONFIRMATION_ADD_BOOK = "templates/modals/confirmation/confirmAddBook.jsp";
    public static final String CONFIRMATION_EDIT_PUBLISHER = "templates/modals/confirmation/confirmEditPublisher.jsp";
    public static final String CONFIRMATION_EDIT_AUTHOR = "templates/modals/confirmation/confirmEditAuthor.jsp";
    public static final String CONFIRMATION_REMOVE_USER = "templates/modals/confirmation/confirmRemoveUser.jsp";
    public static final String SIGN_IN_POPUP = "templates/modals/signin.jsp";
    public static final String SIGN_UP_POPUP = "templates/modals/signup.jsp";

    public static final String CARD_PAGE = "templates/userPages/card.jsp";
    public static final String ADD_BOOK_PAGE = "templates/adminPages/addBookPage.jsp";
    public static final String EDIT_PUBLISHER_PAGE = "templates/adminPages/editPublisherPage.jsp";
    public static final String EDIT_AUTHOR_PAGE = "templates/adminPages/editAuthorPage.jsp";
    public static final String EDIT_GENRE_PAGE = "templates/adminPages/editGenrePage.jsp";
    public static final String CONFIRMATION_GENRE = "templates/modals/confirmation/confirmEditGenre.jsp";
    public static final String ADD_USER_PAGE = "templates/adminPages/addUserPage.jsp";
    public static final String GIVE_BOOK_PAGE = "templates/adminPages/giveBookPage.jsp";
    public static final String CONFIRMATION_GIVE_BOOK = "templates/modals/confirmation/confirmGiveBook.jsp";
    public static final String USER_INFO_PAGE = "templates/adminPages/userInfoPage.jsp";

    public static String getPage(String page){
        Pages currentPage = Pages.valueOf(page);
        switch (currentPage){
            case ADMIN_BOOKS_PAGE:{
                return JspPageName.ADMIN_BOOKS_PAGE;
            }
            case ADMIN_BOOK_PAGE:{
                return JspPageName.ADMIN_BOOK_PAGE;
            }
            case SETTINGS:{
                return JspPageName.SETTINGS;
            }
            case USER_PAGE:{
                return JspPageName.USER_PAGE;
            }
            case CARD_PAGE:{
                return JspPageName.CARD_PAGE;
            }
            case CONFIRMATION_GIVE_BOOK:{
                return JspPageName.CONFIRMATION_GIVE_BOOK;
            }
            case MAIN_PAGE:{
                return JspPageName.MAIN_PAGE;
            }
            case BOOK_PAGE:{
                return JspPageName.BOOK_PAGE;
            }
            case ADD_BOOK_PAGE:{
                return JspPageName.ADD_BOOK_PAGE;
            }
            case EDIT_PUBLISHER_PAGE:{
                return JspPageName.EDIT_PUBLISHER_PAGE;
            }
            case EDIT_GENRE_PAGE:{
                return JspPageName.EDIT_GENRE_PAGE;
            }
            case EDIT_AUTHOR_PAGE:{
                return JspPageName.EDIT_AUTHOR_PAGE;
            }
            case ADD_USER_PAGE:{
                return JspPageName.ADD_USER_PAGE;
            }
            case CONFIRMATION_GENRE:{
                return JspPageName.CONFIRMATION_GENRE;
            }
            case CONFIRMATION:{
                return JspPageName.CONFIRMATION;
            }
            case RESULT_OF_SEARCH_FOR_USER_PAGE:{
                return JspPageName.RESULT_OF_SEARCH_FOR_USER_PAGE;
            }
            case CONFIRMATION_ADD_BOOK:{
                return JspPageName.CONFIRMATION_ADD_BOOK;
            }
            case GIVE_BOOK_PAGE:{
                return JspPageName.GIVE_BOOK_PAGE;
            }
            case USER_INFO_PAGE:{
                return JspPageName.USER_INFO_PAGE;
            }
            case RESULT_OF_SEARCH_FOR_ADMIN_PAGE:{
                return JspPageName.RESULT_OF_SEARCH_FOR_ADMIN_PAGE;
            }
            case GENRE_PAGE:{
                return JspPageName.GENRE_PAGE;
            }
            case CONFIRMATION_EDIT_PUBLISHER:{
                return JspPageName.CONFIRMATION_EDIT_PUBLISHER;
            }
            default:{
                return JspPageName.ADD_BOOK_PAGE;
            }
        }
    }
    enum Pages{
        ADMIN_BOOKS_PAGE,USER_PAGE,MAIN_PAGE,CARD_PAGE,SETTINGS,
        BOOK_PAGE,ADMIN_BOOK_PAGE,ADD_BOOK_PAGE,EDIT_PUBLISHER_PAGE,EDIT_GENRE_PAGE,
        EDIT_AUTHOR_PAGE,ADD_USER_PAGE,CONFIRMATION_GENRE,CONFIRMATION,
        CONFIRMATION_RETURN_BOOK,RESULT_OF_SEARCH_FOR_USER_PAGE,CONFIRMATION_ADD_BOOK,
        GIVE_BOOK_PAGE,CONFIRMATION_GIVE_BOOK,USER_INFO_PAGE,RESULT_OF_SEARCH_FOR_ADMIN_PAGE,
        GENRE_PAGE, JspPageName, CONFIRMATION_EDIT_PUBLISHER
    }
}
