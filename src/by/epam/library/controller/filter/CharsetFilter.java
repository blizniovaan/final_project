package by.epam.library.controller.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by HP on 02.02.2016.
 */
public class CharsetFilter implements Filter {
    public static final String ENCODING = "encoding";
    private String encoding = "UTF-8";
    public void doFilter(ServletRequest request,ServletResponse response,FilterChain filterChain)throws IOException, ServletException {
        request.setCharacterEncoding(encoding);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    public void init(FilterConfig filterConfig) throws ServletException {
        String encodingParam = filterConfig.getInitParameter(ENCODING);
        if (encodingParam != null) {
            encoding = encodingParam;
        }
    }
}
