package by.epam.library.controller.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {
    static final String LANGUAGE = "en";
    static final String PARAMETER_LANGUAGE = "language";
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        if(httpSessionEvent.getSession().getAttribute(PARAMETER_LANGUAGE) == null){
            httpSessionEvent.getSession().setAttribute(PARAMETER_LANGUAGE, LANGUAGE);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().removeAttribute(PARAMETER_LANGUAGE);
    }
}
