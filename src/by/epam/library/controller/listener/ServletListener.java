package by.epam.library.controller.listener;

import by.epam.library.dao.connect.ConnectionException;
import by.epam.library.dao.connect.ConnectionPool;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;

public class ServletListener implements javax.servlet.ServletContextListener {
    static final String INIT_PARAMETER = "init_log4j";
    static final String SLASH = "/";
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool.getInstance().initialize();
        String prefix = servletContextEvent.getServletContext().getRealPath(SLASH);
        String filename = servletContextEvent.getServletContext().getInitParameter(INIT_PARAMETER);
        if (filename != null) {
            PropertyConfigurator.configure(prefix + filename);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().releasePool();
        } catch (ConnectionException e) {
            throw new RuntimeException(e);
        }
    }
}
