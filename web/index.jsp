<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="language"/>

<fmt:message bundle="${language}" key="language.button_ru" var="ru_button" />
<fmt:message bundle="${language}" key="language.button_en" var="en_button" />
<fmt:message bundle="${language}" key="welcomepage.slogan"  var="welcomepage_slogan"/>
<fmt:message bundle="${language}" key="welcomepage.greeting" var="welcomepage_greeting" />
<fmt:message bundle="${language}" key="welcomepage.text1" var="welcomepage_text1" />
<fmt:message  bundle="${language}" key="welcomepage.text2" var="welcomepage_text2"/>

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="templates/resources.jsp"/>
  <script src="scripts/home.js" type="text/javascript"></script>
</head>

<body>
<jsp:include page="templates/header/welcomeHeader.jsp"/>
<div class="wrapper">
  <div class="container">

    <div class="content-welcome-page" id="content-welcome-page">
      <div>
        <p>${welcomepage_slogan}</p>
        <h1>${welcomepage_greeting}</h1>
        <h2>${welcomepage_text1}<br>${welcomepage_text2}</h2>
      </div>
    </div>
  </div>
</div>

<footer class="page-footer">
  <div class="footer-bg">
    <form class="btn_ru" action="controller" method="post">
      <input type="hidden" name="language" value="ru" />
      <input type="submit" class="but"   value="${ru_button}" />
      <input type="hidden" name="page" value="MAIN_PAGE"/>
      <input type="hidden" name="command" value="LOCALIZATION"/>
    </form>

    <form class="btn_en" action="controller" method="post">
      <input type="hidden" name="language" value="en" />
      <input type="submit" class="but"  value="${en_button}" >
      <input type="hidden" name="page" value="MAIN_PAGE"/>
      <input type="hidden" name="command" value="LOCALIZATION"/>
    </form>
  </div>
</footer>

<jsp:include page="templates/modals/signin.jsp"/>
<jsp:include page="templates/modals/signup.jsp"/>
</body>
</html>
