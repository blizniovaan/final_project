window.onload = function() {
	var confirmation = document.getElementById('confirmation-popup'),
		form = document.getElementById('take-book-form');

	confirmation.onclick = close.bind(null, 'confirmation-popup');
	form.onsubmit = takeBook;

	function takeBook(event){
		var elem = event.target,
			selectedOption = document.getElementById('place').value,
			body = 'command=' + encodeURIComponent('TAKE_BOOK') +
				'&userId=' + encodeURIComponent(elem.querySelector('#userid').value) +
				'&bookId=' + encodeURIComponent(elem.querySelector('#bookid').value) +
				'&genre=' + encodeURIComponent(elem.querySelector('#genre').value) +
				'&place=' + selectedOption;
		
		function error() {
			open('confirmation-popup');
			document.getElementById('confirmation-message').innerText = 'Error.Try again.';
		}

		function success() {
			open('confirmation-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}

		post(body, success, error);
		return false;
	}
}