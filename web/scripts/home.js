window.onload = function () {
	var signin = document.getElementById('signin'),
		signup = document.getElementById('signup'),
		signinOverlay = document.getElementById('signin-popup'),
		signupOverlay = document.getElementById('signup-popup'),
		signinSubmit = document.getElementById('signin-form'),
		signupSubmit = document.getElementById('signup-form'),
		signinPopupContainer = document.getElementById('signin-popup-container'),
		signupPopupContainer = document.getElementById('signup-popup-container');

	//var	PASS_REG = new RegExp('^[a-zA-Z]\\w{6,18}'),
	var	PASS_REG = new RegExp('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,18}$'),
	    AUTH_REG = new RegExp('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,18}$'),
		NAME_REG = new RegExp('^[A-ZА-Я][A-ZА-Яа-яa-z-]{1,18}'),
		SURNAME_REG = new RegExp('^[A-ZА-Я][A-ZА-Яа-яa-z-]{1,18}');

	signin.onclick = openPopup.bind(null, 'signin-popup');
	signup.onclick = openPopup.bind(null, 'signup-popup');
	signinOverlay.onclick = close.bind(null, 'signin-popup');
	signupOverlay.onclick = close.bind(null, 'signup-popup');
	signinSubmit.onsubmit = sendSignin;
	signupSubmit.onsubmit = sendSignup;
	signinPopupContainer.onclick = stopProp;
	signupPopupContainer.onclick = stopProp;

	function openPopup(type) {
		post('command=SET_SESSION_ATTRIBUTE&typePopup=' + type, success, error);

		function success() {
			open(type);
		}

		function error() {
			console.log('Error');
		}
	}

	function validate(query, regExp, errorElement) {
		if(!query.match(regExp) || query.match(regExp)[0] != query) {
			errorElement.className = errorElement.className.replace('hide', 'show');
			return false;
		}
		return true;
	}

	function sendSignin(event) {
		var elem = event.target,
			error_el = elem.querySelector('#error'),
			error_name = elem.querySelector('#error-name'),
			error_password = elem.querySelector('#error-password'),
			name = encodeURIComponent(elem.querySelector('#login').value),
			password = encodeURIComponent(elem.querySelector('#password').value),
			command = encodeURIComponent('LOGIN');

		error_name.className = error_name.className.replace('show', 'hide');
		error_password.className = error_password.className.replace('show', 'hide');

		if( !validate(name, AUTH_REG, error_name) || !validate(password, PASS_REG, error_password)) {
			return false
		}

		var body = 'command=' + encodeURIComponent('LOGIN') +
			'&login=' + name +
			'&password=' + password;
		
		post(body, success, error);

		function success(response) {
			goToUrl('/controller?command=SELECT_USER_PAGE');
		}

		function error(response) {
			if(response.currentTarget.status === 400) {
				goToUrl('/controller?command=GO_TO&page=ERROR_PAGE');
			} else {
				error_el.innerHTML = response.currentTarget.response;
				error_el.className = error_el.className.replace(' hide', '');
				error_el.className += ' show';
			}
		}
		return false;
	}

	function sendSignup(event) {
		var	elem = event.target,
			error_el = elem.querySelector('#error'),
			error_auth = elem.querySelector('#error-auth'),
			error_name = elem.querySelector('#error-name'),
			error_surname = elem.querySelector('#error-surname'),
			error_password = elem.querySelector('#error-password'),
			error_reppas = elem.querySelector('#error-reppas'),
			command = encodeURIComponent('REGISTRATION'),
			login = encodeURIComponent(elem.querySelector('#login').value),
			//name = encodeURIComponent(elem.querySelector('#name').value),
			name = document.getElementById('name').value,
			surname = document.getElementById('surname').value,
			password = encodeURIComponent(elem.querySelector('#password').value),
			repeatPassword = encodeURIComponent(elem.querySelector('#rep-password').value);

		error_auth.className = error_auth.className.replace('show', 'hide');
		error_name.className = error_name.className.replace('show', 'hide');
		error_surname.className = error_surname.className.replace('show', 'hide');
		error_password.className = error_password.className.replace('show', 'hide');
		error_reppas.className = error_reppas.className.replace('show', 'hide');

		if( !validate(login, AUTH_REG, error_auth) ||
			!validate(password, PASS_REG, error_password) ||
			!validate(name, NAME_REG, error_name) ||
			!validate(surname, SURNAME_REG, error_surname)) {
			return false;
		}

		if(password != repeatPassword) {
			error_reppas.className = error_reppas.className.replace('hide', 'show');
			return false;
		}

		var body = 'command=' + encodeURIComponent('REGISTRATION') +
			'&name=' + encodeURIComponent(elem.querySelector('#name').value) +
			'&surname=' + encodeURIComponent(elem.querySelector('#surname').value) +
			'&login=' + encodeURIComponent(elem.querySelector('#login').value) +
			'&password=' + encodeURIComponent(elem.querySelector('#password').value)+
		    '&repeatPassword=' + encodeURIComponent(elem.querySelector('#rep-password').value);

		post(body, success, error)

		function success (response) {
			goToUrl('/controller?command=SELECT_USER_PAGE');
		}

		function error (response) {
			if(response.currentTarget.status === 400) {
				goToUrl('/controller?command=GO_TO&page=ERROR_PAGE');
			} else {
				error_el.innerHTML = response.currentTarget.response;
				error_el.className = error_el.className.replace(' hide', '');
				error_el.className += ' show';
			}
		}
		return false;
	}
}