window.onload = function() {
	var overlay = document.getElementById('confirm-add-book-popup'),
		addBtn = document.getElementById('form_add_book'),
		addAuthorBtn = document.getElementById('addAuthor');
	addBtn.onsubmit = addNewBook;
	addAuthorBtn.onclick = addAuthor;
	overlay.onclick = close.bind(null, 'confirm-add-book-popup');

	function addAuthor() {
		var authors = document.getElementById('authorBlock'),
			addAuthorBtn = document.getElementById('addAuthor'),
			parent = authors.parentNode;
		authors = authors.cloneNode(true);
		var cancel = authors.lastElementChild;
		cancel.onclick = cancelAuthor;
		cancel.className = cancel.className.replace(' hide', ' show');
		parent.insertBefore(authors, addAuthorBtn);
	}

	function cancelAuthor(event) {
		event.target.parentNode.remove();
	}

	function addNewBook(){
		var optionGenre = document.getElementById('genre'),
			selOptionGenre = optionGenre.value,
			optionPublisher = document.getElementById('publisher'),
			selOptionPublisher = optionPublisher.options[optionPublisher.selectedIndex].value,
			title = document.getElementById('bookTitle').value,
			description = document.getElementById('description').value,
			editionYear = document.getElementById('editionYear').value,
			optionAuthor = document.getElementsByName('authors'),
			optionAuthor = Array.prototype.slice.call(optionAuthor),
			selOptionAuthor = optionAuthor.map( function(elem) {
				return elem.value;
			});

		var body = 'command=' + encodeURIComponent('ADD_BOOK') +
			'&bookTitle=' + encodeURIComponent(title) +
			'&authorId=' + encodeURIComponent(selOptionAuthor.join()) +
			'&genreId=' + encodeURIComponent(selOptionGenre) +
			'&publisherId=' + encodeURIComponent(selOptionPublisher) +
			'&editionYear=' + encodeURIComponent(editionYear) +
			'&description=' + encodeURIComponent(description);

		function error() {
			open('confirm-add-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}	
		function success() {
			open('confirm-add-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}

		post(body, success, error);
		return false;
	}
}