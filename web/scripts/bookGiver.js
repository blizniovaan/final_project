window.onload = function() {
	var confGiveBookOverlay = document.getElementById('confirm-give-book-popup'),
		form = document.getElementById('admin-give-book-form');
	
	confGiveBookOverlay.onclick = close.bind(null, 'confirm-give-book-popup');
	form.onsubmit = giveBook;
	
	function giveBook() {
		var books = document.getElementsByClassName('givedBook'),
			booksData = getData(books),
			place = document.getElementById('place').value,
			radio = document.getElementsByName('user'),
			checkedRadio = '';
		
		for (var i = 0; i < radio.length; i++) {
			if (radio[i].checked) {
				checkedRadio = radio[i].value;
				break;
			}
		}

		var body = 'command=' + encodeURIComponent('GIVE_BOOKS') +
			'&userId=' + encodeURIComponent(checkedRadio) +
			'&booksId=' + encodeURIComponent(booksData)+
			'&place='+ encodeURIComponent(place);
		
		function error() {
			open('confirm-give-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}

		function success() {
			open('confirm-give-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}

		post(body, success, error);
	
		return false;
	}
}