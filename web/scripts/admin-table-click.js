$(document).on('click', '#t_books>.rowlink', function() {
    var bookId = $(this).find('.bookId').text();
    var genre = $(this).find('#genre').text();

    var body = 'command=' + encodeURIComponent('GET_BOOK_INFO') +
        '&bookId=' + encodeURIComponent(bookId)+ '&genre='+ encodeURIComponent(genre);
    window.location.href = "/controller?" + body;
    return false;
});