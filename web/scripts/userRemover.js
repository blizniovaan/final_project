window.onload = function() {
	var confAddUserOverlay = document.getElementById('confirm-remove-user-popup'),
		form = document.getElementById('admin-user-form');
	confAddUserOverlay.onclick = close.bind(null, 'confirm-remove-user-popup');

	form.onsubmit = deleteUser;
	
	function deleteUser() {
		var checkboxes = document.getElementsByClassName('removedUser'),
			data = getData(checkboxes),
			body = 'command=' + encodeURIComponent('DELETE_USER') +
				'&usersId=' + encodeURIComponent(data);

		function error() {
			open('confirm-remove-user-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}

		function success() {
			open('confirm-remove-user-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}

		post(body, success, error);
		return false;
	}
}

