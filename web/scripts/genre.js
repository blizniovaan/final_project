window.onload = function() {
	var confGenreOverlay = document.getElementById('confirm-genre-popup'),
		addForm = document.getElementById('form_add_genre'),
		editForm = document.getElementById('form_edit_genre'),
		deleteForm = document.getElementById('form_delete_genre');
	confGenreOverlay.onclick = close.bind(null, 'confirm-genre-popup');
	addForm.onsubmit = addGenre;
	editForm.onsubmit = editGenre;
	deleteForm.onsubmit = deleteGenre;

	function addGenre(event) {
		var elem = event.target,
			body = 'command=' + encodeURIComponent('ADD_GENRE') +
			'&title=' + encodeURIComponent(elem.querySelector('#genreTitle').value);
		function error() {
				open('confirm-genre-popup');
				document.getElementById('confirmation-message').innerText = 'Error.Try again.';
		}
		function success() {
				open('confirm-genre-popup');
				document.getElementById('confirmation-message').innerText = 'The genre was successfully added';			
		}
		post(body, success, error);
		return false;
	}

	function editGenre(event) {
		var elem = event.target,
			genreOption = document.getElementById('genre'),
			selGenreOption = genreOption.value,
			body = 'command=' + encodeURIComponent('EDIT_GENRE') +
				'&genreId=' + encodeURIComponent(selGenreOption) +
				'&title='+encodeURIComponent(elem.querySelector('#newGenreTitle').value);
		function error() {
			open('confirm-genre-popup');
			document.getElementById('confirmation-message').innerText = 'Error.Try again.';			
		}
		function success() {
			open('confirm-genre-popup');
			document.getElementById('confirmation-message').innerText = 'The genre was successfully edited';			
		}		
		post(body, success, error);
		return false;
	}

	function deleteGenre() {
		var checkboxs = document.getElementsByClassName('removeGenre'),
			data = getData(checkboxs),
			body = 'command=' + encodeURIComponent('DELETE_GENRE') +
				'&genreId=' + encodeURIComponent(data);
		function error() {
			open('confirm-genre-popup');
			document.getElementById('confirmation-message').innerText = 'Error.This genre is still used.';
		}
		function success() {
			open('confirm-genre-popup');
			document.getElementById('confirmation-message').innerText = 'The genre was successfully removed';
		}		
		post(body, success, error);
		return false;
	}
}