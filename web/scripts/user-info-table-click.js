$(document).on('click', '#t_users>.rowlink', function() {
    var userId = $(this).find('.userId').text();

    var body = 'command=' + encodeURIComponent('GET_INFO_ABOUT_USER') +
        '&userId=' + encodeURIComponent(userId);
    window.location.href = "/controller?" + body;
    return false;
});