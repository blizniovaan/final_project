window.onload = function() {
    var confGenreOverlay = document.getElementById('confirm-publisher-popup'),
        addForm = document.getElementById('form_add_publisher'),
        editForm = document.getElementById('form_edit_publisher'),
        deleteForm = document.getElementById('form_delete_publisher');
    confGenreOverlay.onclick = close.bind(null, 'confirm-publisher-popup');
    addForm.onsubmit = addPublisher;
    editForm.onsubmit = editPublisher;
    deleteForm.onsubmit = deletePublisher;

    function addPublisher(event) {
        var elem = event.target,
            body = 'command=' + encodeURIComponent('ADD_PUBLISHER') +
                '&title=' + encodeURIComponent(elem.querySelector('#publisherTitle').value);
        function error() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }

    function editPublisher(event) {
        var elem = event.target,
            genreOption = document.getElementById('publisher'),
            selGenreOption = genreOption.value,
            body = 'command=' + encodeURIComponent('EDIT_PUBLISHER') +
                '&publisherId=' + encodeURIComponent(selGenreOption) +
                '&title='+encodeURIComponent(elem.querySelector('#newPublisherTitle').value);
        function error() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }

    function deletePublisher() {
        var checkboxs = document.getElementsByClassName('removePublisher'),
            data = getData(checkboxs),
            body = 'command=' + encodeURIComponent('DELETE_PUBLISHER') +
                '&publisherId=' + encodeURIComponent(data);
        function error() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-publisher-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }
}
