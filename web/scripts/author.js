/**
 * Created by HP on 15.03.2016.
 */
window.onload = function() {
    var confAuthorOverlay = document.getElementById('confirm-edit-author-popup'),
        addForm = document.getElementById('form_add_author'),
        editForm = document.getElementById('form_edit_author'),
        deleteForm = document.getElementById('form_delete_author');
    confAuthorOverlay.onclick = close.bind(null, 'confirm-edit-author-popup');
    addForm.onsubmit = addAuthor;
    editForm.onsubmit = editAuthor;
    deleteForm.onsubmit = deleteAuthor;

    function addAuthor(event) {
        var elem = event.target,
            body = 'command=' + encodeURIComponent('ADD_AUTHOR') +
                '&name=' + encodeURIComponent(elem.querySelector('#name').value);
        function error() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }

    function editAuthor(event) {
        var elem = event.target,
            authorOption = document.getElementById('authors'),
            selAuthorOption = authorOption.value,
            body = 'command=' + encodeURIComponent('EDIT_AUTHOR') +
                '&authorId=' + encodeURIComponent(selAuthorOption) +
                '&newName='+encodeURIComponent(elem.querySelector('#newName').value);
        function error() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }

    function deleteAuthor() {
        var checkboxs = document.getElementsByClassName('removeAuthor'),
            data = getData(checkboxs),
            body = 'command=' + encodeURIComponent('DELETE_AUTHOR') +
                '&authorId=' + encodeURIComponent(data);
        function error() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('hide', 'show');
            success.className = success.className.replace('show', 'hide');
        }
        function success() {
            open('confirm-edit-author-popup');
            var error = document.getElementById('confirmation-message-error'),
                success = document.getElementById('confirmation-message-success');
            error.className = error.className.replace('show', 'hide');
            success.className = success.className.replace('hide', 'show');
        }
        post(body, success, error);
        return false;
    }
}
