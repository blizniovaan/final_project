function stopProp(event) {
	event.stopImmediatePropagation();
}

function post (body, success, error) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/controller', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onloadend = function(response) {
		if (response.currentTarget.status === 200) {
			success(response);
		}
		else {
			error(response);
		}
	};
	xhr.send(body);
}

function goToUrl(url) {
	window.location.href = url;
}

function getData(item) {
	var length = item.length,
		data = '';
	for (var i = 0; i < length; i++) {
		if (item[i].type == 'checkbox' & item[i].checked) {
			data = data + item[i].value + ',';
		}
	}
	return data.slice(0, -1);
}

function open(type) {
	var signinPopup = document.getElementById(type);
	signinPopup.className = signinPopup.className.replace(' hide', '');
	signinPopup.className += ' show';
}

function close(type) {
	var signinPopup = document.getElementById(type);
	signinPopup.className = signinPopup.className.replace(' show', '');
	signinPopup.className += ' hide';
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});