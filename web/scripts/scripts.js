function loadScript(){
	var signin = document.getElementById('signin');
	signin.onclick = open.bind(null, 'signin-popup');

	var signinSubmit = document.getElementById('signin-form');
	signinSubmit.onsubmit = sendSignin.bind(null, 'signin-popup');

	var signinOverlay = document.getElementById('signin-popup');
	signinOverlay.onclick = function(event) {
		close('signin-popup');
	}

	var signinPopupContainer = document.getElementById('signin-popup-container');
	signinPopupContainer.onclick = stopProp;

	var signup = document.getElementById('signup');
	signup.onclick = open.bind(null, 'signup-popup');

	var signupSubmit = document.getElementById('signup-form');
	signupSubmit.onsubmit = sendSignup.bind(null, 'signup-popup');

	var signupOverlay = document.getElementById('signup-popup');
	signupOverlay.onclick = function(event) {
		close('signup-popup');
	}

	var signinPopupContainer = document.getElementById('signup-popup-container');
	signinPopupContainer.onclick = stopProp;

	function stopProp(event) {
		event.stopImmediatePropagation();
	}

	function sendSignin(type) {
		var elem = document.getElementById(type);
		var error = elem.querySelector('#error');
		var command = encodeURIComponent('LOGIN');
		var name = encodeURIComponent(elem.querySelector('#login').value);
		var password = encodeURIComponent(elem.querySelector('#password').value);
		var passRegEx = new RegExp('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$');
		var authRegEx = new RegExp('(([A-Za-z0-9]{6,18}))');

		if(!name.match(authRegEx) || name.match(authRegEx)[0] != name) {
			error.innerText = 'Invalid name. Name can consist of uppercase latin letters,\
            lowercase latin letters and numbers. And contain at least 6 sybols.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		if(!password.match(passRegEx) || password.match(passRegEx)[0] != password) {
			error.innerText = 'Invalid password. Password should contain at least one\
            uppercase latin letters, lowercase latin letters and number.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		var xhr = new XMLHttpRequest();

		var body = 'command=' + encodeURIComponent('LOGIN') +
		'&login=' + encodeURIComponent(elem.querySelector('#login').value) +
		'&password=' + encodeURIComponent(elem.querySelector('#password').value);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 401) {
				error.innerHTML = response.currentTarget.response;
				error.className = error.className.replace(' hide', '');
				error.className += " show";
			}
			else if (response.currentTarget.status === 200) {
				window.location.href = "/controller?command=SELECT_USER_PAGE";
			}
		}
		xhr.send(body);
		return false;
	}

	function sendSignup(type) {
		var elem = document.getElementById(type);
		var error = elem.querySelector("#error");
		var command = encodeURIComponent('REGISTRATION');
		var login = encodeURIComponent(elem.querySelector('#login').value);
		var name = encodeURIComponent(elem.querySelector('#name').value);
		var surname = encodeURIComponent(elem.querySelector('#surname').value);
		var password = encodeURIComponent(elem.querySelector('#password').value);
		var repeatPassword = encodeURIComponent(elem.querySelector('#rep-password').value);
		var passRegEx = new RegExp('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$');
		var authRegEx = new RegExp('[A-Za-z0-9_-]{6,18}');
		var nameRegEx = new RegExp('[A-Za-z-]{2,18}');
		var surnameRegEx = new RegExp('[A-Za-z-]{3,18}');

		if(!name.match(nameRegEx) || name.match(nameRegEx)[0] != name) {
			error.innerText = 'Invalid name. Name can consist of uppercase latin letters,\
  lowercase latin letters. And contain at least 2 sybols.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		if(!login.match(authRegEx) || login.match(authRegEx)[0] != login) {
			error.innerText = 'Invalid login. Name can consist of uppercase latin letters,\
  lowercase latin letters and numbers,underline and dash. And contain at least 6 sybols.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		if(!surname.match(surnameRegEx) || surname.match(surnameRegEx)[0] != surname) {
			error.innerText = 'Invalid surname. Name can consist of uppercase latin letters,\
  lowercase latin letters. And contain at least 6 sybols.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		if(!password.match(passRegEx) || password.match(passRegEx)[0] != password) {
			error.innerText = 'Invalid password. Password should contain at least one\
  uppercase latin letters, lowercase latin letters and number.';
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		if(password != repeatPassword) {
			error.innerText = "Passwords should be equals.";
			error.className = error.className.replace(' hide', '');
			error.className += ' show';
			return false;
		}

		var xhr = new XMLHttpRequest();

		var body = 'command=' + encodeURIComponent('REGISTRATION') +
			'&name=' + encodeURIComponent(elem.querySelector("#name").value) +
			'&surname=' + encodeURIComponent(elem.querySelector("#surname").value) +
			'&login=' + encodeURIComponent(elem.querySelector("#login").value) +
			'&password=' + encodeURIComponent(elem.querySelector("#password").value)+
		    '&repeatPassword=' + encodeURIComponent(elem.querySelector("#rep-password").value);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 401) {
				error.innerHTML = response.currentTarget.response;
				error.className = error.className.replace(' hide', '');
				error.className += " show";
			} else if (response.currentTarget.status === 200) {
				window.location.href = "/controller?command=SELECT_USER_PAGE";
			}
		}
		xhr.send(body);
		return false;
	}
}

function returnBook(){
	var popupOverlay = document.getElementById('confirm-return-book-popup');
	popupOverlay.onclick = function() {
		close('confirm-return-book-popup');
	}
	var form = document.querySelector("#card");
	form.onsubmit = returnBook;
	function returnBook(){
		var records = document.getElementsByClassName('returnRecord'),
			n = records.length,
			data =''
		for (var i = 0; i < n; i++){
			if(records[i].type == 'checkbox' & records[i].checked){
				data = data  + records[i].value + ',';
			}
		}

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('RETURN_BOOK') +
			'&recordsId=' + encodeURIComponent(data);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-return-book-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-return-book-popup");
				document.querySelector("#confirmation-message").innerText = "Книга была успешно возвращена!";
			}
		}
		xhr.send(body);
		return false;
	}
}

function takeBookHandler() {
	var signupOverlay = document.getElementById('confirmation-popup');
	signupOverlay.onclick = function() {
		close('confirmation-popup');
	}
	var form = document.querySelector("#take-book-form");
	form.onsubmit = takeBook;
	function takeBook(){
		var elem = document.getElementById("take-book-form");
		var option = document.getElementById("place");
		var selectedOption = option.options[option.selectedIndex].value;

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('TAKE_BOOK') +
			'&userId=' + encodeURIComponent(elem.querySelector("#userid").value) +
			'&bookId=' + encodeURIComponent(elem.querySelector("#bookid").value)+
			'&genre=' + encodeURIComponent(elem.querySelector("#genre").value)+
				'&place=' + selectedOption;

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirmation-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirmation-popup");
				document.querySelector("#confirmation-message").innerText = "The book is successfully added to your library card. Pleasant reading!";
			}
		}
		xhr.send(body);
		return false;
	}
}
function giveBook(){
	var confGiveBookOverlay = document.getElementById('confirm-give-book-popup');
	confGiveBookOverlay.onclick = function () {
		close('confirm-give-book-popup');
	}
	var form = document.querySelector("#admin-give-book-form");
	form.onsubmit = giveBook;
	function giveBook() {
		var books = document.getElementsByClassName('givedBook'),
			n = books.length,
			data = ''
		for (var i = 0; i < n; i++) {
			if (books[i].type == 'checkbox' & books[i].checked) {
				data = data + books[i].value + ',';
			}
		}
		var option = document.getElementById("place");
		var place = option.options[option.selectedIndex].value;

		var radio = document.getElementsByName("user");
		var checkedRadio = '';
		for (var i = 0; i < radio.length; i++) {
			if (radio[i].checked) {
				checkedRadio = radio[i].value;
				break;
			}
		}
			var xhr = new XMLHttpRequest();
			var body = 'command=' + encodeURIComponent('GIVE_BOOKS') +
				'&userId=' + encodeURIComponent(checkedRadio) +
				'&booksId=' + encodeURIComponent(data)+
				'&place='+ encodeURIComponent(place);

			xhr.open("POST", '/controller', true)
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
			xhr.onloadend = function (response) {
				if (response.currentTarget.status === 404) {
					open("confirm-give-book-popup");
					document.querySelector("#confirmation-message").innerText = "Error.Try again.";
				}
				else if (response.currentTarget.status === 200) {
					open("confirm-give-book-popup");
					document.querySelector("#confirmation-message").innerText = "Книги были успешно добавлены в карту пользователя!";
				}
			}
			xhr.send(body);
			return false;
		}
}

function addUser(){
	var confAddUserOverlay = document.getElementById('confirm-add-user-popup');
	confAddUserOverlay.onclick = function () {
		close('confirm-add-user-popup');
	}
	var form = document.querySelector("#form_add_user");
	form.onsubmit = add;
	function add() {
		var elem = document.getElementById("form_add_user");
		var name = elem.querySelector("#name").value;
		var surname = elem.querySelector("#surname").value;
		var login = elem.querySelector("#login").value;
		var password = elem.querySelector("#password").value;

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('REGISTRATION') +
			'&name=' + encodeURIComponent(name) +
			'&surname=' + encodeURIComponent(surname)+
			'&login='+ encodeURIComponent(login)+
		    '&password=' + encodeURIComponent(password);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function (response) {
			if (response.currentTarget.status === 404) {
				open("confirm-add-user-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-add-user-popup");
				document.querySelector("#confirmation-message").innerText = "Пользователь был успешно добавлен!";
			}
		}
		xhr.send(body);
		return false;
	}
}

function deleteUser(){
	var confAddUserOverlay = document.getElementById('confirm-add-user-popup');
	confAddUserOverlay.onclick = function () {
		close('confirm-add-user-popup');
	}
	var form = document.querySelector("#admin-user-form");
	form.onsubmit = add;
	function add() {
		var userId = document.getElementsByClassName('removedUser'),
			n = userId.length,
			data = ''
		for (var i = 0; i < n; i++) {
			if (userId[i].type == 'checkbox' & userId[i].checked) {
				data = data + userId[i].value + ',';
			}
		}
		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('DELETE_USER') +
			'&usersId=' + encodeURIComponent(data);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function (response) {
			if (response.currentTarget.status === 404) {
				open("confirm-add-user-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-add-user-popup");
				document.querySelector("#confirmation-message").innerText = "Успешно!";
			}
		}
		xhr.send(body);
		return false;
	}
}
function deleteBookHandler() {
	var confRemoveOverlay = document.getElementById('confirm-remove-book-popup');
	confRemoveOverlay.onclick = function() {
		close('confirm-remove-book-popup');
	}
	var deleteForm = document.querySelector("#admin-book-form");
	deleteForm.onsubmit = deleteBook;
	function deleteBook(){
		var checkboxs = document.getElementsByClassName('removeBook'),
			n = checkboxs.length,
			data =''
		for (var i= 0; i < n;i++)
			if(checkboxs[i].type == 'checkbox' & checkboxs[i].checked)
				data = data  + checkboxs[i].value + ',';

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('DELETE_BOOK') +
			'&bookId=' + encodeURIComponent(data);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-remove-book-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-remove-book-popup");
				document.querySelector("#confirmation-message").innerText = "The book was successfully removed";
			}
		}
		xhr.send(body);
		return false;
	}


}
function genreHandler() {
	var confGenreOverlay = document.getElementById('confirm-genre-popup');
	confGenreOverlay.onclick = function() {
		close('confirm-genre-popup');
	}
	var addForm = document.querySelector("#form_add_genre");
	addForm.onsubmit = addGenre;
	var editForm = document.querySelector("#form_edit_genre");
	editForm.onsubmit = editGenre;
	var deleteForm = document.querySelector("#form_delete_genre");
	deleteForm.onsubmit = deleteGenre;
	function addGenre(){
		var elem = document.getElementById('form_add_genre');

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('ADD_GENRE') +
			'&title=' + encodeURIComponent(elem.querySelector("#genreTitle").value);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "The genre was successfully added";
			}
		}
		xhr.send(body);
		return false;
	}
	function editGenre(){
		var elem = document.getElementById('form_edit_genre');
		var genreOption = document.getElementById("genre");
		var selGenreOption = genreOption.options[genreOption.selectedIndex].value;

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('EDIT_GENRE') +
			'&genreId=' + encodeURIComponent(selGenreOption)+
			'&title='+encodeURIComponent(elem.querySelector("#newGenreTitle").value);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "The genre was successfully edited";
			}
		}
		xhr.send(body);
		return false;
	}
	function deleteGenre(){
		var checkboxs = document.getElementsByClassName('removeGenre'),
			n = checkboxs.length,
			data =''
		for (var i= 0; i < n;i++)
			if(checkboxs[i].type == 'checkbox' & checkboxs[i].checked)
				data = data  + checkboxs[i].value + ',';

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('DELETE_GENRE') +
			'&genreId=' + encodeURIComponent(data);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "Error.This genre is still used.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-genre-popup");
				document.querySelector("#confirmation-message").innerText = "The genre was successfully removed";
			}
		}
		xhr.send(body);
		return false;
	}
}




function adminAddBookHandler() {
	var overlay = document.getElementById('confirm-add-book-popup'),
		addBtn = document.querySelector("#form_add_book"),
		addAuthorBtn = document.getElementById("addAuthor");
	addBtn.onsubmit = addNewBook;
	addAuthorBtn.onclick = addAuthor;
	overlay.onclick = function() {
		close('confirm-add-book-popup');
	}

	function addAuthor() {
		var authors = document.getElementById("authorBlock"),
			addAuthorBtn = document.getElementById("addAuthor"),
			parent = authors.parentNode;
		authors = authors.cloneNode(true);
		var cancel = authors.lastElementChild;
		cancel.onclick = cancelAuthor;
		cancel.className = cancel.className.replace(" hide", " show");
		parent.insertBefore(authors, addAuthorBtn);
	}
	function cancelAuthor(event) {
		event.target.parentNode.remove();
	}

	function addNewBook(){
		var optionAuthor = document.getElementsByName("authors");
		optionAuthor = Array.prototype.slice.call(optionAuthor);
		var selOptionAuthor = optionAuthor.map(function(elem) {
			return elem.value;
		});

		var optionGenre = document.getElementById("genre");
		var selOptionGenre = optionGenre.value;

		var optionPublisher = document.getElementById("publisher");
		var selOptionPublisher = optionPublisher.options[optionPublisher.selectedIndex].value;

		var title = document.getElementById("bookTitle").value;
		var description = document.getElementById("description").value;
		var editionYear = document.getElementById("editionYear").value;

		var xhr = new XMLHttpRequest();
		var body = 'command=' + encodeURIComponent('ADD_BOOK') +
			'&bookTitle=' + encodeURIComponent(title) +
			'&authorId=' + encodeURIComponent(selOptionAuthor.join()) +
			'&genreId=' + encodeURIComponent(selOptionGenre) +
			'&publisherId=' + encodeURIComponent(selOptionPublisher) +
			'&editionYear=' + encodeURIComponent(editionYear) +
			'&description=' + encodeURIComponent(description);

		xhr.open("POST", '/controller', true)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.onloadend = function(response) {
			if(response.currentTarget.status === 404) {
				open("confirm-add-book-popup");
				document.querySelector("#confirmation-message").innerText = "Error.Try again.";
			}
			else if (response.currentTarget.status === 200) {
				open("confirm-add-book-popup");
				document.querySelector("#confirmation-message").innerText = "The book successfully added";
			}
		}
		xhr.send(body);
		return false;
	}
}

function open(type) {
	var signinPopup = document.getElementById(type);
	signinPopup.className = signinPopup.className.replace(' hide', '');
	signinPopup.className += " show";
}

function close(type) {
	var signinPopup = document.getElementById(type);
	signinPopup.className = signinPopup.className.replace(' show', '');
	signinPopup.className += " hide";
}
