window.onload = function() {
	var confAddUserOverlay = document.getElementById('confirm-add-user-popup'),
		form = document.getElementById('form_add_user');
	confAddUserOverlay.onclick = close.bind(null, 'confirm-add-user-popup');
	form.onsubmit = add;

	function add(event) {
		var elem = event.target,
			name = elem.querySelector('#name').value,
			surname = elem.querySelector('#surname').value,
			login = elem.querySelector('#login').value,
			password = elem.querySelector('#password').value,
			body = 'command=' + encodeURIComponent('REGISTRATION') +
				'&name=' + encodeURIComponent(name) +
				'&surname=' + encodeURIComponent(surname) +
				'&login='+ encodeURIComponent(login) +
				'&password=' + encodeURIComponent(password);
		function error() {
			open('confirm-add-user-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}
		function success() {
			open('confirm-add-user-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}
		post(body, success, error);
		return false;
	}
}