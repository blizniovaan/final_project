window.onload = function() {
	var confRemoveOverlay = document.getElementById('confirm-remove-book-popup'),
		deleteForm = document.getElementById('admin-book-form');
	confRemoveOverlay.onclick = close.bind(null, 'confirm-remove-book-popup');
	deleteForm.onsubmit = deleteBook;
	
	function deleteBook(){
		var checkboxes = document.getElementsByClassName('removeBook'),
			data = getData(checkboxes),
			body = 'command=' + encodeURIComponent('DELETE_BOOK') +
				'&bookId=' + encodeURIComponent(data);

		function error() {
			open('confirm-remove-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}

		function success() {
			open('confirm-remove-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}

		post(body, success, error);
		return false;
	}
}
