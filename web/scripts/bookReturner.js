window.onload = function() {
	var popupOverlay = document.getElementById('confirm-return-book-popup'),
		form = document.getElementById('card');
	
	popupOverlay.onclick = close.bind(null, 'confirm-return-book-popup');
	form.onsubmit = returnBook;
	
	function returnBook(){
		var books = document.getElementsByClassName('returnBook'),
			booksData = getData(books),
			body = 'command=' + encodeURIComponent('RETURN_BOOK') +
				'&recordsId=' + encodeURIComponent(booksData);

		function error() {
			open('confirm-return-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('hide', 'show');
			success.className = success.className.replace('show', 'hide');
		}

		function success() {
			open('confirm-return-book-popup');
			var error = document.getElementById('confirmation-message-error'),
				success = document.getElementById('confirmation-message-success');
			error.className = error.className.replace('show', 'hide');
			success.className = success.className.replace('hide', 'show');
		}
		post(body, success, error);
		return false;
	}
}
