<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="title.genre" var="titleGenre" />

<fmt:message bundle="${lang}" key="genre.military_science" var="military_science" />
<fmt:message bundle="${lang}" key="genre.business_literature" var="business_literature" />
<fmt:message bundle="${lang}" key="genre.detective" var="detective" />
<fmt:message bundle="${lang}" key="genre.children" var="children" />
<fmt:message bundle="${lang}" key="genre.documentary" var="documentary" />
<fmt:message bundle="${lang}" key="genre.housekeeping" var="housekeeping" />
<fmt:message bundle="${lang}" key="genre.dramatic_art" var="dramatic_art" />
<fmt:message bundle="${lang}" key="genre.education" var="education" />
<fmt:message bundle="${lang}" key="genre.computers" var="computers" />
<fmt:message bundle="${lang}" key="genre.romance_novels" var="romance_novels" />
<fmt:message bundle="${lang}" key="genre.poetry" var="poetry" />
<fmt:message bundle="${lang}" key="genre.adventures" var="adventures" />
<fmt:message bundle="${lang}" key="genre.prose" var="prose" />
<fmt:message bundle="${lang}" key="genre.religion" var="religion" />
<fmt:message bundle="${lang}" key="genre.fantasy" var="fantasy" />
<fmt:message bundle="${lang}" key="genre.folklore" var="folklore" />
<fmt:message bundle="${lang}" key="genre.humour" var="humour" />

<div id="sidebar" class="sidebar-nav">
  <jsp:include page="searchForm.jsp"/>
  <p>${titleGenre}</p>
  <ul class="sidebar">
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=military_science">${military_science}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=business_literature">${business_literature}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=detective">${detective}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=children">${children}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=documentary">${documentary}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=housekeeping">${housekeeping}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=dramatic_art">${dramatic_art}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=computers">${computers}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=romance_novels">${romance_novels}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=education">${education}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=poetry">${poetry}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=adventures">${adventures}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=prose">${prose}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=religion">${religion}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=fantasy">${fantasy}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=folklore">${folklore}</a></li>
    <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=humour">${humour}</a></li>
  </ul>
</div>
