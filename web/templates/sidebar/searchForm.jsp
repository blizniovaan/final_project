<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="button.search" var="search" />
<c:choose>
  <c:when test="${sessionScope.role eq 'user'}">
    <form action="controller" method="get" class="form-search">
      <input type="text" class="input-medium search-query" name="enterWord">
      <input type="hidden" name="command" value="SELECT_AVAILABLE_BOOKS_BY_ENTER_WORD"/>
      <input type="hidden" name="page" value="RESULT_OF_SEARCH_FOR_USER_PAGE"/>
      <button type="submit" class="btn-mini">${search}</button>
    </form>
  </c:when>
  <c:when test="${sessionScope.role eq 'admin'}">
    <form action="controller" method="get" class="form-search">
      <input type="text" class="input-medium search-query" name="enterWord">
      <input type="hidden" name="command" value="SELECT_AVAILABLE_BOOKS_BY_ENTER_WORD"/>
      <input type="hidden" name="page" value="RESULT_OF_SEARCH_FOR_ADMIN_PAGE"/>
      <button type="submit" class="btn-mini">${search}</button>
    </form>
  </c:when>
</c:choose>
