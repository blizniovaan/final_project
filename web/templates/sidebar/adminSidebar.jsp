<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="title.genre" var="titleGenre" />
<fmt:message bundle="${lang}" key="element-list.add-user" var="add_user" />
<fmt:message bundle="${lang}" key="element-list.see-all-users" var="see_all_users" />
<fmt:message bundle="${lang}" key="element-list.delete-user" var="delete_user" />
<fmt:message bundle="${lang}" key="element-list.give-book" var="give_book" />

<fmt:message bundle="${lang}" key="element-list.see-all-books" var="see_all_books" />
<fmt:message bundle="${lang}" key="element-list.add-book" var="add_book" />
<fmt:message bundle="${lang}" key="element-list.delete-book" var="delete_book" />

<fmt:message bundle="${lang}" key="element-list.edit-genre" var="edit_genre" />
<fmt:message bundle="${lang}" key="element-list.edit-author" var="edit_author" />
<fmt:message bundle="${lang}" key="element-list.edit-publisher" var="edit_publisher" />

<div id="sidebar" class="admin-sidebar-nav">
  <jsp:include page="searchForm.jsp"/>
  <c:choose>
  <c:when test="${sessionScope.workObject == 'book'}">
    <ul class="sidebar">
      <li><a href="/controller?command=SELECT_ALL_BOOKS&page=ADMIN_BOOKS_PAGE">${see_all_books}</a></li>
      <li><a href="/controller?command=GO_TO_ADD_BOOK_PAGE">${add_book}</a></li>
      <li><a href="/controller?command=GO_TO_EDIT_PUBLISHER_PAGE">${edit_publisher}</a></li>
      <li><a href="/controller?command=GO_TO_EDIT_AUTHOR_PAGE">${edit_author}</a></li>
      <li><a href="/controller?command=GO_TO_DELETE_BOOK_PAGE">${delete_book}</a></li>
      <li><a href="/controller?command=GO_TO_GIVE_BOOK_PAGE">${give_book}</a></li>
    </ul>
  </c:when>
    <c:when test="${sessionScope.workObject == 'user'}">
      <ul class="sidebar">
        <li><a href="/controller?command=SELECT_ALL_USERS">${see_all_users}</a></li>
        <li><a href="/controller?command=GO_TO&page=ADD_USER_PAGE">${add_user}</a></li>
        <li><a href="/controller?command=GO_TO_DELETE_USER_PAGE">${delete_user}</a></li>
        <li><a href="/controller?command=GO_TO_GIVE_BOOK_PAGE">${give_book}</a></li>
      </ul>
    </c:when>
  </c:choose>
</div>
