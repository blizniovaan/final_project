<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>
<fmt:message key="header.yourlibrary-com" bundle="${lang}" var="your_library_com"/>

<header class="header">
  <h4 class="home">
    <a href="/controller?command=GO_TO_ADMIN_BOOKS_PAGE">${your_library_com}</a>
  </h4>
  <nav>
    <jsp:include page="adminNavigationButton.jsp"/>
  </nav>
</header>
