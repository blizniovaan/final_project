<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>
<fmt:message key="header.signin" bundle="${lang}" var="sighin"/>
<fmt:message key="header.signup" bundle="${lang}" var="signup"/>

<ul class="menu">
  <li>
    <a href="#" id="signin">${sighin}</a>
  </li>
  <li>
    <a href="#" id="signup">${signup}</a>
  </li>
</ul>
