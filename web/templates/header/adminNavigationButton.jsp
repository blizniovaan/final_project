<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message key="navigation.settings" bundle="${lang}"  var="settings" />
<fmt:message key="header.homepage" bundle="${lang}" var="header_homepage"/>
<fmt:message key="exit.button" bundle="${lang}" var="exit_button"/>
<fmt:message key="button.book-work" bundle="${lang}" var="book_work_button"/>
<fmt:message key="button.user-work" bundle="${lang}" var="user_work_button"/>

<ul class="menu">
  <li>
    <a href="/controller?command=GO_TO_ADMIN_BOOKS_PAGE">${header_homepage}</a>
  </li>
  <li>
    <a href="/controller?command=SELECT_ALL_USERS" id="user-work">${user_work_button}</a>
  </li>
  <li>
    <a href="/controller?command=SELECT_ALL_BOOKS&page=ADMIN_BOOKS_PAGE" id="book-work">${book_work_button}</a>
  </li>
  <li>
    <a href="/controller?command=GO_TO&page=SETTINGS" id="settings">${settings}</a>
  </li>
  <li>
    <a href="/controller?command=GO_TO&page=MAIN_PAGE" id="exit">${exit_button}</a>
  </li>
</ul>