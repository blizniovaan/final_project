<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="navigation.settings" var="settings" />
<fmt:message key="header.homepage" bundle="${lang}" var="header_homepage"/>

<ul class="menu">
  <li>
    <a href="/controller?command=GO_TO&page=USER_PAGE" id="home-page"> ${header_homepage}</a>
  </li>
  <li>
    <a href="/controller?command=GO_TO_CARD" id="user-card"><fmt:message key="card.button" bundle="${lang}"/> </a>
  </li>
  <li>
    <a href="/controller?command=GO_TO&page=SETTINGS" id="settings">${settings}</a>
  </li>
  <li>
    <a href="/controller?command=GO_TO&page=MAIN_PAGE" id="exit"><fmt:message key="exit.button" bundle="${lang}"/> </a>
  </li>
</ul>