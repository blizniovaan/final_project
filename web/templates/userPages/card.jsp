<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="label.title_book" var="label_title_book" />
<fmt:message bundle="${lang}" key="label.author" var="label_author" />
<fmt:message bundle="${lang}" key="message.empty-library-card" var="mess_empty_card" />
<fmt:message bundle="${lang}" key="title.digital_library" var="top_title" />
<fmt:message bundle="${lang}" key="label.taking-date" var="label_taking_date" />
<fmt:message bundle="${lang}" key="label.delivery-date" var="label_delivery_date" />
<fmt:message bundle="${lang}" key="label.return-book" var="label_return_book" />
<fmt:message bundle="${lang}" key="message.return-book" var="message_return_book" />
<fmt:message bundle="${lang}" key="button.apply" var="apply" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/bookReturner.js" type="text/javascript"></script>
  <jsp:include page="../modals/confirmation/confirmReturnBook.jsp"/>
</head>

<body>
<jsp:include page="../header/userHeader.jsp"/>
<div class="wrapper">

  <jsp:include page="../sidebar/userSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${top_title}</h2><br/>
      <h4>${message_return_book}</h4>
    </div>

    <form action="controller" method="post" id="card">
      <div class="info-table">
        <c:choose>
          <c:when test="${empty records}">
            <div class="warning">
              <h3>${mess_empty_card}</h3>
            </div>
          </c:when>
          <c:when test="${not empty records}">
            <div>
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>${label_title_book}</th>
                  <th>${label_author}</th>
                  <th>${label_taking_date}</th>
                  <th>${label_delivery_date}</th>
                  <th>${label_return_book}</th>
                </tr>
                </thead>
                <c:forEach items="${records}" var="bkinfo">
                  <tbody id="t_books">
                  <tr class="rowlink">
                    <td class="bookId">${bkinfo.book.bookId}</td>
                    <td class="title">${bkinfo.book.title}</td>
                    <td class="authors">
                      <c:forEach items="${bkinfo.book.authors}" var="author">
                        ${author.name}
                      </c:forEach>
                    </td>
                    <td class="takingDate">${bkinfo.takingDate}</td>

                    <c:choose>
                      <c:when test="${not empty bkinfo.deliveryDate}">
                        <td class="deliveryDate">${bkinfo.deliveryDate}</td>
                      </c:when>
                      <c:when test="${empty bkinfo.deliveryDate}">
                        <td class="deliveryDate">not</td>
                      </c:when>
                    </c:choose>

                    <c:choose>
                      <c:when test="${empty bkinfo.deliveryDate}">
                        <td>
                          <input type="checkbox" name="recordsId" id="recordsId" class="returnBook" value="${bkinfo.recordId}" required>
                        </td>
                      </c:when>
                      <c:when test="${not empty bkinfo.deliveryDate}">
                        <td class="disabledBook" name="disabledBook">
                          <inpute type="checkbox" disabled="disabled">x</inpute>
                        </td>
                      </c:when>
                    </c:choose>
                  </tr>
                  </tbody>
                </c:forEach>
              </table>
            </div>
            <input type="hidden" name="command" value="RETURN_BOOK">
            <input type="submit"  class="btn btn-primary active approve" value="${apply}">
          </c:when>
        </c:choose>
      </div>
    </form>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
