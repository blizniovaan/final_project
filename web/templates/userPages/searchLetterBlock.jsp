<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>
<fmt:message bundle="${lang}" key="letter.A" var="A" />
<fmt:message bundle="${lang}" key="letter.B" var="B" />
<fmt:message bundle="${lang}" key="letter.V" var="V" />
<fmt:message bundle="${lang}" key="letter.G" var="G" />
<fmt:message bundle="${lang}" key="letter.D" var="D" />
<fmt:message bundle="${lang}" key="letter.E" var="E" />
<fmt:message bundle="${lang}" key="letter.Zh" var="Zh" />
<fmt:message bundle="${lang}" key="letter.Z" var="Z" />
<fmt:message bundle="${lang}" key="letter.I" var="I" />
<fmt:message bundle="${lang}" key="letter.K" var="K" />
<fmt:message bundle="${lang}" key="letter.L" var="L" />
<fmt:message bundle="${lang}" key="letter.M" var="M" />
<fmt:message bundle="${lang}" key="letter.N" var="N" />
<fmt:message bundle="${lang}" key="letter.O" var="O" />
<fmt:message bundle="${lang}" key="letter.P" var="P" />
<fmt:message bundle="${lang}" key="letter.R" var="R" />
<fmt:message bundle="${lang}" key="letter.S" var="S" />
<fmt:message bundle="${lang}" key="letter.T" var="T" />
<fmt:message bundle="${lang}" key="letter.U" var="U" />
<fmt:message bundle="${lang}" key="letter.F" var="F" />
<fmt:message bundle="${lang}" key="letter.Kh" var="Kh" />
<fmt:message bundle="${lang}" key="letter.Ts" var="Ts" />
<fmt:message bundle="${lang}" key="letter.Ch" var="Ch" />
<fmt:message bundle="${lang}" key="letter.Sh" var="Sh" />
<fmt:message bundle="${lang}" key="letter.Shch" var="Shch" />
<fmt:message bundle="${lang}" key="letter.Y" var="Y" />
<fmt:message bundle="${lang}" key="letter.Ee" var="Ee" />
<fmt:message bundle="${lang}" key="letter.Iu" var="Iu" />
<fmt:message bundle="${lang}" key="letter.Ia" var="Ia" />


<fmt:message bundle="${lang}" key="genre.${genre}" var="currentGenre" />
<fmt:message bundle="${lang}" key="label.by-book-genre" var="by_book_genre" />

<ul class="sort">
  <li><span>${by_book_genre} ${currentGenre}:</span></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=1&genre=${genre}">${A}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=2&genre=${genre}">${B}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=3&genre=${genre}">${V}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=4&genre=${genre}">${G}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=5&genre=${genre}">${D}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=6&genre=${genre}">${E}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=7&genre=${genre}">${Zh}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=8&genre=${genre}">${Z}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=9&genre=${genre}">${I}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=10&genre=${genre}">${K}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=11&genre=${genre}">${L}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=12&genre=${genre}">${M}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=13&genre=${genre}">${N}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=14&genre=${genre}">${O}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=15&genre=${genre}">${P}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=16&genre=${genre}">${R}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=17&genre=${genre}">${S}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=18&genre=${genre}">${T}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=19&genre=${genre}">${U}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=20&genre=${genre}">${F}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=21&genre=${genre}">${Kh}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=22&genre=${genre}">${Ts}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=23&genre=${genre}">${Ch}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=24&genre=${genre}">${Sh}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=25&genre=${genre}">${Shch}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=26&genre=${genre}">${Y}</a></li>
  <li><a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=27&genre=${genre}">${Ia}</a></li>
</ul>

