<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="language.button_en" var="en_button" />
<fmt:message bundle="${lang}" key="language.button_ru" var="ru_button" />
<fmt:message bundle="${lang}" key="message.notBookInfo" var="mess_no_book_info" />
<fmt:message bundle="${lang}" key="message.no_available_books" var="mess_no_available_books" />
<fmt:message bundle="${lang}" key="library_card" var="library_card" />
<fmt:message bundle="${lang}" key="reading_room" var="reading_room" />
<fmt:message bundle="${lang}" key="button.get_book" var="get_book" />
<fmt:message bundle="${lang}" key="label.book_place" var="label_book_place" />
<fmt:message bundle="${lang}" key="label.title_book" var="label_title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="label_status" />
<fmt:message bundle="${lang}" key="label.description" var="label_description" />
<fmt:message bundle="${lang}" key="label.publisher" var="label_publisher" />
<fmt:message bundle="${lang}" key="title.genre" var="genre" />
<fmt:message bundle="${lang}" key="label.edition-year" var="label_edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="label_author" />
<fmt:message bundle="${lang}" key="title.about-book" var="about_book" />
<html>
<head>
    <title>YourLibrary.com</title>
    <jsp:include page="../resources.jsp"/>
    <script src="scripts/bookTaker.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="../header/userHeader.jsp"/>
<div class="wrapper">
    <jsp:include page="../sidebar/userSidebar.jsp"/>
    <div class="content">
        <div class="top-main-content">
            <h2>${about_book}</h2>
        </div>
<c:choose>
    <c:when test="${not empty book}">
        <div class="book_info">
            <p><label>${label_title_book}:</label> ${book.title}</p>
            <p><label>${label_status}:</label> ${book.status.status}</p>
            <p><label>ID:</label> ${book.bookId}</p>
            <p><label>${genre}:</label> ${book.genre.title}</p>
            <p><label>${label_author}:</label>
            <c:forEach items="${book.authors}" var="author">
                    ${author.name}
            </c:forEach>
            </p>
            <p><label>${label_edition_year}:</label> ${book.editionYear}</p>
            <p><label> ${label_publisher}:</label> ${book.publisher.title}</p>
            <p><label>${label_description}:</label>${book.description}</p>
        </div>
        <div class="buffer">
            <form action="controller" method="POST" id="take-book-form">
                <div class="order">
                    <c:choose>
                        <c:when test="${book.status.status == 'доступна'}">
                            <label for="place">${label_book_place}: </label>
                            <select class="select" id = "place">
                                <option id="card-place" value="READING_ROOM">${library_card}</option>
                                <option id="reading-room-place" value="LIBRARY_CARD">${reading_room}</option>
                            </select>
                            <input type="submit" class="btn btn-primary btn-sm" value="${get_book}"/>
                            <input type="hidden" id="userid" value="${sessionScope.userId}"/>
                            <input type="hidden" id="bookid" value="${book.bookId}"/>
                            <input type="hidden" id="genre" value="${genre}"/>
                            <input type="hidden" name="command" value="TAKE_BOOK"/>
                        </c:when>
                        <c:when test="${book.status.status == 'недоступна'}">
                            <label>${mess_no_available_books}</label>
                        </c:when>
                    </c:choose>
                </div>
            </form>
        </div>
    </c:when>
    <c:when test="${empty book}">
        <p>${mess_no_book_info}</p>
    </c:when>
   </c:choose>
    </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<jsp:include page="../modals/confirmation/confirmation.jsp"/>
</body>
</html>
