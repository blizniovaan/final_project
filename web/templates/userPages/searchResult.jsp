<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="out"%>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="genre.${sessionScope.genre}" var="currentGenre" />
<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="status" />
<fmt:message bundle="${lang}" key="label.description" var="description" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="label.edition-year" var="edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="message.no-book" var="warning" />
<fmt:message bundle="${lang}" key="text.there_are_available_books" var="there_are_available_books" />
<fmt:message bundle="${lang}" key="text.check_all_list" var="check_all_list" />
<fmt:message bundle="${lang}" key="text.return-to-available" var="return_to_available" />
<fmt:message bundle="${lang}" key="text.see-all" var="see_all" />
<fmt:message bundle="${lang}" key="text.back" var="back" />
<fmt:message bundle="${lang}" key="text.result-of-search" var="result_of_search" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
</head>

<body>
<jsp:include page="../header/userHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/userSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>Digital library YourLibrary.com</h2>
    </div>
    <div class = "search-letter-block">
      <out:authorsLetterList/>
      <out:booksLetterList/>
    </div>
    <div class="title">
      <h3>${result_of_search}</h3>
    </div>
    <div class = "search-letter-block">
      <c:if test="${searchParameter == 'letter'}">
        <p>${there_are_available_books}<br/>
            ${check_all_list}
          <a href="/controller?command=SELECT_ALL_BOOKS_BY_LETTER&letterFrom=${letterFrom}&page=RESULT_OF_SEARCH_FOR_USER_PAGE">${see_all}</a>
            ${return_to_available}
          <a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_LETTER&letterFrom=${letterFrom}&page=RESULT_OF_SEARCH_FOR_USER_PAGE" >${back}</a><br/></p>

      </c:if>
      <c:if test="${searchParameter == 'author'}">
        <p>${there_are_available_books}.<br/>
            ${check_all_list}
          <a href="/controller?command=SELECT_ALL_BOOKS_BY_AUTHOR&letterFrom=${letterFrom}&page=RESULT_OF_SEARCH_FOR_USER_PAGE">${see_all}</a>
            ${return_to_available}
          <a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=${letterFrom}&page=RESULT_OF_SEARCH_FOR_USER_PAGE" >${back}</a><br/></p>
      </c:if>
      <c:if test="${searchParameter == 'enterWord'}">
        <p>${there_are_available_books}.<br/>
            ${check_all_list}
          <a href="/controller?command=SELECT_ALL_BOOKS_BY_ENTER_WORD&enterWord=${enterWord}&page=RESULT_OF_SEARCH_FOR_USER_PAGE">${see_all}</a>
            ${return_to_available}
          <a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_ENTER_WORD&enterWord=${enterWord}&page=RESULT_OF_SEARCH_FOR_USER_PAGE" >${back}</a><br/>
        </p>
      </c:if>
    </div>
    <div class="info-table">
      <c:choose>
        <c:when test="${empty partBooks}">
          <div class="warning">
            <h3>${warning}</h3>
          </div>
        </c:when>
        <c:when test="${not empty partBooks}">
          <div>
            <table class="table table-striped table-hover table-condensed table-bordered">
              <thead>
              <tr class="fixed">
                <th>ID</th>
                <th>${title_book}</th>
                <th>${author}</th>
                <th>${edition_year}</th>
                <th>${publisher}</th>
              </tr>
              </thead>
              <c:forEach items="${partBooks}" var="bkinfo">
                <tbody id="t_books">
                <tr class="rowlink">
                  <td class="bookId">${bkinfo.bookId}</td>
                  <td class="title">${bkinfo.title}</td>
                    <td class="authors">
                      <c:forEach items="${bkinfo.authors}" var="author">
                        ${author.name}<br/>
                      </c:forEach>
                    </td>
                  <td class="edition-year">${bkinfo.editionYear}</td>
                  <td class="publisher">${bkinfo.publisher.title}</td>
                  <td class="invisible" id="genre">${genre}</td>
                  <td class="invisible" id="page">BOOK_PAGE</td>
                </tr>
                </tbody>
              </c:forEach>
            </table>
          </div>
        </c:when>
      </c:choose>
    </div>
  </div>
</div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<script type="text/javascript" src="scripts/table-click.js"></script>
</body>
</html>
