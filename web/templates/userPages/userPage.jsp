<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="out"%>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="title.digital_library" var="digital_library" />
<fmt:message bundle="${lang}" key="text.february" var="february" />
<fmt:message bundle="${lang}" key="text.read-more" var="read_more" />
<fmt:message bundle="${lang}" key="title.popular" var="popular_books" />

<html>
<head>
    <title>YourLibrary.com</title>
    <jsp:include page="../resources.jsp"/>
</head>

<body>
    <jsp:include page="../header/userHeader.jsp"/>
    <div class="wrapper">
        <jsp:include page="../sidebar/userSidebar.jsp"/>
        <div class="content">
            <div class="top-main-content">
                <h2>${digital_library}</h2>
            </div>
            <div class = "search-letter-block">
                <out:authorsLetterList/>
                <out:booksLetterList/>
            </div>
            <h3 class="topRecord">${popular_books}:</h3>
            <div class="popular-block">
                <form action="controller" method="post">
                    <div class="popular-item">
                        <p>
                            <img class="top-image" src="../../images/modman.png">
                            Мало кто знает, что бабушка актрисы Лив Тайлер Дороти Джонсон — признанный эксперт
                            по этикету, у которой за плечами годы работы в Вашингтонской школе протокола,
                            пять изданных книг...
                            <input type="hidden" name="command" value="SELECT_BOOK">
                            <input type="hidden" name="page" value="USER_PAGE">
                            <input type="hidden" name="genre" value="education">
                            <input type="hidden" name="bookId" value="103">
                            <button type="submit" class="btn btn-link">${read_more}...</button>
                        </p>
                        <span class="date"> 28 ${february}, 2016</span>
                    </div>
                 </form>

                <form action="controller" method="post">
                    <div class="popular-item">
                        <p>
                            <img class="top-image" src="../../images/princippreemstvenosti.png">Два с половиной года американка
                            Жан Ледлофф провела в племенах южноамериканских индейцев и своими глазами увидела ответы на важные
                            вопросы.Почему в воспитании детей стоит...
                            <input type="hidden" name="command" value="SELECT_BOOK">
                            <input type="hidden" name="page" value="USER_PAGE">
                            <input type="hidden" name="genre" value="education">
                            <input type="hidden" name="bookId" value="104">
                            <button type="submit" class="btn btn-link">${read_more}...</button>
                        </p>
                        <span class="date"> 17 ${february}, 2016</span>
                    </div>
                </form>
                <form action = "controller" method = "post">
                    <div class="popular-item">
                        <p><img class="top-image" src="../../images/zacon.png">
                            Практичная, добрая и мудрая книга. С этой книгой нужно дружить,
                            вновь и вновь возвращаться к ней, используя описанные принципы в жизни.Откройте для себя самые интересные...
                            <input type="hidden" name="command" value="SELECT_BOOK">
                            <input type="hidden" name="page" value="USER_PAGE">
                            <input type="hidden" name="genre" value="education">
                            <input type="hidden" name="bookId" value="105">
                            <button type="submit" class="btn btn-link">${read_more}...</button>
                        </p>
                        <span class="date"> 19 ${february}, 2016</span>
                    </div>
                </form>
                <form action="controller" method="post">
                    <div class="popular-item last">
                        <p>
                            <img class="top-image" src="../../images/crizis.png" > Финансовый или личный кризис — тот период в жизни,
                            который можно использовать в свою пользу. Для этого нужно взять на себя смелость
                            отбросить привычное и открыться новому и...
                            <input type="hidden" name="command" value="SELECT_BOOK">
                            <input type="hidden" name="page" value="USER_PAGE">
                            <input type="hidden" name="genre" value="education">
                            <input type="hidden" name="bookId" value="106">
                            <button type="submit" class="btn btn-link">${read_more}...</button>
                        </p>
                        <span class="date"> 15 ${february}, 2016</span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <jsp:include page="../footer/footer.jsp"/>
</body>
</html>
