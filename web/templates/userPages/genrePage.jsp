<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="genre.${genre}" var="currentGenre" />
<fmt:message bundle="${lang}" key="title.digital_library" var="top_title" />
<fmt:message bundle="${lang}" key="message.no-book" var="message_no_book" />
<fmt:message bundle="${lang}" key="label.by-book-genre" var="by_book_genre" />
<fmt:message bundle="${lang}" key="text.there_are_available_books" var="there_are_available_books" />
<fmt:message bundle="${lang}" key="text.check_all_list" var="check_all_list" />
<fmt:message bundle="${lang}" key="text.return-to-available" var="return_to_available" />
<fmt:message bundle="${lang}" key="text.see-all" var="see_all" />
<fmt:message bundle="${lang}" key="text.back" var="back" />

<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="status" />
<fmt:message bundle="${lang}" key="label.description" var="description" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="label.edition-year" var="edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="author" />

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
</head>

<body>
<jsp:include page="../header/userHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/userSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${top_title}</h2>
    </div>
    <div class = "search-letter-block">
    </div>
    <div class="title">
      <h3>${currentGenre}</h3>
    </div>
    <div class = "search-letter-block">
      <jsp:include page="searchLetterBlock.jsp"/>
      <p>${there_are_available_books}<br/>
        ${check_all_list}
        <a href="/controller?command=SELECT_ALL_BOOKS_BY_LETTERS_AND_GENRE&page=GENRE_PAGE&letterFrom=${letterFrom}&genre=${genre}">${see_all}</a>
        ${return_to_available}
        <a href="/controller?command=GO_TO_GENRE_PAGE&letterFrom=${letterFrom}&genre=${genre}">${back}</a><br/></p>
    </div>

    <div class="info-table">
      <c:choose>
        <c:when test="${empty partBooks}">
          <div class="warning">
            <h3>${message_no_book}</h3>
          </div>
        </c:when>
        <c:when test="${not empty partBooks}">
          <div>
            <table class="table table-striped table-hover table-condensed table-bordered">
              <thead>
              <tr>
                <th>ID</th>
                <th>${title_book}</th>
                <th>${author}</th>
                <th>${edition_year}</th>
                <th>${publisher}</th>
              </tr>
              </thead>
              <c:forEach items="${partBooks}" var="bkinfo">
                <tbody id="t_books">
                <tr class="rowlink" data-toggle="tooltip" data-placement="right" title="Click me!">
                  <td class="bookId">${bkinfo.bookId}</td>
                  <td class="title">${bkinfo.title}</td>
                  <td class="authors">
                    <c:forEach items="${bkinfo.authors}" var="author">
                      <p>${author.name}</p>
                    </c:forEach>
                  </td>
                  <td class="edition-year">${bkinfo.editionYear}</td>
                  <td class="publisher">${bkinfo.publisher.title}</td>
                  <td class="invisible" id="genre">${genre}</td>
                  <td class="invisible" id="page">BOOK_PAGE</td>
                </tr>
                </tbody>
              </c:forEach>
            </table>
          </div>
        </c:when>
      </c:choose>
    </div>
  </div>
</div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<script type="text/javascript" src="scripts/table-click.js"></script>
</body>
</html>
