<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="navigation.settings" var="settings" />
<fmt:message bundle="${lang}" key="text.website-language" var="website_language" />
<fmt:message bundle="${lang}" key="text.current-settings" var="current_settings" />
<fmt:message bundle="${lang}" key="option.choose-element" var="choose_element" />
<fmt:message bundle="${lang}" key="language.button_ru" var="ru_button" />
<fmt:message bundle="${lang}" key="language.button_en" var="en_button" />
<fmt:message bundle="${lang}" key="button.apply" var="apply" />

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
</head>

<body>
<c:choose>
  <c:when test="${sessionScope.role == 'admin'}">
    <jsp:include page="../header/adminHeader.jsp"/>
  </c:when>
  <c:when test="${sessionScope.role == 'user'}">
    <jsp:include page="../header/userHeader.jsp"/>
  </c:when>
</c:choose>
<div class="wrapper">
  <div class="container">

    <div class="content-welcome-page" id="content-welcome-page">
      <div>
        <p>${settings}</p>
        <h4>${current_settings}:</h4><br/><br/>
      </div>
      <form action="controller" method="post">
        <label>${website_language}:      </label>
        <select name = "language" id = "language" >
          <option value="" disabled selected>${choose_element}</option>
          <option value = "ru">${ru_button}</option>
          <option value = "en">${en_button}</option>
        </select><br/><br/>
        <input type="submit" class="btn btn-primary active" value="${apply}"/>
        <input type="hidden" name="page" value="SETTINGS"/>
        <input type="hidden" name="command" value="LOCALIZATION"/>
      </form>
    </div>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>

</body>
</html>
