<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message key="signup.name" bundle="${lang}" var="name"/>
<fmt:message key="signup.surname" bundle="${lang}" var="surname"/>
<fmt:message key="signup.login" bundle="${lang}" var="login"/>
<fmt:message key="signup.password" bundle="${lang}" var="password"/>
<fmt:message key="signup.reppassword" bundle="${lang}" var="reppassword"/>
<fmt:message key="signup.button" bundle="${lang}" var="button"/>
<fmt:message key="message.login-error" bundle="${lang}" var="login_err"/>
<fmt:message key="message.password-error" bundle="${lang}" var="password_err"/>
<fmt:message key="message.reppass-error" bundle="${lang}" var="reppass_err"/>
<fmt:message key="message.name-error" bundle="${lang}" var="name_err"/>
<fmt:message key="message.surname-error" bundle="${lang}" var="surname_err"/>

<div class="popup-overlay hide" id="signup-popup">
  <div class="popup" id="signup-popup-container">
    <form action="controller" method="post" id="signup-form">
      <input type="hidden"  name="command" value="REGISTRATION" />
      <label for="name">${name}</label>
      <input type="text" id="name" name="name" required/>
      <label for="surname">${surname}</label>
      <input type="text" id="surname" name="surname" required/>
      <label for="login">${login}</label>
      <input type="text" id="login" name="login" required/>
      <label for="password">${password}</label>
      <input type="password" id="password" name="password" required/>
      <label for="password">${reppassword}</label>
      <input type="password" id="rep-password" name="rep-password" required/>
      <button type="submit" class="submit" id="signup-submit">${button}</button>
      <p class="ok hide" id="ok"></p>
      <p class="error hide" id="error-auth">${login_err}</p>
      <p class="error hide" id="error-name">${name_err}</p>
      <p class="error hide" id="error-surname">${surname_err}</p>
      <p class="error hide" id="error-password">${password_err}</p>
      <p class="error hide" id="error-reppas">${reppass_err}</p>
      <p class="error hide" id="error"></p>
    </form>
  </div>
</div>
