<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="letter.A" var="A" />
<fmt:message bundle="${lang}" key="letter.B" var="B" />
<fmt:message bundle="${lang}" key="letter.V" var="V" />
<fmt:message bundle="${lang}" key="letter.G" var="G" />
<fmt:message bundle="${lang}" key="letter.D" var="D" />
<fmt:message bundle="${lang}" key="letter.E" var="E" />
<fmt:message bundle="${lang}" key="letter.Zh" var="Zh" />
<fmt:message bundle="${lang}" key="letter.Z" var="Z" />
<fmt:message bundle="${lang}" key="letter.I" var="I" />
<fmt:message bundle="${lang}" key="letter.K" var="K" />
<fmt:message bundle="${lang}" key="letter.L" var="L" />
<fmt:message bundle="${lang}" key="letter.M" var="M" />
<fmt:message bundle="${lang}" key="letter.N" var="N" />
<fmt:message bundle="${lang}" key="letter.O" var="O" />
<fmt:message bundle="${lang}" key="letter.P" var="P" />
<fmt:message bundle="${lang}" key="letter.R" var="R" />
<fmt:message bundle="${lang}" key="letter.S" var="S" />
<fmt:message bundle="${lang}" key="letter.T" var="T" />
<fmt:message bundle="${lang}" key="letter.U" var="U" />
<fmt:message bundle="${lang}" key="letter.F" var="F" />
<fmt:message bundle="${lang}" key="letter.Kh" var="Kh" />
<fmt:message bundle="${lang}" key="letter.Ts" var="Ts" />
<fmt:message bundle="${lang}" key="letter.Ch" var="Ch" />
<fmt:message bundle="${lang}" key="letter.Sh" var="Sh" />
<fmt:message bundle="${lang}" key="letter.Shch" var="Shch" />
<fmt:message bundle="${lang}" key="letter.Y" var="Y" />
<fmt:message bundle="${lang}" key="letter.Ee" var="Ee" />
<fmt:message bundle="${lang}" key="letter.Iu" var="Iu" />
<fmt:message bundle="${lang}" key="letter.Ia" var="Ia" />

<fmt:message bundle="${lang}" key="title.by_author" var="by_author" />

<ul class="sort">
  <li><span>By surname</span></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=1&letterTo=2">${A}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=2&letterTo=3">${B}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=3&letterTo=4">${V}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=4&letterTo=5">${G}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=5&letterTo=6">${D}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=6&letterTo=7">${E}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=7&letterTo=8">${Zh}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=8&letterTo=9">${Z}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=9&letterTo=10">${I}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=10&letterTo=11">${K}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=11&letterTo=12">${L}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=12&letterTo=13">${M}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=13&letterTo=14">${N}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=14&letterTo=15">${O}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=15&letterTo=16">${P}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=16&letterTo=17">${R}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=17&letterTo=18">${S}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=18&letterTo=19">${T}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=19&letterTo=20">${U}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=20&letterTo=21">${F}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=21&letterTo=22">${Kh}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=22&letterTo=23">${Ts}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=23&letterTo=24">${Ch}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=24&letterTo=25">${Sh}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=25&letterTo=26">${Shch}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=26&letterTo=27">${Y}-${Ee}-${Iu}</a></li>
  <li><a href="/controller?command=SELECT_AVAILABLE_BOOKS_BY_AUTHOR&letterFrom=27&letterTo=1">${Ia}</a></li>
</ul>
