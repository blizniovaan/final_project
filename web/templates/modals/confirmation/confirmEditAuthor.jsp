<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="button.thanks" var="ok_thanks" />
<fmt:message bundle="${lang}" key="message.successfully" var="successfully" />
<fmt:message bundle="${lang}" key="message.error" var="error" />

<div class="popup-overlay-confirmation hide" id="confirm-edit-author-popup">
  <div class="popup-confirmation" id="confirm-edit-author-popup-container">
    <div id="confirmation-message-error" class="hide"><h4 class="confirm-message-error">${error}</h4> </div>
    <div id="confirmation-message-success" class="hide"><h4 class="confirm-message-success">${successfully}</h4> </div>
    <a class="button" href="/controller?command=GO_TO_EDIT_AUTHOR_PAGE">${ok_thanks}</a>
  </div>
</div>

