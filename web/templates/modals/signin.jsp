<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>
<fmt:message key="signin.login" bundle="${lang}" var="login"/>
<fmt:message key="signin.password" bundle="${lang}" var="password"/>
<fmt:message key="signin.button" bundle="${lang}" var="button"/>
<fmt:message key="message.login-error" bundle="${lang}" var="login_err"/>
<fmt:message key="message.password-error" bundle="${lang}" var="password_err"/>
<fmt:message key="signin.button" bundle="${lang}" var="button"/>

<div class="popup-overlay hide" id="signin-popup">
  <div class="popup" id="signin-popup-container">
    <form action="controller" method="POST" id="signin-form">
      <input type="hidden"  name="command" value="LOGIN" />
      <label for="login">${login}</label>
      <input type="text" id="login" name="login" required/>
      <label for="password">${password} </label>
      <input type="password" id="password" name="password" required/>
      <button type="submit" class="submit" id="signin-submit">${button}</button>
      <p class="ok hide" id="ok"></p>
      <p class="error hide" id="error"></p>
      <p class="error hide" id="error-name">${login_err}</p>
      <p class="error hide" id="error-password">${password_err}</p>
    </form>
  </div>
</div>
