<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="language.button_en" var="en_button" />
<fmt:message bundle="${lang}" key="language.button_ru" var="ru_button" />

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/home.js" type="text/javascript"></script>
</head>
<body>
<div class="wrapper">
  <jsp:include page="../header/welcomeHeader.jsp"/>
  <jsp:include page="../modals/signin.jsp"/>
  <jsp:include page="../modals/signup.jsp"/>
  <div class="container">
    <div class="content-welcome-page" id="content-welcome-page">
      <div>
        <p>Oh God! </p>
        <h2>Время Вашей сессии истекло.<br>Для дальнейшей работы с сайтом вам необходимо авторизоваться повторно.</h2>
      </div>
    </div>
  </div>
</div>

  <jsp:include page="../footer/footer.jsp"/>
</body>
</html>



