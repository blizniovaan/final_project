<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="text.oh-got" var="oh_got" />
<fmt:message bundle="${lang}" key="text.error" var="error" />

<html>
<head>
    <title>YourLibrary.com</title>
    <script src="scripts/home.js" type="text/javascript"></script>
    <jsp:include page="../resources.jsp"/>
</head>
<body>
    <c:if test="${sessionScope.role eq 'user'}">
        <jsp:include page="../header/userHeader.jsp"/>
    </c:if>
    <c:if test="${sessionScope.role eq null}">
        <jsp:include page="../header/welcomeHeader.jsp"/>
        <jsp:include page="../modals/signin.jsp"/>
        <jsp:include page="../modals/signup.jsp"/>
    </c:if>
    <c:if test="${sessionScope.role eq 'admin'}">
        <jsp:include page="../header/adminHeader.jsp"/>
    </c:if>
    <div class="wrapper">
        <div class="container">
            <div class="content-welcome-page" id="content-welcome-page">
                <div>
                    <p>${oh_got}</p>
                    <h3>${error}</h3>
                    <h4>404</h4>
                </div>
            </div>
        </div>
    </div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
