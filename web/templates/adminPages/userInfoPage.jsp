<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="title.digital_library" var="digital_library" />
<fmt:message bundle="${lang}" key="label.name" var="name" />
<fmt:message bundle="${lang}" key="label.surname" var="surname" />
<fmt:message bundle="${lang}" key="label.login" var="login" />

<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="message.empty-library-card" var="mess_empty_card" />
<fmt:message bundle="${lang}" key="label.taking-date" var="taking_date" />
<fmt:message bundle="${lang}" key="label.delivery-date" var="delivery_date" />
<fmt:message bundle="${lang}" key="label.book_place" var="book_place" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
</head>
<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${digital_library}</h2>
    </div>
      <p><label>ID:</label> ${user.userId}</p>
      <p><label>${name}:</label> ${user.name}</p>
      <p><label>${surname}:</label> ${user.surname}</p>
      <p><label>${login}:</label> ${user.login}</p>
      <c:choose>
        <c:when test="${empty records}">
          <div class="warning">
            <h3>${mess_empty_card}</h3>
          </div>
        </c:when>
        <c:when test="${not empty records}">
            <div class="tableContainer">
              <form action="controller" method="post" id="admin-user-form">
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>${title_book}</th>
                  <th>${author}</th>
                  <th>${taking_date}</th>
                  <th>${delivery_date}</th>
                  <th>${book_place}</th>
                </tr>
                </thead>
                <c:forEach items="${records}" var="bkinfo">
                  <tbody id="t_books">
                  <tr class="rowlink">
                    <td class="bookId">${bkinfo.book.bookId}</td>
                    <td class="title">${bkinfo.book.title}</td>
                    <td class="authors">
                      <c:forEach items="${bkinfo.book.authors}" var="author">
                        ${author.name}
                      </c:forEach>
                    </td>
                    <td class="takingDate">${bkinfo.takingDate}</td>
                    <c:choose>
                      <c:when test="${not empty bkinfo.deliveryDate}">
                        <td class="deliveryDate">${bkinfo.deliveryDate}</td>
                      </c:when>
                      <c:when test="${empty bkinfo.deliveryDate}">
                        <td class="deliveryDate">not</td>
                      </c:when>
                    </c:choose>
                    <td class="place">${bkinfo.place}</td>
                  </tr>
                  </tbody>
                </c:forEach>
              </table>
              </form>
            </div>
        </c:when>
      </c:choose>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
