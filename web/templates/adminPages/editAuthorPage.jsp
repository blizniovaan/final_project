<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="button.delete-author" var="delete" />
<fmt:message bundle="${lang}" key="text.error" var="error" />
<fmt:message bundle="${lang}" key="title.edit" var="title_edit" />
<fmt:message bundle="${lang}" key="label.delete" var="delete" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="label.author-name" var="author_name" />
<fmt:message bundle="${lang}" key="label.edit-author" var="edit_author" />
<fmt:message bundle="${lang}" key="label.new-name" var="new_name" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <jsp:include page="../modals/confirmation/confirmEditAuthor.jsp"/>
  <script src="scripts/author.js"></script>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${title_edit}</h2>
    </div>
    <form id="form_add_author">
      <div class="addBlock">
        <p>
          <label>${author_name}: </label>
          <input type="text" id="name" name="name" required/>
          <input type="submit" class="btn btn-primary btn-sm" value="Add new author">
        </p>
      </div>
    </form>
    <form id="form_edit_author">
      <div class="editBlock">
        <p>
          <label>${edit_author}:</label>
          <select name = "authors" id="authors" required>
            <option value="" disabled selected>element</option>
            <c:forEach items="${authors}" var="authorinfo">
              <option value="${authorinfo.authorId}">${authorinfo.name}</option>
            </c:forEach>
          </select>
          <label>${new_name}:</label>
          <input type="text" id="newName" name="newName" required/>
          <input type="submit" class="btn btn-primary btn-sm" value="Edit name">
        </p>
      </div>
    </form>
    <c:choose>
      <c:when test="${empty authors}">
        <div class="warning">
          <h3>${error}</h3>
        </div>
      </c:when>
      <c:when test="${not empty authors}">
        <form id="form_delete_author">
          <div class="tableContainer">
            <table class="table table-striped table-hover table-condensed table-bordered">
              <thead>
              <tr>
                <th>ID</th>
                <th>${author}</th>
                <th>${delete}</th>
              </tr>
              </thead>
              <c:forEach items="${authors}" var="authorinfo">
                <tbody id="t_books">
                <tr class="rowlink">
                  <td class="info_authorId">${authorinfo.authorId}</td>
                  <td id="info_authorName">${authorinfo.name}</td>
                  <td class="invisible" id="page" value="ADMIN_BOOK_PAGE">ADMIN_BOOK_PAGE</td>
                  <td class="removeAuthor" name="removeAuthor" id="removeAuthor">
                    <input type="checkbox" class="removeAuthor" name="removeAuthor" value="${authorinfo.authorId}"/>
                  </td>
                </tr>
                </tbody>
              </c:forEach>
            </table>
          </div>
          <input type="submit" class="btn btn-primary btn-sm deleteBtn" value="${delete}">
        </form>
      </c:when>
    </c:choose>
  </div>

</div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
