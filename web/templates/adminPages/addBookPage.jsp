<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="status" />
<fmt:message bundle="${lang}" key="label.description" var="description" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="label.edition-year" var="edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="title.genre" var="genre" />
<fmt:message bundle="${lang}" key="title.add-the-book" var="add_the_book" />
<fmt:message bundle="${lang}" key="button.add" var="add" />

<html>
<head>
    <title>YourLibrary.com</title>
    <jsp:include page="../resources.jsp"/>
    <jsp:include page="../modals/confirmation/confirmAddBook.jsp"/>
    <script src="scripts/admin.js" type="text/javascript"></script>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
    <jsp:include page="../sidebar/adminSidebar.jsp"/>
    <div class="content">
        <div class="top-main-content">
            <h2>${add_the_book}</h2>
        </div>
        <form class="bookForm" action="controller" method="post" id="form_add_book">
            <div class="book_info">
                <p>
                    <label>${title_book}:</label>
                    <input type="text" id="bookTitle" name="bookTitle" maxlength="100" required/>
                </p>
                <p>
                    <label>${author}:</label>
                    <div  id="authorBlock" class="authorBlock">
                        <select name="authors" id="authors" required>
                            <option value="" disabled selected>element</option>
                            <c:forEach items="${authors}" var="authorinfo">
                                <option value="${authorinfo.authorId}">${authorinfo.name}</option>
                            </c:forEach>
                        </select>
                        <input type="button" value="-" class="cancel hide" id="cancel"/>
                    </div>
                    <input type="button" id="addAuthor" value="+"/>
                </p>
                <p>
                    <label>${edition_year}:</label>
                    <input type="number" id="editionYear" name="editionYear" min="500" max="2016"/>
                </p>
                <p>
                    <label> ${publisher}:</label>
                    <select name="publisher" id="publisher" required>
                        <option value="" disabled selected>element</option>
                        <c:forEach items="${publishers}" var="pubinfo">
                            <option value="${pubinfo.publisherId}">${pubinfo.title}</option>
                        </c:forEach>
                    </select>
                </p>
                <p>
                    <label> ${genre}:</label>
                    <select name="genre" id="genre" required>
                        <option value="" disabled selected>element</option>
                        <c:forEach items="${genres}" var="genreinfo">
                            <option value="${genreinfo.genreId}">${genreinfo.title}</option>
                        </c:forEach>
                    </select>
                </p>
                <p>
                    <label>${description}:</label>
                    <textarea name="description" id="description" maxlength="850" cols="50"> </textarea>
                </p>
                <input type="submit" value="${add}">
            </div>
        </form>
    </div>
</div>

<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
