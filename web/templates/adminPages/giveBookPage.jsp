<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="out"%>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="genre.${genre}" var="currentGenre" />
<fmt:message bundle="${lang}" key="button.apply" var="apply" />
<fmt:message bundle="${lang}" key="label.name" var="name" />
<fmt:message bundle="${lang}" key="label.surname" var="surname" />
<fmt:message bundle="${lang}" key="label.login" var="login" />
<fmt:message bundle="${lang}" key="title.digital_library" var="digital_library" />
<fmt:message bundle="${lang}" key="message.no-book" var="mess_no_book" />
<fmt:message bundle="${lang}" key="label.book_place" var="book_place" />
<fmt:message bundle="${lang}" key="library_card" var="library_card" />
<fmt:message bundle="${lang}" key="reading_room" var="reading_room" />
<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.choose" var="choose" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/bookGiver.js" type="text/javascript"></script>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>

  <div class="content">
    <div class="top-main-content">
      <h2>${digital_library}</h2>
    </div>
    <div class="title">
      <h3></h3>
    </div>

    <c:choose>
    <c:when test="${empty books}">
      <div class="warning">
        <h3>${mess_no_book}</h3>
      </div>
    </c:when>
    <c:when test="${not empty books}">
    <form action="controller" method="post" id="admin-give-book-form" name="admin-give-book-form" class="admin-give-book-form">
      <div class = "admin-place">
        <label for = "place">${book_place}: </label>
        <select class="select" id = "place">
          <option id="card-place" value="LIBRARY_CARD">${library_card}</option>
          <option id="reading-room-place" value="READING_ROOM">${reading_room}</option>
        </select>
      </div>
      <div class="block">
        <div class="doubleTable1Container">
          <table class="table table-striped table-hover table-condensed table-bordered">
            <thead>
            <tr>
              <th>ID</th>
              <th>${title_book}</th>
              <th>${author}</th>
              <th>${choose}</th>
            </tr>
            </thead>
            <c:forEach items="${books}" var="bkinfo">
              <tbody id="t_books">
              <tr class="rowlink">
                <td class="bookId">${bkinfo.bookId}</td>
                <td class="title">${bkinfo.title}</td>
                <td class="authors">
                  <c:forEach items="${bkinfo.authors}" var="author">
                    ${author.name}
                  </c:forEach>
                </td>
                <td><input type="checkbox" value="${bkinfo.bookId}" class="givedBook" name="givedBook"/></td>
              </tr>
              </tbody>
            </c:forEach>
          </table>
        </div>
        </c:when>
        </c:choose>
        <c:choose>
        <c:when test="${empty users}">
          <div class="warning">
            <h3>${mess_no_book}</h3>
          </div>
        </c:when>
        <c:when test="${not empty users}">
        <div class="doubleTable2Container">
          <table class="table table-striped table-hover table-condensed table-bordered">
            <thead>
            <tr>
              <th>ID</th>
              <th>${name}</th>
              <th>${surname}</th>
              <th>${login}</th>
              <th>${choose}</th>
            </tr>
            </thead>
            <c:forEach items="${users}" var="userinfo">
              <tbody id="t_books">
              <tr class="rowlink">
                <td class="userinfo">${userinfo.userId}</td>
                <td class="title">${userinfo.name}</td>
                <td class="authors">${userinfo.surname}</td>
                <td class="authors">${userinfo.login}</td>
                <td><input type = "radio" value="${userinfo.userId}" class = "user" name="user" id="user"/></td>
              </tr>
              </tbody>
            </c:forEach>
          </table>
        </div>
      </div>
      <input type="submit"  class="btn btn-primary btn-sm approve" value="${apply}">
    </form>
    </c:when>
    </c:choose>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<jsp:include page="../modals/confirmation/confirmGiveBook.jsp"/>
</body>
</html>
