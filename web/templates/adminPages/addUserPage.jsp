<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="button.add" var="add" />
<fmt:message bundle="${lang}" key="title.add-the-user" var="add_the_user" />
<fmt:message bundle="${lang}" key="label.name" var="name" />
<fmt:message bundle="${lang}" key="label.surname" var="surname" />
<fmt:message bundle="${lang}" key="label.login" var="login" />
<fmt:message bundle="${lang}" key="label.role" var="role" />
<fmt:message bundle="${lang}" key="label.password" var="password" />

<fmt:message key="signup.reppassword" bundle="${lang}" var="reppassword"/>

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/userCreator.js" type="text/javascript"></script>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${add_the_user}</h2>
    </div>
    <form class="bookForm" action="controller" method="post" id="form_add_user" name="form_add_user">
      <div class="book_info">
        <p>
          <label>${name}:</label>
          <input type="text" id="name" name="name" required/>
        </p>
        <p>
          <label>${surname}:</label>
          <input type="text" id="surname" name="surname" required/>
        </p>
        <p>
          <label>${login}:</label>
          <input type="text" id="login" name="login" required/>
        </p>
        <p>
          <label>${password}:</label>
          <input type="text" id="password" name="password" required/>
        </p>
        <p>
          <label>${role}:</label>
          <input value="user" disabled/>
        </p>
        <input type = "submit" class="btn btn-primary btn-sm" value = "${add}">
      </div>
    </form>
  </div>
</div>

<jsp:include page="../footer/footer.jsp"/>
<jsp:include page="../modals/confirmation/confirmAddUser.jsp"/>
</body>
</html>
