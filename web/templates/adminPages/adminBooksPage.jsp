<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="out"%>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="language.button_ru" var="ru_button" />
<fmt:message bundle="${lang}" key="language.button_en" var="en_button" />
<fmt:message bundle="${lang}" key="genre.${genre}" var="currentGenre" />
<fmt:message bundle="${lang}" key="button.apply" var="apply" />
<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="status" />
<fmt:message bundle="${lang}" key="label.description" var="description" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="label.edition-year" var="edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="message.no-book" var="message_no_book" />
<fmt:message bundle="${lang}" key="title.genre" var="genre" />
<fmt:message bundle="${lang}" key="label.delete" var="delete" />
<fmt:message bundle="${lang}" key="title.digital_library" var="digital_library" />

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/bookRemover.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${digital_library}</h2>
    </div>
    <div class = "search-letter-block">
      <c:if test="${command != 'delete'}">
        <out:authorsLetterList/>
        <out:booksLetterList/>
      </c:if>
    </div>
    <form action="controller" method="post" id="admin-book-form">
      <div class="info-table">
        <c:choose>
          <c:when test="${empty partBooks}">
            <div class="warning">
              <h3>${message_no_book}</h3>
            </div>
          </c:when>
          <c:when test="${not empty partBooks}">
            <div>
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>${title_book}</th>
                  <th>${author}</th>
                  <th>${publisher}</th>
                  <th>${genre}</th>
                  <c:if test="${command == 'delete'}">
                    <th>${delete}</th>
                  </c:if>
                </tr>
                </thead>
                <c:forEach items="${partBooks}" var="bkinfo">
                  <tbody id="t_books">
                  <tr class="rowlink">
                    <td class="bookId">${bkinfo.bookId}</td>
                    <td class="title">${bkinfo.title}</td>
                    <td class="authors">
                      <c:forEach items="${bkinfo.authors}" var="author">
                        ${author.name}<br/>
                      </c:forEach>
                    </td>
                    <td class="publisher">${bkinfo.publisher.title}</td>
                    <td id="genre">${bkinfo.genre.title}</td>
                    <td class="invisible" id="page" value="ADMIN_BOOK_PAGE">ADMIN_BOOK_PAGE</td>
                    <c:if test="${command == 'delete'}">
                      <td class="removeBook" name="removeBook" id="removeBook">
                        <input type="checkbox" class="removeBook" name="removeBook" value="${bkinfo.bookId}"/>
                      </td>
                    </c:if>
                  </tr>
                  </tbody>
                </c:forEach>
              </table>
            </div>
          </c:when>
        </c:choose>
      </div>
      <c:if test="${command == 'delete'}">
        <input type="submit"  class="btn btn-primary btn-sm approve" value="${delete}">
      </c:if>
    </form>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<jsp:include page="../modals/confirmation/confirmRemoveBook.jsp"/>
<c:if test="${command != 'delete'}">
  <script type="text/javascript" src="scripts/table-click.js"></script>
</c:if>
</body>
</html>
