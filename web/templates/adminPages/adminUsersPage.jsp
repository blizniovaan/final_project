<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="title.digital_library" var="digital_library" />
<fmt:message bundle="${lang}" key="label.name" var="name" />
<fmt:message bundle="${lang}" key="label.surname" var="surname" />
<fmt:message bundle="${lang}" key="label.login" var="login" />
<fmt:message bundle="${lang}" key="label.role" var="role" />
<fmt:message bundle="${lang}" key="label.password" var="password" />
<fmt:message bundle="${lang}" key="button.apply" var="apply" />
<fmt:message bundle="${lang}" key="message.no-users" var="no_users" />
<fmt:message bundle="${lang}" key="label.delete" var="delete" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <script src="scripts/userRemover.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${digital_library}</h2>
    </div>
    <div class = "search-letter-block">
      <jsp:include page="../modals/searchByUserSurname.jsp"/>
    </div>
    <div class = "search-letter-block">
    </div>
    <form action="controller" method="post" id="admin-user-form">
      <div class="info-table">
        <c:choose>
          <c:when test="${empty users}">
            <div class="warning">
              <h3>${no_users}</h3>
            </div>
          </c:when>
          <c:when test="${not empty users}">
            <div>
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>${name}</th>
                  <th>${surname}</th>
                  <th>${login}</th>
                  <th>${role}</th>
                  <c:if test="${command == 'delete'}">
                    <th>${delete}</th>
                  </c:if>
                </tr>
                </thead>
                <c:forEach items="${users}" var="usersinfo">
                  <tbody id="t_users">
                  <tr class="rowlink">
                    <td class="userId">${usersinfo.userId}</td>
                    <td class="userName">${usersinfo.name}</td>
                    <td class="userSurname">${usersinfo.surname}</td>
                    <td class="userLogin">${usersinfo.login}</td>
                    <td class="role">${usersinfo.role}</td>
                    <c:if test="${command == 'delete'}">
                      <td class = "removedUser" name = "removedUser" id = "removedUser">
                        <input type = "checkbox" class = "removedUser" name = "removedUser" value = "${usersinfo.userId}"/>
                      </td>
                    </c:if>
                  </tr>
                  </tbody>
                </c:forEach>
              </table>
            </div>
          </c:when>
        </c:choose>
      </div>
      <br/>
      <c:if test="${command == 'delete'}">
        <input type="submit"  class="btn btn-primary btn-sm approve" value="${apply}">
      </c:if>
    </form>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
<jsp:include page="../modals/confirmation/confirmRemoveUser.jsp"/>
<c:if test="${command != 'delete'}">
  <script type="text/javascript" src="scripts/user-info-table-click.js"></script>
</c:if>
</body>
</html>
