<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>
<fmt:message bundle="${lang}" key="button.delete-publisher" var="delete" />
<fmt:message bundle="${lang}" key="text.error" var="error" />
<fmt:message bundle="${lang}" key="title.edit" var="title_edit" />
<fmt:message bundle="${lang}" key="label.publisher-title" var="publisher_title" />
<fmt:message bundle="${lang}" key="label.edit-publisher-title" var="edit_publisher_title" />
<fmt:message bundle="${lang}" key="label.new-title" var="new_title" />
<fmt:message bundle="${lang}" key="label.delete" var="delete" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="text.element" var="element" />
<fmt:message bundle="${lang}" key="button.add" var="add" />
<fmt:message bundle="${lang}" key="button.edit" var="edit" />
<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
  <jsp:include page="../modals/confirmation/confirmEditPublisher.jsp"/>
  <script src="scripts/publisher.js"></script>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${title_edit}</h2>
    </div>
    <form action="controller" method="post" id="form_add_publisher">
      <div class="addBlock">
        <p>
          <label>${publisher_title}:</label>
          <input type="text" id="publisherTitle" name="publisherTitle" required/>
          <input type="submit" class="btn btn-primary btn-sm" value="${add}">
        </p>
      </div>
    </form>
    <form action="controller" method="post" id="form_edit_publisher">
      <div class="editBlock">
        <p>
          <label>${edit_publisher_title}:</label>
          <select name="publisher" id="publisher" required>
            <option value="" disabled selected>${element}</option>
            <c:forEach items="${publishers}" var="publisherinfo">
              <option value="${publisherinfo.publisherId}">${publisherinfo.title}</option>
            </c:forEach>
          </select>
          <label>${new_title}:</label>
          <input type="text" id="newPublisherTitle" name="newPublisherTitle" required/>
          <input type="submit" class="btn btn-primary btn-sm" value="${edit}">
        </p>
      </div>
    </form>
    <form action="controller" method="post" id="form_delete_publisher">
      <c:choose>
        <c:when test="${empty publishers}">
          <div class="warning">
            <h3>${error}</h3>
          </div>
        </c:when>
        <c:when test="${not empty publishers}">
          <div>
            <div class="tableContainer">
              <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>${publisher}</th>
                  <th>${delete}</th>
                </tr>
                </thead>
                <c:forEach items="${publishers}" var="publisherinfo">
                  <tbody id="t_books">
                  <tr class="rowlink">
                    <td class="info_publisherId">${publisherinfo.publisherId}</td>
                    <td id="info_publisherTitle">${publisherinfo.title}</td>
                    <td class="invisible" id="page" value="ADMIN_BOOK_PAGE">ADMIN_BOOK_PAGE</td>
                    <td class="removePublisher" name="removePublisher" id="removePublisher">
                      <input type="checkbox" class="removePublisher" name="removePublisher" value="${publisherinfo.publisherId}" />
                    </td>
                  </tr>
                  </tbody>
                </c:forEach>
              </table>
            </div>
            <input type="submit" class="btn btn-primary btn-sm deleteBtn" value="${delete}">
          </div>
        </c:when>
      </c:choose>
    </form>
  </div>
</div>
</div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
