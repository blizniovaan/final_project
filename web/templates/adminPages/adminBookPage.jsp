<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.library" var="lang"/>

<fmt:message bundle="${lang}" key="label.title_book" var="title_book" />
<fmt:message bundle="${lang}" key="label.book_status" var="status" />
<fmt:message bundle="${lang}" key="label.description" var="description" />
<fmt:message bundle="${lang}" key="label.publisher" var="publisher" />
<fmt:message bundle="${lang}" key="label.edition-year" var="edition_year" />
<fmt:message bundle="${lang}" key="label.author" var="author" />
<fmt:message bundle="${lang}" key="title.about-book" var="about_book" />

<html>
<head>
  <title>YourLibrary.com</title>
  <jsp:include page="../resources.jsp"/>
</head>

<body>
<jsp:include page="../header/adminHeader.jsp"/>
<div class="wrapper">
  <jsp:include page="../sidebar/adminSidebar.jsp"/>
  <div class="content">
    <div class="top-main-content">
      <h2>${about_book}</h2>
    </div>
    <div class="book_info">
      <p><label>${title_book}:</label> ${book.title}</p>
      <p><label>${status}:</label> ${book.status.status}</p>
      <p><label>ID:</label> ${book.bookId}</p>
      <p>
        <label>${author}:</label>
      <c:forEach items="${book.authors}" var="author">
         ${author.name}
      </c:forEach>
      </p>
      <p><label>${edition_year}:</label> ${book.editionYear}</p>
      <p><label> ${publisher}:</label> ${book.publisher.title}</p>
      <p><label>${description}:</label>${book.description}</p>
    </div>
  </div>
</div>
<jsp:include page="../footer/footer.jsp"/>
</body>
</html>
